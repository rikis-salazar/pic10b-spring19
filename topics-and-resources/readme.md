# List of Topics & additional links (resources)

As stated in the [class syllabus][syll]

> ... be advised that some of the material presented during lecture can be found
> elsewhere.

This means that the textbook will mostly be used as a _reference_, as opposed to
a _duplicate_. That is, I will not simply _repeat or duplicate_ the author's
explanations. Instead, alternative explanations/examples will be provided for
you. Moreover, we will not necessarily discuss the class topics in the order in
which they appear in the textbook.

The list of topics below contain _key concepts_ that we have discussed during
lecture. Whenever possible, the section form the textbook where information
about said concepts can be found is included.

## Topics discussed after midterm 2

*   A template _Linked List_ class

    During lecture I _mostly_ followed the first part of [this
    handout][list-handout]. I write _mostly_ because some local variables, and
    some member functions of the `Iterator<T>` class were not fully discussed;
    however I was true the general outline as well as the ideas found therein.

    **Technical concepts:**

    -   The inner workings of a [doubly linked list][linked-list] container:
        how data is stored, how to insert/delete elements, how to look up data.

    See sections 12.1, 12.2, and 12.3, as well as [this Google slides
    presentation][google-lists].

*   Container adapters: _stacks_ and _queues_

    During lecture I pointed that the signature feature of these containers is
    that the perform a limited set of operations in constant time (in most of
    the cases).

    **Technical concepts:**

    -   The inner workings of a [stack][stack] container: how data is stored,
        how to insert/delete elements~~, how to look up data~~.

    -   The inner workings of a [queue][queue] container: how data is stored,
        how to insert/delete elements~~, how to look up data~~.

    Container adapters rely on other adapters (usually sequential ones) to
    provide the correct functionality. [In this handout][cont-adapt] I explain
    the basic ideas behind how to _adapt_ our very own template `stack`, and
    `queue` classes.

    See sections 12.4, as well as [this Google slides
    presentation][google-adapt].

*   Binary Search Trees.

    During lecture we

    *   discuss terminology associated to binary trees in general: _nodes_,
        _height_, _root_, _leaves_, etc.;
    *   proposed different algorithms to implement common tasks either
        recursively, or non-recursively;
    *   _"computed"_ the computational cost associated to most of these
        algorithms: we did not setup recurrence relations, but relayed on
        specific tree features, more specifically, the _height_ of the tree.
    *   pointed our that some of the so-called [associative container
        classes][assoc-cont] in `C++` are based on _self-balancing_ binary
        trees.

    **Technical concepts:**

    -   The inner workings of a [binary search tree][bst-wiki] implemented via a
        _node_ class with only two pointers: how data is stored, how to
        insert/delete elements, how to look up data.

    -   Tree traversals: _pre-order_, _in-order_, and _post-order_.

    See sections 13.2, and 13.3, as well as [this Google slides
    presentation][google-bst].

*   Min-heaps (and priority queues).

    During lecture we

    *   proposed different ways to implement min-heap: via a binary tree, and
        via an underlying container that provides _fast_ lookups.
    *   _"computed"_ the computational cost of operations associated to most
        heaps in general.

    **Technical concepts:**

    -   The inner workings of a [heap][heap-wiki] implemented via a
        `std::vector`: how data is entered into the heap, how the root node is
        removed from the heap.

        > **Note:** Using an underlying container (_i.e.,_ adapting) is the
        > _standard approach_ (see [`std::priority_queue`][std-pq]).

    See sections 13.5, and 13.6, as well as [this Google slides
    presentation][google-heap].


[list-handout]: ../handouts/template-list/
[linked-list]: https://en.wikipedia.org/wiki/Doubly_linked_list
[google-lists]: https://docs.google.com/presentation/d/e/2PACX-1vQA0w2fuAevyrGLXgIWaigly1iiyY_oVdB3pSNGQOTW_eT9cjeX4lw2KH1cUIxwPvVtNwgXaH5Q4XWK/pub?start=false&loop=false&delayms=3000
[stack]: https://en.wikipedia.org/wiki/Stack_(abstract_data_type)
[queue]: https://en.wikipedia.org/wiki/Queue_(abstract_data_type)
[cont-adapt]: ../handouts/container-adapters/
[google-adapt]: https://docs.google.com/presentation/d/e/2PACX-1vQR2oq_dFx2KLYlG2AI93N9DFx1ojvapR5xKDwqX-MT50uxOOXgRuEPCOioFlJS6QhAqxc0upeMIvrS/pub?start=false&loop=false&delayms=3000
[bst-wiki]: https://en.wikipedia.org/wiki/Binary_search_tree
[assoc-cont]: https://en.wikipedia.org/wiki/Associative_containers
[google-bst]: https://docs.google.com/presentation/d/e/2PACX-1vRgmi2P9ZaZGAlJkw6_NLXVdmql1bXAsTmOuQ3fopLJkJejvKNgiPKzfTjte64ZC9TaO19qCOccwRoG/pub?start=false&loop=false&delayms=3000
[heap-wiki]: https://en.wikipedia.org/wiki/Binary_heap
[std-pq]: https://en.cppreference.com/w/cpp/container/priority_queue
[google-heap]: https://docs.google.com/presentation/d/e/2PACX-1vQUWFxV2gIdxMKXWTw4ZEdiwu6qyjyDy_J0g0m5fohjsi9lt6uAVKeLcMbJb0YecTyOkIhAaACvIBOV/pub?start=false&loop=false&delayms=3000

---

## Topics discussed prior to midterm 2 (but after midterm 1)

*   Analysis of recursive algorithms

    During lecture I carefully analyzed two specific examples, namely the
    factorial function, and the _binary search_ algorithm. The basic idea is to
    obtain a _recurrence relation_, and a set of stopping conditions; then
    iterate until a general expression is found. This general expression, when
    combined with the stopping conditions, should then lead to a non-recurrent
    formula that can be expressed via _Big Oh_ notation.

    **Technical concepts:**

    -   Performance analysis of recursive algorithms: unfortunately our textbook
        does not perform such analysis. However it does perform _only one_
        analysis of non-recursive algorithm (section 11.3).

        The closest thing to the analysis we performed during lecture can be
        found at [this old Google slides presentation][google-rec2] document.
        The material you want to review is in slides 1 through 5.

*   Recursion

    During lecture we mostly went over specific examples from [an old Google
    slides presentation][google-rec] document available at the [Old lessons
    [deprecated]][old-lessons] section of the CCLE course website. We did not
    discussed all of the examples as the basic ideas were already covered while
    discussing the `factorial` function, as well as the `T_plus`, and `T_minus`
    examples. A particular _eye-opening_ moment was the comparison of the
    _recursive_ and _non-recursive_ implementations of the `fibonacci` function.

    **Technical concepts:**

    -   Recursion: see sections 10.1, 10.3, and 10.6.

    **_Which one is better?_**

    -   Non-recursive implementations of _fibonacci_, _factorial_, and _binary
        search_ are better. Why?
    -   Recursive implementations of _hanoi_, and _Monte Carlo roulette_ are
        better. Why?

    To answer these questions checkout the code available at the _Recursion_
    section of the [Old code [deprecated]][old-code] section of the CCLE course
    website.

*   Templates: the `first_less_than_second` function, as well as the
    `SimplePair` class.

    During lecture we followed the contents of the following handouts:

    i.  [Template functions][pic10b-temp-fun].
    i.  [Template classes][pic10b-temp-class].

    **Technical concepts:**

    -   Template functions and template classes: see sections 16.1, 16.3, and
        16.4.

    > In addition, feel free to check out the `MyVector` and `MaxAndMin` classes
    > that I have used in the past. There is no handout associated to them, but
    > the code is not hard to follow. A collection of _barebones_ files are
    > available for you to try to provide your own code and compare it to the
    > working versions of these classes.
    >
    > The files corresponding to the aforementioned `MaxAndMin` and `MyVector`
    > classes can be found in the _Template classes_ section of the [Old code
    > [deprecated]][old-code] section of the CCLE course website.
    >
    > Feel free to go over the _Template functions_ section as well.

*   The non-template `Pic10b::vector` class

    During lecture we followed the contents of [this handout][pic10b-vec].

    **Technical concepts:**

    -   [Namespace][namesp]:

        > "a declarative region that provides a scope to the identifiers (names
        > of the types, function, variables etc) inside it."

    -   The inner workings of a vector container: how data is stored, how to
        insert/delete elements, how to look up data.
    -   _The big 4_ re-revisited: we coded all members of the big 4 for this
        specific type of container.

*   A `BasicNode` class

    During lecture we followed the contents of [this handout][case-against].

    **Technical concepts:**

    -   _Non-default, classs-specific big 4_: the `BasicNode` class class was
        coded with the purpose of illustrating the dangers of letting the
        compiler provide default code for the big 4. We also discussed a way to
        provide a [class-specific copy constructor][incomplete-fix] that avoids
        the issues we encountered.

        > **Note:** the assignment operator was not _fixed_ during lecture.
        > Instead I suggested you try to fix it on your own.[^note-self]

*   The `Thing` class

    During lecture we followed the contents of [this handout][big4].

    **Technical concepts:**

    -   [_The big 4_ revisited (handout)][big4]. The class `Thing` class was
        coded with the purpose of figuring out the signatures, as well as the
        default behavior of these special functions.

*   _Big Oh_ notation

    Unfortunately, your textbook does not provide a lot of information regarding
    this mathematical tool. The links below contain some examples, and should
    help you get familiar with this topic.

    -   [A beginners guide to _Big Oh_ notation][link1]
    -   [_Big Oh_ notation (Wikipedia entry)][link2]
    -   [_Big Oh_ notation explained][link3]
    -   [_Big Oh_ notation -- Problem solving with algorithms and data
        structures][link4]
    -   [_Big Oh_ notation examples (Google search results)][link5]

    Also, feel free to checkout slides 4 through 15 of [this deprecated Google
    slides presentation][slides2].

*   The `Cosa` class

    **Technical concepts:**

    -   [_The big 4 (big 3 + default constructor)_][wiki-big3]  See sections 5.5
        and 15.3.2 for default constructor; 15.3.3 for copy constructor; 15.3.5
        for assignment operator; and 15.4 for destructors.

    -   [_The `static` keyword_][static].

        See also [this old example][static_code] to find out about _subtle_
        differences between `const`/_non-const_ static variables and/or objects.

[static]: https://www.geeksforgeeks.org/static-keyword-cpp/
[static_code]: https://ccle.ucla.edu/pluginfile.php/2793165/mod_resource/content/5/static_examples.cpp
[link1]: https://rob-bell.net/2009/06/a-beginners-guide-to-big-o-notation/
[link2]: https://en.wikipedia.org/wiki/Big_O_notation
[link3]: https://yourbasic.org/algorithms/big-o-notation-explained/
[link4]: https://interactivepython.org/lpomz/courselib/static/pythonds/AlgorithmAnalysis/BigONotation.html
[link5]: https://www.google.com/search?q=big+o+notation+examples
[slides2]: https://docs.google.com/presentation/d/11UZY6dvgNY1S1vXEZZZBsFUa9kz7_Udd3xbRRxy71wM/pub?start=false&loop=false&delayms=3000&slide=id.p
[big4]: ../handouts/the-big-4/
[case-against]: ../handouts/case-against-default-big-4/
[incomplete-fix]: ../handouts/basic-node-revisited/
[pic10b-vec]: ../handouts/non-template-vector/
[pic10b-temp-fun]: ../handouts/template-functions/
[pic10b-temp-class]: ../handouts/template-classes/
[old-code]: https://ccle.ucla.edu/course/view.php?id=73519&section=6
[old-lessons]: https://ccle.ucla.edu/course/view.php?id=73519&section=5
[google-rec]: https://docs.google.com/presentation/d/1aRnGfG3DofjW9EJtD0Gm7igp7204syI9An74qTy4NHQ/pub?start=false&loop=false&delayms=3000&slide=id.p
[google-rec2]: https://docs.google.com/presentation/d/1NQ74tJYOJi_fuidBUTKt61dCPcOJ2tdwfCxvJ2oepY8/pub?start=false&loop=false&delayms=3000&slide=id.p

[^note-self]: _Note to self:_ This might make for a _nice_ exam question.


---

## Topics discussed prior to midterm 1

*   The `Fraction` class

    **Technical concepts:**

    -   [_Shadowing of variables/objects._][wiki-shadow] (See page 1022, as well
        as section 18.2.1)
    -   [_Function overloading_][func_overl]. We overloaded

        i.  constructors by number of parameters (see sections 5.5, and 5.6),
            and
        i.  _hybrid_ getter/setter functions, by `const` modifier (see class
            `SafeArray`, page 573, section 14.9).

        > _Note:_ `C++` does not allow overloading by return type.

    -   [`struct` _vs._ `class`][st-v-cl]

    -   [`const` correctness][cc-main]. Here is a
        [good, although technical definition][cc-gtd], and
        [the example found here][cc-efh] is related to the errors we encountered
        during lecture.

    -   [_Return by reference_][rbr]

        > "When returning a reference, [make sure] the object being referred to
        > does not go out of scope."

        Or, in other words: _"With great power, comes great responsability."_

        I have been searching for the exact place where the class textbook
        introduces this concept, but so far I have only managed to spot places
        where it is used (without really explaining what it is). The first time
        it is used (I believe) is in page 552 in the `Fraction::operator+=`
        function. The closest thing to an explanation I have found is at page
        565, where it is written

        > A good rule of thumb is if you return a value that exists outside the
        > scope of a function (such as `this`) you should return a reference. If
        > you return the value held by a local variable, return a value.

    -   _Inheritance._ I briefly mentioned this concept while discussing the
        use of `ostream` objects. In short, `cout` as well as an output file
        stream can be "_stored_" in a `ostream` container, even if they _do not
        belong to the same class_ (`ostream` _vs._ `ofstream`).

        > **Please note:** Other than the mention above, I will not talk about
        > _inheritance_ in this course. However, if you want to know more about
        > this topic see contents in Chapter 8).

    -   [_Chaining_][cha]. (See section 14.5)

    -   _Operator overloading_. There is a whole chapter in the textbook,
        namely chapter 14.

    -   [_Default value parameters_][dfv]. We used them for the first time in
        the `print_to()` function:

        -   `print_to()`, without parameters sends information to the console,
            it is equivalent to `print_to(std::cout)`; on the other hand
        -   `print_to(out)` sends information to the output stream _stored_ in
            the `std::ostream` object `out`.

        See also our improved version of the class constructors.

    -   [_Initialization lists_][il]. They allow to write more efficient
        constructors. It is also a good way to stick a very important
        programming principle: _make sure all of your class constructors
        properly initialize all of the class member fields_ (see Advanced
        Topic 5.1, pages 247-248, as well as section 21.4).

    -   [_The_ `friend` _keyword_][fk]. It is used to grant non-members of a
        class access to the private fields (see section 18.4).

    -   _[Implicit conversions][impl] and the_ `explicit` _keyword_. Some times,
        the compiler performs conversions from one type to another in situations
        where no explicit request appears. _E.g.:_ `1 + Fraction(2,3)` is
        interpreted by the compiler as `Fraction(1) + Fraction(2,3)` because

        -   we overloaded `operator+` as a _non-member function_,
        -   we coded a 1-parameter constructor that takes an `int` as parameter,
            and
        -   we did not use the `explicit` keyword in any of our constructors.

        (See section 14.8, and the advanced topic 14.4 therein.)


    **Writing better programs:**

    -   [_Single vs. multiple_ points of maintenance][spm]

        > The concept of a single point of maintenance dictates that frequently
        > used elements should be defined, and modified, in a single location.
        > Duplication of such elements increases the difficulty of change, may
        > decrease clarity and increases the likelihood of inconsistency.

        Also, remember that _"copy/paste code leads to copy/paste bugs"_.

    -   [Readable _vs._ _"smart"_ code][rvs]

    Also, make sure to review the slides & code associated to this class. They
    can be found in the [_home-made_ classes][hmc] section of this website.

[syll]: ../syllabus
[hmc]: ../home-made-classes
[wiki-shadow]: https://en.wikipedia.org/wiki/Variable_shadowing
[func_overl]: https://www.tutorialspoint.com/cplusplus/cpp_overloading.htm
[st-v-cl]: https://www.geeksforgeeks.org/g-fact-76/
[cc-main]: https://isocpp.org/wiki/faq/const-correctness
[cc-gtd]: https://isocpp.org/wiki/faq/const-correctness#overview-const
[cc-efh]: https://isocpp.org/wiki/faq/const-correctness#const-member-fns
[rbr]: https://www.tutorialspoint.com/cplusplus/returning_values_by_reference.htm
[spm]: http://www.ifsq.org/single-point-of-maintenance.html
[rvs]: https://www.simplethread.com/dont-be-clever/
[dfv]: https://www.programiz.com/cpp-programming/default-argument
[cha]: https://en.wikipedia.org/wiki/Method_chaining
[il]: https://www.geeksforgeeks.org/when-do-we-use-initializer-list-in-c/
[fk]: https://www.tutorialspoint.com/cplusplus/cpp_friend_functions.htm
[impl]: http://www.cplusplus.com/doc/tutorial/typecasting/
[wiki-big3]: https://en.wikipedia.org/wiki/Rule_of_three_(C%2B%2B_programming)

<!-- Another one... otra vez contigo!!!
12. Sorting algorithms

    During lecture we studied several different sorting algorithms. Our focus
    not only was on they associated cost, but also the identification of their
    _strengths_, as well as their _weaknesses_.

    More specifically, we analyzed the following _non-standard_ algorithms:

    *   _Binary Search Tree_ sort:

        1.  Create a binary search tree, then
        2.  Destroy (or traverse) it _in-order_.

    *   _Heap_ sort (see also _heapify_ below):

        1.  Create a min-heap, then
        2.  Destroy it by removing one node at a time.

    *   _Heapify_:

        1.  Turn a correctly balanced binary tree (_e.g.,_ a vector) into a
            min-heap by traversing it "_backwards_" (_i.e.,_ from leaves to
            root), while continuously _"restoring the heap property"_; then
        2.  destroy the initial structure (which is now a heap) by removing one
            node at a time.

        > **Note:** This process is done in place. That is, the original
        > container does not need to be copied. See also [`std::make_heap'][mh].

    We also analyzed and/or demonstrated the use in practice of the following
    algorithms:

    *   The _"slow"_ ones

        -   [_Selection sort_][sel-sort]
        -   [_Insertion sort_][ins-sort]
        -   [_Bubble sort_][bubble-sort]

    *   The _"fast"_ ones

        -   [_Quick sort_][quick-sort]
        -   [_Merge sort_][merge-sort]

    See sections 11.1 through 11.5, as well as [this Google slides
    presentation][google-sort].

[google-sort]: https://docs.google.com/presentation/d/e/2PACX-1vT3zwFJEmdTZJ21UEsAFUsYpJzrWFvEiOx6d34TUHx3Sdrkg8ntcV6hfg1UvQgil3wnBlJCV7SQPVqa/pub?start=false&loop=false&delayms=3000
[mh]: https://en.cppreference.com/w/cpp/algorithm/make_heap
[sel-sort]: https://www.toptal.com/developers/sorting-algorithms/selection-sort
[bubble-sort]: https://www.toptal.com/developers/sorting-algorithms/bubble-sort
[ins-sort]: https://www.toptal.com/developers/sorting-algorithms/insertion-sort
[quick-sort]: https://www.toptal.com/developers/sorting-algorithms/quick-sort
[merge-sort]: https://www.toptal.com/developers/sorting-algorithms/merge-sort
-->

---

[Return to main PIC course website][main]

[main]: ..
[namesp]: https://www.geeksforgeeks.org/namespace-in-c/
