# Syllabus: Spring 2019 Lecture 2

> If you are reading the online (html) version of this syllabus, [click
> here][pdf] to access a `.pdf` version of this document.[^if-you]

[pdf]: readme.pdf
[^if-you]: The embedded link does not work in the `.pdf` document.

---

## Course information

**Contact:** [rsalazar@math.ucla.edu][correo] (write "Pic 10B" in the
subject).

**Time:** 12:00 to 12:50 pm.

**Location:** MS 5200

**Office hours:** [See CCLE class website (CCLE)][class-website].

**Teaching assistant(s):**

|**Section**| **T. A.**     |**Office**| **E-mail**                           |
|:---------:|:--------------|:--------:|:-------------------------------------|
| 2A, 2B    | Gura, K. V.   | MS 3915B | [`gura@math.ucla.edu`][t-a] |

[correo]: mailto:rsalazar@math.ucla.edu
[class-website]: https://ccle.ucla.edu/course/view/19S-COMPTNG10B-2?section=0
[t-a]: mailto:gura@math.ucla.edu

## Course Description

Basic principles of programming, using `C++`; algorithmic, procedural problem
solving; program design and development; basic data types, control structures
and functions; functional arrays and pointers/iterators; introduction to classes
for programmer-defined data types.


## Textbook

You are welcome to use your copy of Horstmann C. & Budd, T. A. Big `C++`,
_2nd/3rd Edition._ John Wiley and Sons, Inc. However, be advised that most of
the material presented during lecture can be found elsewhere. Whenever possible,
lecture notes and/or slides will be prepared, and if applicable, made available
to students.


## CCLE and MyUCLA

This course will use a combination of a password-protected Internet site (CCLE),
as well as other regular (_i.e.,_ non-protected) sites to post course materials
and announcements. These materials can include the syllabus, handouts and
Internet links referenced in class. On some special occasions hard copies of
said materials will be available during class, however these occasions will
constitute the exception as opposed to the rule; that is, in most cases hard
copies of these materials **will not** be made available to the class.


## Reaching me via email

_During this current quarter I will be in charge of course(s) were enrollment is
higher than usual for a UCLA class._ In practice, this means that emails you
send to my email address might go unanswered for a rather long period of time.
Before sending me a message, you are encouraged to consult CCLE, as well as this
syllabus, as your question(s) might already be answered there.

If you feel that your question/issue has not been addressed in the documents
listed above, do feel free to send me a message, however keep the following in
mind:

*   I receive a high volume of messages throughout the day; in most cases it
    will be faster for you to get the information you seek if you reach out to
    me in person (say during O.H., or right before/after lecture).
*   Messages with special keywords _skip_ my inbox. Use this to your advantage:
    if you add `Pic 10b` to your subject line, your message will find its way
    into a special folder that I check periodically. In most cases this reduces
    the time you have to wait before I reply to it.
*   Messages with special attachments, more specifically text files with
    specific file extensions (_e.g.,_ `.cpp`, `.h`, etc.), as well as messages
    containing some types of `.pdf` attachments, are sent directly to my _trash_
    folder.

    > This inconvenience brought to you by students that are under the
    > impression that deadlines do not apply to them (_e.g.,_ they attempt to
    > _late-submit_ their assignments via email attachments).

## Grading

_Grading method:_ pop quizzes (**Quiz**), homework assignments (**Hw**), midterms
(**Mid1**, **Mid2**), and final exam (**Final**).

*   _Pop quizzes:_ there will be **at least 10 pop quizzes** throughout the quarter.
    Each quizz will be worth **up to 2 full percentage points** in the
    computation of your overall final score. **There will be no advanced
    notice** of when these quizzes will take place, but notice (see grading
    schemas below) that in principle, you can miss up to 5 of them and still
    qualify for a 100% overall score.

    > Note: the maximum that can be earned in this category is 10%. That is, if
    > you answer correctly 6 or more of these quizzes, you can only be awarded
    > the maximum score of 10% as opposed to 12% or more.

*   _Homework assignments:_ there will be 7 or 8 homework assignments throughout
    the quarter. **No late homework will be accepted** for any reason, but
    **your lowest homework score will not count** towards the computation of
    your overall score.

*   _Midterm:_ two fifty-minute midterms will be given on **April 22 (Monday
    week 4)**, and **May 20 (Monday week 8)** from **12:00 to 12:50 pm** at **MS
    5200**.

*   _Final exam:_ this exam will be given on **Monday, June 10** from **3:00
    to 6:00 pm** at **TBA**.  

    [Click here for up-to-date information about the exam location][final-tba].  

    > **Important:**  
    > 
    > _Failure to take the final exam during this time will result in an
    > automatic F!_

[final-tba]: https://sa.ucla.edu/ro/public/soc/Results/ClassDetail?term_cd=19S&subj_area_cd=COMPTNG&crs_catlg_no=0010B+++&class_id=157051210&class_no=+002++

Your final score in the class will be the maximum of the following two grading
breakdowns:

|                                                                        |  
|:----------------------------------------------------------------------:|  
| 10% **Quiz** + 20% **Hw** + 20% **Mid1** + 20% **Mid2** + 30% **Final** |  
| or |  
| 10% **Quiz** + 20% **Hw** + 30% **Highest Midterm** + 40% **Final** |  


Overall scores determines letter grades according to the table below.

|                       |                      |                    |  
|:----------------------|:---------------------|:-------------------|  
| A+ (N/A)              | A (93.33% or higher) | A- (90% -- 93.32%) |  
| B+ (86.66% -- 89.99%) | B (83.33% -- 86.65%) | B- (80% -- 83.32%) |  
| C+ (76.66% -- 79.99%) | C (73.33% -- 76.65%) | C- (70% -- 73.32%) |  

The remaining grades will be assigned at my own discretion. Please do not
request exceptions.

> **Note:** students taking this class on a P/NP basis that attain an
> overall score in the C- range **are not** guaranteed a P letter grade.

**All grades are final when filed by the instructor on the Final Grade Report.**


### Policies and procedures (exams)

Make sure to bring your UCLA ID card to every exam. You will not be allowed to
consult books, notes, the internet, digital media or another student's exam.
Please turn off and put away any electronic devices during the entire duration
of the exam.

**There will be absolutely no makeup midterms under any circumstances.**
However, notice that this class **features a dual grading schema** for the
purposes of grade computations. This makes it possible for students to earn a
100% overall final score even they miss one midterm exam.

Exams will be returned to you during discussion section and your TA will go over
the exam at that time. _Any questions regarding how the exam was graded **must
be submitted in writing** with your exam to the TA at the end of section that
day._ No regrade requests will be allowed afterwards regardless of whether or
not you attended section. Please get in touch with me if you anticipate missing
section due to a family emergency or a medical reason.


### Policies and procedures (assignments)

Links to programming assignment descriptions, as well as submission pages, will
be available at the class website once I deem we have covered the material you
will need to successfully complete them. Once a specific assignment is
_available to the class_ you will have **at least one week to complete it**.

Prior to the assignment due dates I usually conduct an informal poll (during
lecture) to determine whether or not an extension might be appropriate. If this
is the case, I will quietly change the CCLE submission settings for the
corresponding assignment. In particular, this means that I will likely not send
an official announcement to the class. You are encouraged to check the CCLE
submission pages on a regular basis to find out about changes (if any) to
assignment due dates.

_In an effort to be fair to all students, messages sent to my email address that
contain either `.cpp`, `.txt`, `.h`, `.hpp`, as well as some `.pdf` attachments
will automatically be deleted from my inbox._

You are responsible for naming your files correctly and you need to make sure
that you submit them through the proper channels. The 'official' compiler we
will use is the one available at the `laguna.math.ucla.edu` server[^one]. If
your code does not compile against it, you will receive an initial score of
0/20 points, and will earn a penalty of at least 1 pt. To receive credit for
your work, you must **fix the problem** and request an **in person** regrade.
That is, you need to make arrangements to meet with me and have your working
code readily available for me to accept your new submission. This meeting should
take place within 7 days of you being informed that your project did not
compile.

> **Note:** 
>
> In previous courses I had in place a system where students could request
> regrades by sending code to a _special_ email address. This system **will not
> be used** for this course. Any regrade will have to be requested in person.


**Penalties and timing of regraded scores**

*   If the issue with your code is something minor (_e.g.,_ a missing library).
    The maximum possible deduction is 1 point. 

*   If the changes needed to make the project compile are significant, a minimum
    penalty of 2 points will be assessed to the overall score.
 
*   Working projects submitted by other students will be graded before ones that
    have _been fixed_. In practice, this means that you will not get to know
    your score until the end of the quarter. Although this seems a bit harsh,
    you can avoid this scenario by always making sure your project compiles
    against the official compiler before you submit your files via CCLE.

To avoid potential unnecessary delays and/or penalties, you are encouraged to
always test your code via the tester suites (zip files) that will be made
available with every assignment.

[^one]: The official compiler is `g++ (SUSE Linux) 4.8.3 20140627
  [gcc-4_8-branch revision 212064]` as of this writing.

As pointed out before, scores on homework assignments and exams will appear on
MyUCLA. It is your responsibility to verify in a timely manner that the scores
appearing there are accurate. If you believe your homework has been graded
incorrectly, or that your score was not correctly recorded, you must bring this
to the attention of the instructor as soon as possible. I reserve the right to
asses a _tardy_ penalty on requests that are not made within 7 calendar days of
the date the scores were recorded.


## About coding integrity

You are encouraged to discuss aspects of the course with other students as well
as homework assignments with others in general terms (_i.e.,_ general ideas and
words are OK but code is not OK). If you need specific help with your programs
you may consult any of the course TAs, or the course instructor. Copying major
parts of a program or document written by someone else (_e.g.,_ code found
online) should be avoided in general. Under certain circumstances --for example,
when starting a project from a template, or when trying to implement an
idea/algorithm presented to you during lecture/DS, and/or found online--,
copying is not _frowned upon_, but actually encouraged. If you believe this
scenario applies to you, you should disclose the source/author of the code you
are including in your project. For example, you can write a comment a long the
lines of

~~~~~ {.cpp}
// The following code is taken from www.some.web-site.edu/found-it/online
// where "Elmer H. Omero" uses it to solve the P = NP problem.
~~~~~


## Accommodations

If you need any accommodations for a disability, please contact the UCLA
CAE[^two] [(`www.cae.ucla.edu`)][CAE]. Make sure to let me know as soon as
possible about necessary arrangements that need to be made.

[CAE]: http://www.cae.ucla.edu
[^two]: Center for Accessible Education

|                                                      |
|:----------------------------------------------------:|
| Course Syllabus Subject to Update by the Instructor. |

