# Weekly summaries

Here is a brief description of the topics we have discussed during this quarter.

## Week 10

*   Friday
    -   _Min heaps_ and _priority queues_: terminology & common operations.
    -   _Min heaps_: analysis of its only two operations; _enter_ & _leave_.
    -   A sorting algorithm: create a _min-heap_, then destroy it.
    -   Other sorting algorithms: demos & resources.
*   Wednesday
    -   _Binary Search Trees_: analysis of common operations; removing a node.
    -   _Binary Search Trees_: recursive algorithms; _e.g.,_ computing the
        height of a tree, counting the number of nodes, traversals (pre-order,
        post-order, in-order).
    -   A sorting algorithm: create a _binary search tree_, then traverse it
        _in-order_.
*   Monday
    -   _Binary Search Trees_: analysis of common operations; _e.g.,_
        _insertion_, _searches_, findind the _min/max_ values, finding the _next
        in order_.

## Week 9

*   Friday
    -   Container adapters: _stacks_ and _queues_.
    -   _Binary Search Trees_: terminology.
*   Wednesday
    -   _Linked Lists_.
*   Monday
    -   Memorial day holiday.

## Week 8

*   Friday
    -   Recurrence relations: analysis of recursive linear search.
    -   Recurrence relations: analysis of binary search.
    -   Introduction to Linked Lists.
    -   Pop quiz 7.
*   Wednesday
    -   Template classes + 3 file layout = Link error
    -   Recurrence relations: iterating to obtain a non-recurrent relation.
*   Monday
    -   Midterm 2.

## Week 7

*   Friday
    -   Recursion.
    -   Analysis of recursive algorithms.
*   Wednesday
    -   Template classes: converting a non-template class into a template class.
    -   Workaround to template member function specialization: _have the member
        rely on a non-member and then specialize the non-member_.
*   Monday
    -   Template functions: the case for one _template_ function to rule them
        all.
    -   Specialization of template functions.

## Week 6

*   Friday
    -   A non-template vector class: the `push_back(double)` function, the
        _square brackets_ operator _hybrid_ functions.
    -   An introduction to template functions.
    -   Pop quiz 5.
*   Wednesday
    -   A non-template vector class: the assignment operator, the
        `push_back(double)` function.
*   Monday
    -   A non-template vector class: the default constructor, destructor and
        copy constructor.

## Week 5

*   Friday
    -   Fixing the `BasicNode` class: coding class specific big 4.
    -   A non-template vector class: figuring out the member fields.
*   Wednesday
    -   The big 4: the _default_ behavior (see _the big 4_ handout as well as
        the _home-made_ `Thing` class).
    -   The big 4: the case against the _default_ behavior.
*   Monday
    -   Mathematical definition of _Big Oh_ notation.
    -   Examples of code complexity.

## Week 4

*   Friday
    -   Searching algorithms: _linear search_
    -   Searching algorithms: _binary search_
    -   Introduction to algorithm complexity and _Big Oh_ notation.
    -   Pop quiz 4
*   Wednesday
    -   `static` member fields.
    -   `static` member functions.
    -   The big 3: we coded primitive versions of these functions; for the most
        part they simply display a message (see _home-made_ `Cosa` class).
*   Monday
    -   Midterm 1.

## Week 3

*   Friday
    -   Pop quiz 3.
    -   Use of initialization lists in class constructors.
    -   `static` local variables.
    -   `static` member fields.
    -   Counting objects of a class via a `static` counter.
    -   `static` member functions (to be continued after midterm).
*   Wednesday
    -   Introduction to _The Big 3_: copy constructor, assignment operator, and
        destructor.
    -   Boolean operators reuse the `Fraction::compare()` function.
    -   Implicit conversion of explicit parameters via constructors.
    -   The `explicit` keyword.
*   Monday
    -   Single point of maintenance: _reuse_, _reuse_, _reuse_.
    -   Shortcut-like member operators: `+=`, `*=`, etc.
    -   Pre-fix and post-fix increment/decrement operators: `++`, `--`.

## Week 2

*   Friday
    -   Testing projects against the official course compiler.
    -   Member unary operators: `Fraction Fraction::operator-()` (unary minus)
    -   Member _vs_ non-member operators: _implicit conversions_.
*   Wednesday
    -   Overloading `operator<<`  to _print_ `Fraction` objects.
    -   Non-member binary operators: `Fraction operator+( Fraction, Fraction )`
        +   The `friend` keyword.
    -   Member binary operators: `Fraction Fraction::operator*( Fraction )`
    -   Self-commenting _vs_ "smart" code.
*   Monday
    -   Passing stream objects to functions.
        +   By value: not possible, the function that makes copies of these
        objects has been _deleted_.
        +   By reference: OK.
    -   Default value parameters.
    -   A possible signature for the _shift left_ operator (`<<`).

## Week 1

*   Friday
    -   Common programming mistakes: failing to be `const` correct.
    -   Return by reference.
    -   Overloading by `const` modifier: functions that have same return number
        and types of parameters, as well as same return type, are distinguished
        by the presence/absence of the `const` modifier.
*   Wednesday
    -   Overloading by number of parameters: _default_, _1-parameter_, and
        _2-parameter_ constructors of the `Fraction` class.
    -   C++ reserved words: `struct` _vs_ `class`.
    -   Common programming mistakes: _variable shadowing_.
    -   Common programming mistakes: copy/paste code leads to copy/paste _bugs_.
*   Monday
    -   Course overview
    -   Syllabus overview
    -   Example: using `make` to compile a project.

---

[Return to main course website][back]

[back]: ..
