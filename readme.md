# PIC 10B: Intermediate programming (spring 2019)

This is to be considered the class website. Here you will find handouts, slides,
and code related to the concepts we discuss during lecture.

## Sections

*   [The class syllabus][syllabus] ([`.pdf` version][syllabus-pdf])
*   [Walk-throughs & handouts][handouts]
*   [Lecture summaries][summary]
*   [_Home-made_ classes][classes]
*   [List of topics & other reading material][links]
*   [Practice material: old exams and exercises][practice]
*   Course assignments
    -   [Assignment descriptions and companion files][hw]
    -   [Submission links and test suites][hw-ccle]

[syllabus]: syllabus/
[syllabus-pdf]: syllabus/readme.pdf
[handouts]: handouts/
[summary]: summaries/
[classes]: home-made-classes/
[links]: topics-and-resources/
[not-found]: https://kualo.co.uk/404
[practice]: https://ccle.ucla.edu/course/view/19S-COMPTNG10B-2?section=4
[hw]: assignments/
[hw-ccle]: https://ccle.ucla.edu/course/view/19S-COMPTNG10B-2?section=2

## Additional resources

Here are other sites associated to our course:

*   [The CCLE class website][CCLE]: this will be used only for class
    announcements, and homework submissions.

*   [Discussion section material (examples, code, etc.)][ds]: Your TA might
    decide to post discussion material for the beneffit of the class as a whole.

[ds]: https://ccle.ucla.edu/course/view.php?id=73519&section=3

[CCLE]: https://ccle.ucla.edu/course/view/19S-COMPTNG10B-2
