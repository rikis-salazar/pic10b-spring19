# Handout: data files created via file streams in Visual Studio&reg;

This handout should help you figure out

*   where to place text files that are meant to be read from within a
    project (_e.g.,_ in assignment 1, you are to read _grades_ from
    `xibalba.txt`), and
*   how to read (_i.e.,_ parse) information from a text file assuming you know
    its structure.

The basic idea is to send some info to a file, then extract a portion of that
info from the same file. Along the way we will learn where VS stores files
associated to a file stream via a _relative path_.


## Your working directory structure

While helping you keep track of a programming project, IDE's create auxiliary
files/folders and place them within a directory hierarchy. For example, Visual
Studio&reg; 2017 might create a structure similar to

    <base folder>
    |
    |-- <solution folder>
    |   |-- Debug                    /* Your compiled programs live here... */
    |   |-- Release                  /* or here, depending on your setup.   */
    |   |
    |   |-- <project folder>         /* Sources & local libraries live here */
    |   |   |-- Debug
    |   |   |-- Release
    |   |   |-- <source file>.cpp
    |   |   |-- <project folder>.vcxproj
    |   |   |-- <project folder>.vcxproj.filters
    |   |
    |   |-- <project folder>.sln
    |
    |-- ... /* Other solution folders */

where `<base folder>`, `<solution folder>`, and `<project folder>` are related
to the entries of the text fields _Location:_, _Name:_, and _Solution name:_
that appear in the _New Project_ window that pops up when you start a new
project (see figure 1 below).

| ![fstream-images/01-base-soln-proj.png](fstream-images/01-base-soln-proj.png) |  
|:-----:|  
| Figure 1: New project pop up window in VS 2017. |

For this handout, I created a new project with the following values:

*   `<base folder> = C:\Users\User\Source\Repos`, which will appear as _Ricardo
    Salazar_ &rarr; _Source_ &rarr; _Repos_ in the file explorer.
*   `<solution folder> = SolutionDir`
*   `<project folder> = ProjectDir`

With this details in hand, we are ready to tackle the problem of locating files
created with `std::fstream` objects.


## Part 1: Creating a file via `std::ofstream`

Consider the following [incomplete] program

~~~~~ {.cpp}
#include <fstream>
#include <string>
...

int main() {
    // Create an output file...
    int year = 2018;
    const double PI = 3.14;
    std::string message = "Ash nazg durbatuluk,\nash nazg gimbatul...";

    std::ofstream fout;
    fout.open("data-file.txt");
    fout << "Year: " << year << '\n';
    fout << "Pi =\t" << PI << '\n';
    fout << "'The one ring' had the inscription:\n" << message << '\n';
    fout << "Wich translates to:\n"
         << "One ring to rule them all,\none ring to find them...\n";
    fout.close();
    ...

    return 0;
}
~~~~~

that sends different variable/object types (_e.g.,_ `int`, `double`, and
`std::string`/`c-string`) to a file named `data-file.txt`.


After compiling and running this program, there should be a text file named
`data-file.txt` somewhere within the directory structure detailed above. This
location is known as the **working directory**[^one]. To find this directory
all we have to do is to perform a file search. In my case, windows listed only
one file with this name; moreover, it gave me the option (via right click) to
_Open file location_ (See Figure 2 below). 

[^one]: Different IDEs might use a completely different location. You are
  encouraged to consult your IDE's documentation to find out details about your
  specific setup

| ![fstream-images/02-project-dir.png](fstream-images/02-project-dir.png) |  
|:-----:|  
| Figure 2: The working directory for my VS setup. Note that the base directory uses an alias.|


## OK, but... why does this matter?

In one [or more] of your assignments, you will be asked to read information from
a file that I will provide. In order for you to be able to test your projects
locally (_i.e.,_ in your own device), you will need to place a copy of the input
files in your working directory. Failure to do so might prevent you from testing
your project correctly.

> **Note:** You do not have to worry about the _working directory_ for the
> PIC web server setup. I will instruct the compiler where to find the input
> files and/or where to store any output files your project might produce.


## Part 2: Reading from a file via `std::ifstream`

The basic idea here is that essentially the input file _plays the role_ of a
very impatient user that provides all the information it knows in one single
interaction with your program.

One disadvantage of this, is that we will have to deal with _buffering_. On the
other hand, a clear advantage is that this _user_ will always be very reliable;
impatient, but reliable.


### Making sense of the data in the text file

This is in general a rather difficult problem[^two] if we cannot control the way
the input files are generated. Luckily ~~for us~~ for you, I will always tell
you in advance what to expect from these files.

[^two]: This problem is known as _parsing_.

For this handout, let us assume we want to recover specific values we sent
to [`data-file.txt`][df]. For convenience, here are the contents of the file:

~~~~~ {.numberLines}
Year: 2018
Pi =	3.14
'The one ring' had the inscription:
Ash nazg durbatuluk,
ash nazg gimbatul...
Wich translates to:
One ring to rule them all,
one ring to find them...
~~~~~

The information we want is summarized below

*   The year: 2nd _field_ in line 1
*   The value of &pi;: 3rd _field_ in line 2
*   The _black speech_ text: Lines 4, and 5.

The _trick_ is to use appropriate variables to either _ignore_ or _capture_
specific values. 

> **Please note:** In our case this will be enough because we know the structure
> of the text file. If this is not the case, the extraction of information is
> not trivial.

Here is a snippet that works in our case

~~~~~ {.cpp}
    std::string dummy_str;
    char dummy_ch;
    int the_year;
    double pi;
    const std::string BLNK = " ";
    std::string the_message;

    std::ifstream fin;
    fin.open("data-file.txt");

    fin >> dummy_str;                // "Year:" is ignored.
    fin >> the_year;                 // Captures 2018
    fin >> dummy_str >> dummy_ch;    // "Pi" and '=' are ignored.
    fin >> pi;                       // Captures 3.14

    getline(fin, dummy_str);         // Ignores end of 2nd line, as
    getline(fin, dummy_str);         // well as contents of 3rd line.
    getline(fin, dummy_str);         // Captures 1st part of phrase.

    the_message += dummy_str + BLNK;

    getline(fin, dummy_str);         // Captures 2nd part of phrase.

    the_message += dummy_str;
    fin.close();
~~~~~


For your convenience [here is the full program][fp]. It contains a small section
at the end that displays the information read from the file to the console.


[df]: fstream-sample-files/data-file.txt
[fp]: fstream-sample-files/input-output.cpp




## Troubleshooting

If for some reason you are not able to complete this walk through, make sure to
get in touch with us to help you correct any issues.


---

[Return to main PIC course website][main]

[main]: ../..

