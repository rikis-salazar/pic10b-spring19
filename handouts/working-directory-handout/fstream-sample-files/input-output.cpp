#include <iostream>
#include <fstream>
#include <string>

int main() {
    // Create an output file...
    int year = 2018;
    const double PI = 3.14;
    std::string message = "Ash nazg durbatuluk,\nash nazg gimbatul...";

    std::ofstream fout;
    fout.open("data-file.txt");
    fout << "Year: " << year << '\n';
    fout << "Pi =\t" << PI << '\n';
    fout << "'The one ring' had the inscription:\n" << message << '\n';
    fout << "Wich translates to:\n"
         << "One ring to rule them all,\none ring to find them...\n";
    fout.close();

    // We want (from input file):
    //    * The year.- the 2nd field in line 1.
    //    * The value of pi.- the 3rd field in line 2.
    //    * The message: lines 4 and 5, concatenated.
    std::string dummy_str;
    char dummy_ch;
    int the_year;
    double pi;
    const std::string BLNK = " ";
    std::string the_message;

    std::ifstream fin;
    fin.open("data-file.txt");

    fin >> dummy_str;                // "Year:" is ignored.
    fin >> the_year;                 // Captures 2018
    fin >> dummy_str >> dummy_ch;    // "Pi" and '=' are ignored.
    fin >> pi;                       // Captures 3.14

    getline(fin, dummy_str);         // Ignores end of 2nd line, as
    getline(fin, dummy_str);         // well as contents of 3rd line.
    getline(fin, dummy_str);         // Captures 1st part of phrase.

    the_message += dummy_str + BLNK;

    getline(fin, dummy_str);         // Captures 2nd part of phrase.

    the_message += dummy_str;
    fin.close();

    std::cout << "INFO FROM FILE\n"
              << "The year: " << the_year << '\n'
              << "Value of pi: " << pi << '\n'
              << "Inscription: " << the_message << '\n';
    return 0;
}
