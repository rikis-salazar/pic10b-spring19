# Template classes

This is a set of sections that will eventually be turned into a more complete
handout. It is based on the code we discussed during lecture regarding our
`SimplePair` class.


## The problem

Give a non-template class, we want to add two template parameters to make the
class more general.

Here is our starting point (non-template class interface)

~~~~~ {.cpp}
class SimplePair{
  private:
    int first;
    std::string second;
  public:
    // Constructors
    SimplePair();
    SimplePair(const int&, const std::string&);
    // Getters & Setter
    int get_first() const;
    std::string get_second() const;
    void set_values(const int&, const std::string&);
    // A member operator
    SimplePair& operator+=(const SimplePair&);
    // A non-member friend operator
  friend std::ostream& operator<<(std::ostream&, const SimplePair&);
};
~~~~~

here are the member function definitions

~~~~~ {.cpp}
SimplePair::SimplePair(){
    first = 0;
    second = "";
}
SimplePair::SimplePair(const int& f, const std::string& s){
    first = f;
    second = s;
}
int SimplePair::get_first() const {
    return first;
}
std::string SimplePair::get_second() const {
    return second;
}
void SimplePair::set_values(const int& f, const std::string& s){
    first = f;
    second = s;
}
SimplePair& SimplePair::operator+=(const SimplePair& rhs) {
    first += rhs.first;             // addition
    second += "-" + rhs.second;     // concatenation
    return *this;
}
~~~~~

and here are the non-member function definitions

~~~~~ {.cpp}
std::ostream& operator<<(std::ostream& out, const SimplePair& rhs){
    out << "(" << rhs.first << "," << rhs.second << ")";
    return out;
}
void print_values(const SimplePair& p){
    std::cout << "\tfirst: " << p.get_first()
              << "\tsecond: " << p.get_second() << "\n";
}
~~~~~

In `main` we test this class via the following code

~~~~~ {.cpp}
    SimplePair p0;
    SimplePair p1(1,"Uno");
    SimplePair p2 = SimplePair();
    p2.set_values(2,"Dos");
    . . .
    print_values(p0);
    . . .
    SimplePair p = p1;
    p += p2;
    . . .
    std::cout << p;
    . . .
~~~~~

> [Use this `cpp.sh` link to test the code above][link1], or [click here to
> access a file that you can compile in your favorite IDE][file1].

[link1]: http://cpp.sh/65yrl
[file1]: test-code/test-01.cpp


## Adding a template parameter

Similar to classes, we need to place a `template <typename F>` statement right
before the class interface.

~~~~~ {.cpp}
template <typename F>
class SimplePair{
    . . .
};
~~~~~

and similar to classes, the scope is limited to the class interface. However
unlike functions (which get to keep their old names), template classes lose
their old names. In this particular case, the identifier `SimplePair` no longer
refer to a class (as that class no longer exists). Instead, we have to use the
new name, namely `SimplePair<F>`.

This brings up the first issue: on the one hand, the default constructor should
now start with the following line of code

~~~~~ {.cpp}
SimplePair<F>::SimplePair(){
~~~~~

On the other hand, the identifier `F` is meaningless because this definition is
not within the class interface. In other words, if we attempt to compile the
code above, the compiler will not be able to do its job properly because `F`
does not name a valid expression.

To make sense of this identifier (which is replacing the `int` type in the
original non-template class), we need to add a `template <typename F>` statement
right before the default constructor definition.

~~~~~ {.cpp}
template <typename F>
SimplePair<F>::SimplePair(){
    . . .
}
~~~~~

Similarly, the addition of the `template <typename F>` is also needed right
before the definitions of the two parameter constructor, as well as other
members of the class. For example, for the `get_second` getter we should have

~~~~~ {.cpp}
template <typename F>
std::string SimplePair<F>::get_second() const {
    return second;
}
~~~~~

Lastly, since non-members might also deal with the new `SimplePair<F>` class,
they might need to be turned into template functions. For example, the
`print_values` function should now look like this:

~~~~~ {.cpp}
template <typename F>
void print_values(const SimplePair<F>& p){
    std::cout << "\tfirst: " << p.get_first()
              << "\tsecond: " << p.get_second() << "\n";
}
~~~~~

A similar change should also apply to the overload of `operator<<` that _prints_
objects of the `SimplePair`class.

Next up, we should make sure the appropriate instances of `int` are replaced
with the more generic type `F`. For example, in the class interface the
following changes are needed.

~~~~~ {.cpp}
template <typename F>
class SimplePair{
  private:
    F first;    // <-- Replaces original `int first;` line.
    . . .
  public:
    . . .
    SimplePair(const F&, const std::string&);  // <-- Generic 2-param ctor.
    . . .
    F get_first() const; // <-- `first` might no longer be an int.
    void set_values(const F&, const std::string&); // <-- Generic setter.
    . . .
};
~~~~~

The member functions `get_first` as well as `set_values` need the following
changes:

~~~~~ {.cpp}
template <typename F>
F SimplePair<F>::get_first() const {
// int SimplePair<F>::get_first() . . .  // `first` might not be `int` compatible
    return first;
}
template <typename F>
void SimplePair<F>::set_values(const F& f, const std::string& s){
// void SimplePair<F>::set_values(const int& f, . . . )
// `f` might not be int, but it is the same type as `first`.
    first = f;
    second = s;
}
~~~~~

As a last step, we need to replace all instances of `SimplePair` in `main` with
the appropriate template objects. After these changes almost everything works as
expected. There seems to an issue with the  overload of `operator<<`[^one];
however, if we restrict ourselves to _printing_ `SimplePair<int>` objects via
the non-member `to_cout`, things seems OK (although we do get a _compiler
warning_, as well as a _note_).

[^one]: The current calls to this version of `operator<<` do not compile. This
  is because the combination _friend_ + _template_ is a delicate one. Here we
  will take the easy way out: we will no longer let `operator<<` be a _friend_
  of our template class. During lecture I briefly demonstrated a possible way to
  fix this issue, but do not expect this topic to show up in any of the
  remaining exams.

> **Subtle point:**
>
> In the default constructor we have to be careful with the statement
>
> ~~~~~ {.cpp}
> first = 0;   // 0 is the default value for `int` type.
> ~~~~~
>
> Since now `first` might not necessarily represent an `int`, we need to come up
> with a way to set the member field to the default value corresponding to
> whatever the template parameter `F` represents.
>
> In the case where `F` is the name of a class (as opposed to a primitive type),
> this is a piece of cake: the expression `F()` represents a call to the default
> constructor for the class `F`; this will certainly do it or us.
>
> Luckily for us, if `F` holds the name of a primitive (like `int`, or
> `double`), the expression `F()` does not represent a call to a default
> constructor, but it does return the default value for `F`. In the case of
> `int`, this value is 0. 
>
> In summary, in our default  constructor we can use the more generic statement
> `first = F();`, as in the following definition
>
> ~~~~~ {.cpp}
> template <typename F>
> SimplePair<F>::SimplePair(){
>     first = F(); // <-- F() is a call to the 'default' value of F.
>     second = ""; // <-- the empty string is the 'default' string
> }
> ~~~~~
>
> Moreover, we can take advantage of an initialization list and write a more
> efficient version like this
>
> ~~~~~ {.cpp}
> template <typename F>
> SimplePair<F>::SimplePair() : first() {
>     //                        ^^^^^^^ A call to the 'default' value of F.
>     second = ""; // <-- the empty string is the 'default' string
> }
> ~~~~~
>
> > Question: _Can you write an even more efficient version?_

> [Use this `cpp.sh` link to test the code above][link2], or [click here to
> access a file that you can compile in your favorite IDE][file2].

[link2]: http://cpp.sh/4kv53
[file2]: test-code/test-02.cpp


## Fixing `operator<<`

As mentioned above, here we will simply remove the declaration of `operator<<`
from the interface of `SimplePair<F>`. This will prevent us from accessing the
member fields `first` and `second` directly, however, we can still access them
(indirectly) via the  public _getters_.

This is how the class interface, plus the member definition look like

~~~~~ {.cpp}
template <typename F>
class SimplePair{
  . . .
  // friend std::ostream& operator<<(std::ostream&, const SimplePair&);
};

. . .

template <typename F>
std::ostream& operator<<(std::ostream& out, const SimplePair<F>& rhs){
    // out << "(" << rhs.first << "," << rhs.second << ")";
    out << "(" << rhs.get_first() << "," << rhs.get_second() << ")";
    return out;
}
~~~~~

> [Use this `cpp.sh` link to test the code above][link3], or [click here to
> access a file that you can compile in your favorite IDE][file3].

[link3]: http://cpp.sh/8dhkx
[file3]: test-code/test-03.cpp

> **Note:**
>
> Although you might have gotten used to ignoring the compiler warnings,
> sometimes they do provide some useful information about what might turn out to
> be a problem in the future. In this case, prior to stripping _private access_
> from `operator<<`, the compiler reported a _warning_ along the lines of
>
> > ... friend declaration [of `operator<<` inside the class interface] declares
> > a non-template function ....
>
> In addition, a _note_ along the lines of
>
> > ... if this is not what you intended, make sure the function template has
> > already been declared and add <> after the function name ...
>
> was issued by the compiler. Whereas it is possible to _fix_ this issue by
> following the instructions from the note, it is much easier in this case to
> simply use the public interface of the class to provide the needed
> functionality in `operator<<`.


## Adding a second template parameter

Just like in the case of the first template parameters, a series of changes are
needed.

*   In the class interface:
    -   update the template declaration, for example, by adding `typename S`
        inside the _angled brackets_.

        ~~~~~ {.cpp}
        template <typename F, typename S>
        class SimplePair {
            . . .
        };
        ~~~~~

    -   replace instances of `std::string` with the template parameter `S` as
        needed. For example, the `get_second`, and `set_values` member function
        declarations should look like this 

        ~~~~~ {.cpp}
            . . . 
          public:
            . . .
            // std::string get_second() const; // `second` might not be a string
            S get_second() const;
            // void set_values(const F&, const std::string&); // See above
            void set_values(const F&, const S&);
            . . .
        ~~~~~

*   In the definition of member, and non-member functions:
    -   update the template declarations and replace `std::string` with `S`
        where needed within the function definitions. For example, `get_second`
        should be defined like this

        ~~~~~ {.cpp}
        template <typename F, typename S>
        S SimplePair<F,S>::get_second() const {
            return second;
        }
        ~~~~~

        and `operator<<` should have the following definition:

        ~~~~~ {.cpp}
        template <typename F, typename S>
        std::ostream& operator<<(std::ostream& out, const SimplePair<F,S>& rhs){
            out << "(" << rhs.get_first() << "," << rhs.get_second() << ")";
            return out;
        }
        ~~~~~

    -   identify places where old code might be too specific for a type, then
        try to write more generic code. For example,

        +   in the default constructor we are currently using the statement
            `second = "";` to initialize a string member field to its default
            value, namely an empty string.

            Notice that in this scenario, the same task can actually be achieved
            with the statement `second = std::string();`, which is an explicit
            call to the default constructor. Moreover, if we interpret
            `std::string` as the name of a type, we can write a more generic
            version that calls the default constructor for an arbitrary type.
            The following version of the default constructor should do the
            trick.

            ~~~~~ {.cpp}
            template <typename F, typename S>
            SimplePair<F,S>::SimplePair() : first() {
                // second = "";   // Too specific
                // second = std::string();   // Better, but still too specific
                second = S();   // Calls default constructor for arbitrary S
            }
            ~~~~~

        +   in member `operator+=` the statement `second += "-" + rhs.second;`
            is again specific for strings. Removing the dash character makes the
            code less specific. However, this gets rid of the specialized
            concatenation behavior we originally had in the non-template
            `SimplePair` class. We will restore this special behavior later.

*   In `main()`:
    -    Replace `SimplePair<int>` with `SimplePair<int,std::string>`, and
    -    [Optional] Add statements that test other types of pairs. _E.g.,_
        `SimplePair<int,double>`, `SimplePair<Fraction,Thing>`, etc.

You are greatly encouraged to code these changes yourself. Take the code from
either the previous section, or the original code from the `SimplePair` class,
then add the template parameters and/or extra lines of code described in this
section. If your implementation is correct, it should behave more or less like
the one in the links below.

> [Use this `cpp.sh` link to test the current version of the `SimplePair<F,S>`
> class][link4], or [click here to access a file with the current state of the
> class][file4].

[link4]: http://cpp.sh/6yb7s
[file4]: test-code/test-04.cpp


## Specialization of template classes

Based on our experience with template functions, we might be tempted to code a
specialized version of `SimplePair<F,S>::operator+=` to _properly_ concatenate
string objects. The basic premise being that we simply need to drop one of the
template parameters, might lead us to this implementation:

~~~~~ {.cpp}
// The generic version
template <typename F, typename S>
SimplePair<F,S>& SimplePair<F,S>::operator+=(const SimplePair<F,S>& rhs) {
   first += rhs.first;
   second += rhs.second;
   return *this;
}

// Specialized version for `SimplePair` objects where the second entry is a
// `std::string` object.
template <typename F>
SimplePair<F,std::string>& SimplePair<F,std::string>::operator+=(
        const SimplePair<F,std::string>& rhs) {
   first += rhs.first;
   second += "-" + rhs.second;     // specialized addition for strings 
   return *this;
}

// Specialized version for `SimplePair` objects where the first entry is a
// `std::string` object.
template <typename S>
SimplePair<std::string,S>& SimplePair<std::string,S>::operator+=(
. . .
~~~~~

However, things do not go as planned. The compiler then complains along the
lines of

~~~~~
. . . error: invalid use of incomplete type ‘class SimplePair<F, . . . >’ . . .
~~~~~

The issue here is that unlike template functions, template classes ~~are dumb~~
cannot be **partially specialized**. We could code specialized versions to
handle specific pairs of types. But this would kind of defeat the purpose of
having a template `SimplePair`. Imagine if you had to provide the following
definitions

~~~~~ {.cpp}
// Specialized version for `SimplePair<int,std::string>`
SimplePair<int,std::string>& SimplePair<int,std::string>::operator+=(
        const SimplePair<int,std::string>& rhs) {
   first += rhs.first;
   second += "-" + rhs.second;     // specialized addition for strings
   return *this;
}

// Specialized version for `SimplePair<double,std::string>`
SimplePair<double,std::string>& SimplePair<double,std::string>::operator+=(
        const SimplePair<double,std::string>& rhs) {
    // Same code as above
    . . .
}

// Specialized version for `SimplePair<Fraction,std::string>`
SimplePair<Fraction,std::string>& SimplePair<Fraction,std::string>::operator+=(
        const SimplePair<Fraction,std::string>& rhs) {
    // Same code as above
    . . .
}

// Oh boy... and to think I still have to code the symmetric versions
// (`SimplePair<std::string,int>`, `SimplePair<std::string,double>`, etc.)
~~~~~

> _¡Oh! ¿Y ahora quién podrá ~~defenderme~~ ayudarnos?_  
> _Yo. ~~El Chapulín Colorado~~ Un operador templete, no miembro,
> especializado._


## _Fake_ partial specialization of template classes

As stated in the previous section, template classes cannot be partially
specialized but template functions can. Based on this fact we can fix the
encountered issues via a two step process:

1.  delegate the _addition_ responsibility of the member `operator+=` to a
    non-member `operator+`, then
1.  code partially specialized versions of this non-member operator.

The first step above is achieved with the following changes:

~~~~~ {.cpp}
template <typename F, typename S>
SimplePair<F,S>& SimplePair<F,S>::operator+=(const SimplePair<F,S>& rhs) {
   *this = *this + rhs;    // Uses non-member operator+ (defined elsewhere)
   return *this;
}
. . .

// Generic addition
template <typename F, typename S>
SimplePair<F,S> operator+( const SimplePair<F,S>& lhs,
                           const SimplePair<F,S>& rhs) {
   F f = lhs.get_first() + rhs.get_first();
   S s = lhs.get_second() + rhs.get_second();
   return SimplePair<F,S>(f,s);
}
~~~~~

You are encouraged to _fix_ the still broken implementation by providing all the
needed specialized overloads of `operator+`. You should then compare your
proposed solution to the one provided in the following links.

> [_Fake_ partial specialization of the `SimplePair<F,S>` class
> (`cpp.sh`).][link5] [Click here to access the code directly.][file5]

[link5]: http://cpp.sh/3va5j
[file5]: test-code/test-05.cpp


## Other things to keep in mind


*   **Templates + three file layout = Link error**

    When coding libraries that use templates, the typical three file layout does
    not work as expected. Basically, the compiler processes the template code,
    and tries its best to determine if it is syntactically correct; however,
    unless the values for the templates parameters are known, no binary code is
    actually generated.

    To avoid this issue, we must make sure that the compiler knows what are the
    specific types we want to use for the template functions or objects.

    -   Fix 1: Including the `.cpp` file instead if the `.h` in the driver.

        This works because an `#include` statement is essentially a _copy/paste_
        operation. The driver file then has a copy of all the template
        definitions (as opposed to simple the declarations), and can then
        generate valid code once it determines the needed template values from
        the code used in the driver.

        This is how the driver looks like with this approach:

        ~~~~~ {.cpp}
        #include <iostream>
        . . .   // Other "Regular" libraries

        // #include "library_with_template_params.h" // Leads to link error
        #include "library_with_template_params.cpp"  // Copies code from file
        . . .   // Other custom libraries with templates

        #include "library_without_template_params.h" // No issues here
        . . .   // Other custom libraries without templates
        . . .

        int main (){
            . . .
            int dummy_int_var = 0;
            call_to_template_fun( dummy_int_var );   // See (***) below
            . . .

            return 0;
        }

        // (***): Once the compiler reaches this line, it can look back at the
        // template code from the appropriate library, and generate the
        // corresponding binary code: the `int` version of the function
        // `call_to_template_fun`.
        ~~~~~

    -   Fix 2: Move the definitions from the `.cpp` file to the `.h` file.

        This is essentially the same fix as the previous one, except that it
        makes the process of including libraries a bit more uniform.

        ~~~~~ {.cpp}
        #include <iostream>
        . . .   // Other "Regular" libraries

        #include "library_with_template_params.h"    // OK!
        #include "library_without_template_params.h" // OK!
        . . .   // Other custom libraries with or without templates

        int main(){
            . . .
            int dummy_int_var = 0;
            call_to_template_fun( dummy_int_var );
            . . .
            return 0;
        }
        ~~~~~

*   **Libraries with templates are often technical in nature**

    In other words, _casual_ users/programmers are not expected to look into
    them. Instead, more experienced programmers are the ones that are likely to
    try analyze/understand it. Moreover they are more likely to try to actually
    locate details abut a specific function. In this case, implementing members
    functions within the class interface is not only expected, but encouraged as
    well[^nine].

    For the example discussed in this handout the class would look more or less
    like this

    ~~~~~ {.cpp}
    template <typename F, typename S>
    class SimplePair{
      public:
        // Constructors
        SimplePair() : first(), second() {}
        SimplePair(const F& f, const S& s) : first(f), second(s) {}

        // Getters & Setter
        F get_first() const { return first; }
        S get_second() const { return second; }
        void set_values(const F& f, const S& s){
            first = f;
            second = s;
        }

        // Member +=
        SimplePair& operator+=(const SimplePair& rhs){
            *this = *this + rhs;
            return *this;
        }

      private:
        F first;
        S second;
    };

    // Generic non-member addition
    template <typename F, typename S>
    SimplePair<F,S> operator+(
            const SimplePair<F,S>& lhs,
            const SimplePair<F,S>& rhs){
       F f = lhs.get_first() + rhs.get_first();
       S s = lhs.get_second() + rhs.get_second();
       return SimplePair<F,S>(f,s);
    }

    // Partially specialized non-member addition: std::string
    . . .

    // Partially specialized non-member addition: std::string
    . . .

    // Specialized non-member addition: std::string
    . . .

    // Non-member shift left operator
    template <typename F, typename S>
    std::ostream& operator<<(std::ostream& out, const SimplePair<F,S>& rhs){
       out << "(" << rhs.get_first() << "," << rhs.get_second() << ")";
       return out;
    }

    // Other functions
    . . .
    ~~~~~

    For your convenience, [here is a link][dir] to a directory containing a
    _two-file-layout_ implementation of the template `SimplePair` class. Notice
    how the use of the `template <typename F, typename S>` has been greatly
    reduced, as it is only needed in the definition of the non-member functions.

    Or, if you prefer [use this other link][s-they-say] to ~~get rick-rolled~~
    watch a clip of _things_  cyclists say.

[dir]: test-code/two-file-layout/
[s-they-say]: https://www.youtube.com/watch?v=GMCkuqL9IcM


[^nine]: Note however that in this case an extra effort should be made to ensure
  that the code is well written (_i.e.,_ readable, self-commenting, etc.).

---

[Return to main PIC course website][main]

[main]: ../..

