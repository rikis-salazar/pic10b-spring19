#include <iostream>
#include <string>


template <typename F, typename S>
class SimplePair{    // Class is now SimplePair<F,S>
  private:
    F first;
    S second;
  public:
    // Constructors
    SimplePair();
    SimplePair(const F&, const S&);

    // Setter & Getters
    void set_values(const F&, const S&);
    F get_first() const;
    S get_second() const;

    // A member operator
    SimplePair& operator+=(const SimplePair&);
};



// Member definitions
template <typename F, typename S>
SimplePair<F,S>::SimplePair() : first(), second() {
    // Fields already initialized at this point with default values.
    // An equivalent implementation would use the statements:
    //     first = F(); // <-- F() is a call to the 'default' value of F.
    //     second = S(); // <-- S() is a call to the 'default' value of S.
    // second = ""; // <-- S might not be a std::string
}

template <typename F, typename S>
SimplePair<F,S>::SimplePair(const F& f, const S& s)
  : first(f), second(s) { }

template <typename F, typename S>
void SimplePair<F,S>::set_values(const F& f, const S& s){
    first = f;
    second = s;
}

template <typename F, typename S>
F SimplePair<F,S>::get_first() const {
    return first;
}

template <typename F, typename S>
S SimplePair<F,S>::get_second() const {
    return second;
}

template <typename F, typename S>
SimplePair<F,S>& SimplePair<F,S>::operator+=(const SimplePair<F,S>& rhs) {
    first += rhs.first;
    second += rhs.second;
    // second += "-" + rhs.second;     // concatenation (might not apply to S)
    return *this;
}



// ***************************************************************
// ************************* NON-MEMBERS *************************
// ***************************************************************

template <typename F, typename S>
std::ostream& operator<<(std::ostream& out, const SimplePair<F,S>& rhs){
    out << "(" << rhs.get_first() << "," << rhs.get_second() << ")";
    return out;
}

template <typename F, typename S>
void print_values(const SimplePair<F,S>& p){
    std::cout << "\tfirst: " << p.get_first()
              << "\tsecond: " << p.get_second() << "\n";
}



// ***************************************************************
// ************************* THE DRIVER **************************
// ***************************************************************
int main(){
    std::cout << "Hello, _paired-up_ world!\n";

    SimplePair<int,std::string> p0;
    SimplePair<int,std::string> p1(1,"Uno");
    SimplePair<int,std::string> p2 = SimplePair<int,std::string>();
    p2.set_values(2,"Dos");

    std::cout << "p0:";
    print_values(p0);
    std::cout << "p1:";
    print_values<int,std::string>(p1);    // OK, but unnecessary and ugly.
    std::cout << "p2:";
    print_values(p2);

    SimplePair<int,std::string> p = p1;
    p += p2;
    std::cout << "p = p1, followed by\np+=p2:";
    print_values(p);

    std::cout << "\nUsing operator<<\n";
    std::cout << "p0:\t" << p0 << "\n";
    std::cout << "p1:\t" << p1 << "\n";
    std::cout << "p2:\t" << p2 << "\n";
    std::cout << "p:\t" << p << "\n";

    return 0;
}


/**

SAMPLE OUTPUT:

Hello, _paired-up_ world!
p0:     first: 0        second: 
p1:     first: 1        second: Uno
p2:     first: 2        second: Dos
p = p1, followed by
p+=p2:  first: 3        second: UnoDos

Using operator<<
p0:     (0,)
p1:     (1,Uno)
p2:     (2,Dos)
p:      (3,UnoDos)

**/
