#ifndef TEMPLATE_PAIR_H
#define TEMPLATE_PAIR_H

#include <iostream>    // std::cout
#include <string>      // std::string


template <typename F, typename S>
class SimplePair{
  public:
    SimplePair() : first(), second() {}
    SimplePair(const F& f, const S& s) : first(f), second(s) {}

    // Getters
    F get_first() const { return first; }
    S get_second() const { return second; }

    // Setter
    void set_values(const F& f, const S& s){
        first = f;
        second = s;
    }

    // Member operator
    SimplePair& operator+=(const SimplePair& rhs){
        *this = *this + rhs;
        return *this;
    }

  private:
    F first;
    S second;
};


// Generic non-member addition
template <typename F, typename S>
SimplePair<F,S> operator+(
        const SimplePair<F,S>& lhs,
        const SimplePair<F,S>& rhs){
   F f = lhs.get_first() + rhs.get_first();
   S s = lhs.get_second() + rhs.get_second();
   return SimplePair<F,S>(f,s);
}

// Partially specialized non-member addition: std::string
template <typename F>
SimplePair<F,std::string> operator+(
        const SimplePair<F,std::string>& lhs,
        const SimplePair<F,std::string>& rhs){
   F f = lhs.get_first() + rhs.get_first();
   std::string s = lhs.get_second() + "-" + rhs.get_second();
   return SimplePair<F,std::string>(f,s);
}

// Partially specialized non-member addition: std::string
template <typename S>
SimplePair<std::string,S> operator+(
        const SimplePair<std::string,S>& lhs,
        const SimplePair<std::string,S>& rhs){
   std::string f = lhs.get_first() + "-" + rhs.get_first();
   S s = lhs.get_second() + rhs.get_second();
   return SimplePair<std::string,S>(f,s);
}

// Specialized non-member addition: std::string
SimplePair<std::string,std::string> operator+(
        const SimplePair<std::string,std::string>& lhs,
        const SimplePair<std::string,std::string>& rhs){
   std::string f = lhs.get_first() + "-" + rhs.get_first();
   std::string s = lhs.get_second() + "-" + rhs.get_second();
   return SimplePair<std::string,std::string>(f,s);
}


// Non-member shift left operator
template <typename F, typename S>
std::ostream& operator<<(std::ostream& out, const SimplePair<F,S>& rhs){
   out << "(" << rhs.get_first() << "," << rhs.get_second() << ")";
   return out;
}


// Non-member helper (for debugging purposes)
template <typename F, typename S>
void print_values(const SimplePair<F,S>& p){
   std::cout << "\tfirst: " << p.get_first()
             << "\tsecond: " << p.get_second() << "\n";
}

#endif
