// Final two-file layout version.

#include <iostream>    // std::cout
#include <iomanip>     // std::boolalpha
#include <string>      // std::string

#include "template_pair.h"

int main(){
  using std::cout;
  using std::string;

    cout << std::boolalpha;
    cout << "Hello, _paired-up_ world!\n";

    SimplePair<int,string> p0;
    cout << "p0:";
    print_values(p0);

    SimplePair<int,string> p1(1,"Uno");
    cout << "p1:";
    print_values<int,string>(p1);    // OK, but unnecessary and ugly.

    SimplePair<int,string> p2(2,"Two"); // Uhm... Spanish, please.
    p2.set_values(2,"Dos");             // Thanks.
    cout << "p2:";
    print_values(p2);

    // A pair that holds the "values" of p1 and p2
    SimplePair<int,string> p = p1;
    p += p2;
    std::cout << "p=p1, followed by\np+=p2:";
    print_values(p);


    cout << "\nSo far, these are the pairs we've created\n";
    cout << "p0:\t" << p0 << "\n";
    cout << "p1:\t" << p1 << "\n";
    cout << "p2:\t" << p2 << "\n";
    cout << "p:\t" << p << "\n";


    cout << "\nBoth parameters could be generic...\n";
    SimplePair<bool,int> p3(true,-1);
    cout << "p3:\t" << p3 << "\n";


    cout << "\n... or strings can be used in first entry.\n";
    SimplePair<string,double> p4("Pi",3.1416);
    SimplePair<string,bool> p5("!Falso",true);
    p5.set_values("!Cierto",false);
    cout << "p4:\t" << p4 << "\n";
    cout << "p5:\t" << p5 << "\n";


    cout << "\nMoreover, addition works as expected:\n";
    cout << "p3+p3:\t" << p3+p3  << "\n";
    cout << "p4+p4:\t" << p4+p4  << "\n";
    cout << "p5+p5:\t" << p5+p5  << "\n";


    cout << "\nEven both types can be strings...\n";
    SimplePair<string,string> p6("Library","Biblioteca");
    cout << "p6:\t" << p6 << "\n";


    cout << "\n... and operator+= works as expected:\n";
    cout << "p6+=p6:\t" << (p6 += p6) << "\n";
    cout << "p6:\t" << p6 << "\n";

   return 0;
}


/**

SAMPLE OUTPUT:

Hello, _paired-up_ world!
p0:     first: 0        second: 
p1:     first: 1        second: Uno
p2:     first: 2        second: Dos
p=p1, followed by
p+=p2:  first: 3        second: Uno-Dos

So far, these are the pairs we've created
p0:     (0,)
p1:     (1,Uno)
p2:     (2,Dos)
p:      (3,Uno-Dos)

Both parameters could be generic...
p3:     (true,-1)

... or strings can be used in first entry.
p4:     (Pi,3.1416)
p5:     (!Cierto,false)

Moreover, addition works as expected:
p3+p3:  (true,-2)
p4+p4:  (Pi-Pi,6.2832)
p5+p5:  (!Cierto-!Cierto,false)

Even both types can be strings...
p6:     (Library,Biblioteca)

... and operator+= works as expected:
p6+=p6: (Library-Library,Biblioteca-Biblioteca)
p6:     (Library-Library,Biblioteca-Biblioteca)

**/
