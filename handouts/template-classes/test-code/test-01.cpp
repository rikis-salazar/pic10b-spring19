#include <iostream>
#include <string>

// Interface
class SimplePair{
  private:
    int first;
    std::string second;
  public:
    // Constructors
    SimplePair();
    SimplePair(const int&, const std::string&);
    // Getters & Setter
    int get_first() const;
    std::string get_second() const;
    void set_values(const int&, const std::string&);
    // A member operator
    SimplePair& operator+=(const SimplePair&);
    // A non-member friend operator
  friend std::ostream& operator<<(std::ostream&, const SimplePair&);
};



// Member Definitions
SimplePair::SimplePair(){
    first = 0;
    second = "";
}

SimplePair::SimplePair(const int& f, const std::string& s){
    first = f;
    second= s;
}

int SimplePair::get_first() const {
    return first;
}

std::string SimplePair::get_second() const {
    return second;
}

void SimplePair::set_values(const int& f, const std::string& s){
    first = f;
    second = s;
}

SimplePair& SimplePair::operator+=(const SimplePair& rhs) {
    first += rhs.first;             // addition
    second += "-" + rhs.second;     // concatenation
    return *this;
}



// Non-member definitions
std::ostream& operator<<(std::ostream& out, const SimplePair& rhs){
    out << "(" << rhs.first << "," << rhs.second << ")";
    return out;
}

void print_values(const SimplePair& p){
    std::cout << "\tfirst: " << p.get_first()
              << "\tsecond: " << p.get_second() << "\n";
}



// Driver
int main(){
    std::cout << "Hello, _paired-up_ world!\n";

    SimplePair p0;
    SimplePair p1(1,"Uno");
    SimplePair p2 = SimplePair();
    p2.set_values(2,"Dos");

    std::cout << "p0:";
    print_values(p0);
    std::cout << "p1:";
    print_values(p1);
    std::cout << "p2:";
    print_values(p2);

    SimplePair p = p1;
    p += p2;
    std::cout << "p = p1, followed by p+=p2:";
    print_values(p);

    std::cout << "\nUsing operator<<\n";
    std::cout << "p0:\t" << p0 << "\n";
    std::cout << "p1:\t" << p1 << "\n";
    std::cout << "p2:\t" << p2 << "\n";
    std::cout << "p:\t" << p << "\n";

    return 0;
}


/**

SAMPLE OUTPUT:

Hello, _paired-up_ world!
p0:	first: 0	second: 
p1:	first: 1	second: Uno
p2:	first: 2	second: Dos
p = p1, followed by p+=p2:	first: 3	second: Uno-Dos

Using operator<<
p0:	(0,)
p1:	(1,Uno)
p2:	(2,Dos)
p:	(3,Uno-Dos)

**/
