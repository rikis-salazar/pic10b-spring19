#include <iostream>
#include <string>


template <typename F>      // The class is now SimplePair<F>
class SimplePair{
  private:
    F first;
    std::string second;

  public:
    // Constructors
    SimplePair();
    SimplePair(const F&, const std::string&);

    // Setter & Getters
    void set_values(const F&, const std::string&);
    F get_first() const;
    std::string get_second() const;

    // A member operator
    SimplePair& operator+=(const SimplePair&);

  // A non-member operator
  friend std::ostream& operator<<(std::ostream&, const SimplePair&);
};



// Member definitions
template <typename F>
SimplePair<F>::SimplePair(){
    first = F(); // <-- F() is a call to the 'default' value of F.
    second = ""; // <-- the empty string is the 'default' string
}

template <typename F>
SimplePair<F>::SimplePair(const F& f, const std::string& s)
  : first(f), second(s) { }  // <-- Initializer list & empty body

template <typename F>
void SimplePair<F>::set_values(const F& f, const std::string& s){
    first = f;
    second = s;
}

template <typename F>
F SimplePair<F>::get_first() const {
    return first;
}

template <typename F>
std::string SimplePair<F>::get_second() const {
    return second;
}

template <typename F>
SimplePair<F>& SimplePair<F>::operator+=(const SimplePair<F>& rhs) {
    first += rhs.first;
    second += "-" + rhs.second;     // concatenation
    return *this;
}


// ***************************************************************
// ************************* NON-MEMBERS *************************
// ***************************************************************
template <typename F>
std::ostream& operator<<(std::ostream& out, const SimplePair<F>& rhs){
    out << "(" << rhs.first << "," << rhs.second << ")";
    return out;
}

template <typename F>
void print_values(const SimplePair<F>& p){
    std::cout << "\tfirst: " << p.get_first()
              << "\tsecond: " << p.get_second() << "\n";
}




// ***************************************************************
// ************************* THE DRIVER **************************
// ***************************************************************
int main(){
   std::cout << "Hello, _paired-up_ world!\n";

   // Template classes are not smart. They need to be told the types of the
   // template parameters.
   SimplePair<int> p0;
   SimplePair<int> p1(1,"Uno");
   SimplePair<int> p2 = SimplePair<int>();

   // Member functions are stuck with their non-template name.
   //     p2.set_values<int>(2,"Dos");     // Oops! Cannot use template name.
   //
   // In this case, the compiler confuses the left angle bracket `<` with an
   // attempt to call `operator<`.

   // In a sense template functions are smart. That is, there is no need to
   // specify template parameter.
   p2.set_values(2,"Dos");

   // Non-member template functions are smart...
   std::cout << "p0:";
   print_values(p0);

   // ... and they do not get confused if the template type is provided...
   std::cout << "p1:";
   print_values<int>(p1);

   std::cout << "p2:";
   print_values(p2);
   // ... as long as there are no conflicts or ambiguity.
   //     print_values<double>(p2); // Uncommenting this produces error!

   SimplePair<int> p = p1;
   p += p2;
   std::cout << "p = p1, followed by p+=p2:";
   print_values(p);

   /**
   std::cout << "\nUsing operator<<\n";
   std::cout << "p0:\t" << p0 << "\n";
   std::cout << "p1:\t" << p1 << "\n";
   std::cout << "p2:\t" << p2 << "\n";
   std::cout << "p:\t" << p << "\n";
   **/

   return 0;
}


/**

SAMPLE OUTPUT:

Hello, _paired-up_ world!
p0:	first: 0	second: 
p1:	first: 1	second: Uno
p2:	first: 2	second: Dos
p = p1, followed by p+=p2:	first: 3	second: Uno-Dos

**/
