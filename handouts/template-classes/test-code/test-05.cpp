#include <iostream>
#include <string>


// Class interface
template <typename F, typename S>
class SimplePair{    // Class is now SimplePair<F,S>
  private:
    F first;
    S second;

  public:
    // Constructors
    SimplePair();
    SimplePair(const F&, const S&);

    // Setter & Getters
    void set_values(const F&, const S&);
    F get_first() const;
    S get_second() const;

    // A member operator
    SimplePair& operator+=(const SimplePair&);
};



// Member definitions
template <typename F, typename S>
SimplePair<F,S>::SimplePair() 
  : first(), second() { }

template <typename F, typename S>
SimplePair<F,S>::SimplePair(const F& f, const S& s)
  : first(f), second(s) { }

template <typename F, typename S>
void SimplePair<F,S>::set_values(const F& f, const S& s){
    first = f;
    second = s;
}

template <typename F, typename S>
F SimplePair<F,S>::get_first() const {
    return first;
}

template <typename F, typename S>
S SimplePair<F,S>::get_second() const {
    return second;
}

template <typename F, typename S>
SimplePair<F,S>& SimplePair<F,S>::operator+=(const SimplePair<F,S>& rhs) {
    *this = *this + rhs;    // Uses non-member operator+
    return *this;
}



// Non-member functions
template <typename F, typename S>
std::ostream& operator<<(std::ostream& out, const SimplePair<F,S>& rhs){
    out << "(" << rhs.get_first() << "," << rhs.get_second() << ")";
    return out;
}

template <typename F, typename S>
void print_values(const SimplePair<F,S>& p){
    std::cout << "\tfirst: " << p.get_first()
              << "\tsecond: " << p.get_second() << "\n";
}

// Generic addition
template <typename F, typename S>
SimplePair<F,S> operator+( const SimplePair<F,S>& lhs,
                           const SimplePair<F,S>& rhs) {
    F f = lhs.get_first() + rhs.get_first();
    S s = lhs.get_second() + rhs.get_second();
    return SimplePair<F,S>(f,s);
}

// Specialized additions (string dash-concatenation)
template <typename F>
SimplePair<F,std::string> operator+(
        const SimplePair<F,std::string>& lhs,
        const SimplePair<F,std::string>& rhs) {
    F f = lhs.get_first() + rhs.get_first();
    std::string s = lhs.get_second() + "-" + rhs.get_second();
    return SimplePair<F,std::string>(f,s);
}

template <typename S>
SimplePair<std::string,S> operator+(
        const SimplePair<std::string,S>& lhs,
        const SimplePair<std::string,S>& rhs) {
    std::string f = lhs.get_first() + "-" + rhs.get_first();
    S s = lhs.get_second() + rhs.get_second();
    return SimplePair<std::string,S>(f,s);
}

SimplePair<std::string,std::string> operator+(
        const SimplePair<std::string,std::string>& lhs,
        const SimplePair<std::string,std::string>& rhs) {
    std::string f = lhs.get_first() + "-" + rhs.get_first();
    std::string s = lhs.get_second() + "-" + rhs.get_second();
    return SimplePair<std::string,std::string>(f,s);
}



// The driver
int main(){
   std::cout << "Hello, _paired-up_ world!\n";

   SimplePair<int,std::string> p0;
   SimplePair<int,std::string> p1(1,"Uno");
   SimplePair<int,std::string> p2 = SimplePair<int,std::string>();
   p2.set_values(2,"Dos");

   std::cout << "p0:";
   print_values(p0);
   std::cout << "p1:";
   print_values<int,std::string>(p1);    // OK, but unnecessary and ugly.
   std::cout << "p2:";
   print_values(p2);

   SimplePair<int,std::string> p = p1;
   p += p2;
   std::cout << "p = p1, followed by\np+=p2:";
   print_values(p);

   std::cout << "\nUsing operator<<\n";
   std::cout << "p0:\t" << p0 << "\n";
   std::cout << "p1:\t" << p1 << "\n";
   std::cout << "p2:\t" << p2 << "\n";
   std::cout << "p:\t" << p << "\n";

   SimplePair<std::string,int> q("Cuatro",4);
   std::cout << "q:\t" << q << "\n";
   SimplePair<std::string,int> r = q;
   r += q;
   std::cout << "r:\t" << r << "\n";

   return 0;
}


/**

SAMPLE OUTPUT:

Hello, _paired-up_ world!
p0:     first: 0        second: 
p1:     first: 1        second: Uno
p2:     first: 2        second: Dos
p = p1, followed by
p+=p2:  first: 3        second: Uno-Dos

Using operator<<
p0:     (0,)
p1:     (1,Uno)
p2:     (2,Dos)
p:      (3,Uno-Dos)
q:      (Cuatro,4)
r:      (Cuatro-Cuatro,8)

**/
