# A _working_ `BasicNode` class

This is a continuation of a [previous handout][previous] where we argued you
that the compiler provided code for the big 4 is not always appropriate for a
given class.

[previous]: ../case-against-default-big-4

## The problem

Recall that the compiler provided code for the big 4 leads us to the following
scenario:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ___________________________        ___________________________
   /             b             \      /           b_copy          \
  +-----------------------------+    +-----------------------------+
  |  the_ptr          +------+  |    |  the_ptr          +------+  |
  |     * ----------> | 2019 | <---------- *             | ???? |  |
  |                   +------+  |    |                   +------+  |
  +-----------------------------+    +-----------------------------+
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

However, in order for our class to work as expected, we should have this
instead:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ___________________________        ___________________________
   /             b             \      /           b_copy          \
  +-----------------------------+    +-----------------------------+
  |  the_ptr          +------+  |    |  the_ptr          +------+  |
  |     * ----------> | 2019 |  |    |     * ----------> | 2019 |  |
  |                   +------+  |    |                   +------+  |
  +-----------------------------+    +-----------------------------+
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To pinpoint the problem, let us explicitly write an equivalent version of the
code the compiler is placing in the copy constructor. Notice though, that since
there can be only one copy constructor, we have to comment out the previous
request for a _default_ implementation.

This is what the code looks like:

~~~~~ {.cpp}  
class BasicNode {
  . . .
    // BasicNode( const BasicNode& ) = default;

    BasicNode( const BasicNode& source ){
        the_value =  source.the_value;
        the_ptr = source.the_ptr;
    }

    BasicNode& operator=( const BasicNode& ) = default;
    ~BasicNode() = default;
    . . .
};
~~~~~  

_Did you spot the issue already?_

The problematic statement is the second one inside the copy constructor:

~~~~~ {.cpp}
        the_ptr = source.the_ptr;
~~~~~ 

Let us analyze this carefully. `the_ptr` in the left hand side of the statement
refers to the pointer in the implicit object `b_copy`; whereas as the code implies,
`source.the_ptr` in the right hand side refers to the pointer in the explicit
object `b`.

Since the assignment operator for pointers simply copies over the memory address
from the left hand side to the right hand side, this is akin to having the end
of _the arrow_ from the implicit object `b_copy`, _land_ at the current _landing
spot_ of the explicit object `b`.


## The _fix_

Luckily the issue is not hard to solve. We simply need to make sure that the
pointer in the object being created via the copy constructor _lands_ in the
appropriate _box_, namely the implicit object's `the_value` field.

This _class-specific_ version of the copy constructor works as intended.

~~~~~ {.cpp}  
class BasicNode {
    . . . 

    // BasicNode( const BasicNode& ) = default; // Nope!
    BasicNode( const BasicNode& source ){
        the_value =  source.the_value;   // This is OK.
        // the_ptr = source.the_ptr;     // Nope!
        // We should have `the_ptr` handle `the_value` instead.
        the_ptr = &the_value;
    }

    BasicNode& operator=( const BasicNode& ) = default;
    . . .
};
~~~~~  

## Are we done yet?

Nope. Look at the sample output produced by the driver we proposed [in the
previous handout][previous]:

~~~~~
a:      The memory address 0x7ffd2d21b900, holds the data 0.
b:      The memory address 0x7ffd2d21b910, holds the data 2019.
b_copy: The memory address 0x7ffd2d21b920, holds the data 2019.
a:      The memory address 0x7ffd2d21b910, holds the data 2019.

Reseting 'a' and re-inspecting objects...
a:      The memory address 0x7ffd2d21b910, holds the data 0.
b:      The memory address 0x7ffd2d21b910, holds the data 0.
b_copy: The memory address 0x7ffd2d21b920, holds the data 2019.
~~~~~

The nodes `a` and `b` are still sharing the memory address `0x7ffd2d21b910`.
This is not correct: all of them nodes should have different memory locations;
moreover, only the value stored within `a` should be equal to 0.


## The rule of 3 in C++

Roughly stated it says that if a class-specific member of the big 3 is coded by
the programmer, then the other two members should also be coded by the
programmer in a class-specific way.

In this particular example, there is no need to bother with the class destructor
because we are only dealing with _stack_ memory.[^heap] However, ~~we~~ you
should bother writing a version of the assignment operator that completely
solves the pointer sharing issue we are still dealing with.

To determine whether or not you have a correct implementation of the assignment
operator, compare the output of your program to the following sample output:

~~~~~
a:      The memory address 0x7ffd2d21b900, holds the data 0.
b:      The memory address 0x7ffd2d21b910, holds the data 2019.
b_copy: The memory address 0x7ffd2d21b920, holds the data 2019.
a:      The memory address 0x7ffd2d21b900, holds the data 2019.

Reseting 'a' and re-inspecting objects...
a:      The memory address 0x7ffd2d21b900, holds the data 0.
b:      The memory address 0x7ffd2d21b910, holds the data 2019.
b_copy: The memory address 0x7ffd2d21b920, holds the data 2019.
~~~~~

If they _"match"_, you should hive five yourself while saying out loud:

> [_¡No contaban con mi astucia!_][chapulin]

[^heap]: Soon this will change as we will start dealing with _heap_ memory.

[chapulin]: https://www.youtube.com/watch?v=IMZt2hocabs&start=57


## Test code

For your convenience, [here is a file][example] containing the code discussed in
this handout.

[example]: basic-node-files/basic-node-custom-big4-incomplete.cpp


---

[Return to main PIC course website][main]

[main]: ../..

