#include <iostream>

class BasicNode {
  private:
    int the_value;
    int* the_ptr;

  public:
    BasicNode( int n = 0 ){
        the_ptr = &the_value;
        *the_ptr = n;
    }

    BasicNode( const BasicNode& source ){
        the_value =  source.the_value;   // This is OK.
        // the_ptr = source.the_ptr;     // Nope!
        // We should have `the_ptr` handle `the_value` instead.
        the_ptr = &the_value;
    }

    BasicNode& operator=( const BasicNode& ) = default ;
    ~BasicNode() = default; // Still OK because we are not using heap memory.

    void set_value( int n ){ *the_ptr = n; }

  friend std::ostream& operator<<( std::ostream&, const BasicNode& );
};


std::ostream& operator<<( std::ostream& out, const BasicNode& node ){
    out << "The memory address " << node.the_ptr
        << ", holds the data " << *node.the_ptr << ".\n";
    return out;
}


int main(){
    BasicNode a;
    BasicNode b(2019);
    std::cout << "a:\t" << a;
    std::cout << "b:\t" << b;

    BasicNode b_copy(b);
    std::cout << "b_copy:\t" << b_copy;

    a = b;
    std::cout << "a:\t" << a;

    std::cout << "\nReseting 'a' and re-inspecting objects...\n";
    a.set_value(0);
    std::cout << "a:\t" << a;
    std::cout << "b:\t" << b;
    std::cout << "b_copy:\t" << b_copy;

    return 0;
}


/**

SAMPLE OUTPUT:

a:      The memory address 0x7ffd2d21b900, holds the data 0.
b:      The memory address 0x7ffd2d21b910, holds the data 2019.
b_copy: The memory address 0x7ffd2d21b920, holds the data 2019.
a:      The memory address 0x7ffd2d21b910, holds the data 2019.

Reseting 'a' and re-inspecting objects...
a:      The memory address 0x7ffd2d21b910, holds the data 0.
b:      The memory address 0x7ffd2d21b910, holds the data 0.
b_copy: The memory address 0x7ffd2d21b920, holds the data 2019.

**/
