# Course walk-throughs & handouts

## Course handouts

The documents below explain concepts discussed while coding some _home-made_
classes.

*   [The big 4][big4].
*   [The case against the default big 4][against-big4].
*   [A _working_ `BasicNode` class][custom-big4].
*   [Our very own _non-template_ `Pic10b::vector` class][vector].
*   [Template functions][temp-fun].
*   [Template classes][temp-class].
*   [A template doubly linked list class][list].
*   [Container adapters: _stacks_ & _queues_][adapters].

[big4]: the-big-4/
[against-big4]: case-against-default-big-4/
[custom-big4]: basic-node-revisited/
[vector]: non-template-vector/
[temp-fun]: template-functions/
[temp-class]: template-classes/
[list]: template-list/
[adapters]: container-adapters/


## Walkthroughs

The following documents contain information that might come in handy at some
point.

*   [Making sure your project compiles before you submit it to CCLE][compile].
*   [Figuring out your _working directory_][wd].

[compile]: testing-submissions-handout/
[wd]: working-directory-handout/


---

[Return to main PIC course website][main]

[main]: ..

