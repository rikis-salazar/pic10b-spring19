# Handout: getting started with homework assignments

During this quarter, you will be coding mostly libraries instead of complete
programs. To help you make sure your libraries are correctly set up, every
assignment assignment will provide a _tester suite_ (`.zip` file). This _suite_
will typically consist of a compressed directory structure, together with a
set of _recipes_ (`Makefile`) to compile/run your projects. To make use of this
suites you'll need to make sure that your homework files are placed in the
correct location, then you'll need to upload the directory structure to the PIC
server and _run the appropriate recipes_ (`make` commands).


## A fictitious homework assignment 

For this handout to make a bit more sense, let us pretend you are to turn in two
files, namely `hw0.h`, and `hw0.cpp`, which altogether form part of a _more
complex_ homework project. Let us further assume that somewhere in the
assignment description, the following statement can be found

> [F]or this assignment, you are to code a function with signature:  
> `void print_message();`  

To complicate things further, the assignment description also states that

> [Y]ou are to use a _3-file layout_: a _driver file_ containing the `main()`
> routine, plus a _library declaration_ file (`.h`), plus a _library definition
> file_ (`.cpp`).

Some other details about the library files are to be found within the assignment
description as well. _E.g.:_

> *   `hw0.h`, should contain the function declaration (**without the function
>     definition**); and
> *   `hw0.cpp`, should contain the function body (_i.e.,_ the function
>     definition).  

Lastly, you are told that you are not to code the driver. Instead, _test files_
will be provided to you:

> The _drivers_ listed below should give you and idea of how your library will
> be tested. To grade your assignment, other drivers not listed here could be
> used. However, if your project compiles against these files, it should also
> compile against the rest of the drivers that might be used against your
> project.
>
> *   [`hw0_drv1.cpp`][drv1]: calls the function `print_message()` once.
> *   [`hw0_drv2.cpp`][drv2]: calls the function `print_message()` three times.

[drv1]: hw0-driver-files/hw0_drv1.cpp
[drv2]: hw0-driver-files/hw0_drv2.cpp


## What are you to do in this scenario?

First, and foremost: **make sure you know how to properly setup your IDE[^one]
to compile projects consisting of several files**. If you are uncertain how this
is done, you should approach your TA, or myself. We will be happy to help you
get started.

Next, you should get to work. In this case, you can either code the library
files yourself, or you can use the files in [this directory][impl].

I am certain you have been told this already, but it cannot be overemphasized
enough:

1.  Always think before you code: it is better to spend several days thinking
    about how to implement an idea, then write some code; than start coding
    right away, only to realize later that you have no idea what works, and what
    doesn't.
1.  Writing [_stubs_][st] is a good way to make sure you are heading in the
    right direction. A project with the correct set of stubs will compile, even
    if it does not run the way it is supposed to.
1.  Always code in small increments: by adding a few lines of code at a time,
    you are more likely to catch a compile/logic error sooner rather than later.

[impl]: hw0-sample-files/
[st]: https://www.dreamincode.net/forums/topic/220121-stub-functions/
[^one]: IDE = Integrated Development Environment. Some common IDEs are: Visual
  Studio, Xcode, Code::Blocks, etc.

Once your project is ready for submission, you should test it against the
official compiler: `g++` hosted at the PIC web server (see details below).


## Checking whether or not your project compiles

The process can be broken down into 3 steps:

1.  prepare a local directory structure and place your files in the correct
    location; then
1.  upload the directory structure to the PIC web server; and lastly    
1.  connect to the PIC web server and run a series of commands to compile, and
    run your project.


### Step 1: prepare a local directory

> Prerequisites: a completed project assignment; more specifically, the files
> `hw0.h`, and `hw0.cpp`.

For this step, I suggest you place your project files in an _easy-to-access_
directory. For example, I created a directory (`tester_demo`) in my Desktop
folder (Figure 1).

| ![hw0-images/00-tester_demo_contents.png](hw0-images/00-tester_demo_contents.png) |  
|:-----:|  
| Figure 1: `tester_demo` folder in Desktop. |

Next, you need to download the tester suite. For this assignment [it is located
here][hw0-suite]. Very likely, the zip file will be downloaded to a special
folder in your computer. In my case, it wound up in my `Downloads` folder. You
need to locate this zip file and unzip it. To make my life a little easier, I
transferred the zip file over to the `tester_demo` folder (see Figure 2).

| ![hw0-images/01-tester_demo_2.png](hw0-images/01-tester_demo_2.png) |  
|:-----:|  
| Figure 2: tester suite inside `tester_demo` folder. |

This is how it looks in my computer after the zip file is expanded:

| ![hw0-images/02-extracted.png](hw0-images/02-extracted.png) |  
|:-----:|  
| Figure 3: expanded contents of `pic10b-check-hw0.zip`. |

> **Please note:** depending on the software you use to expand the contents of
> `pic10b-check-hw0.zip`, the result might be either:
>
> i.  a folder named `pic10b-check-hw0` inside the `tester_demo` folder; or
> i.  a folder named `pic10b-check-hw0` inside another folder named
>     `pic10b-check-hw0`, which in turn is inside the `tester_demo` folder.
>     We'll call this a _nested setup_.
>
> In my case, as you can see from Figure 3, the expansion lead to the nested
> setup described in ii. above.

To complete the setup, we simply move [or copy] the submission files to their
correct location: the `SubmissionsDir` folder located inside the [_inner_]
`pic10b-check-hw0` folder.

| ![hw0-images/03-transferred.png](hw0-images/03-transferred.png) |  
|:-----:|  
| Figure 4: expanded contents of `pic10b-check-hw0.zip`. |

This should do it for us. At this point, if everything went well, we have a
local _"working"_ copy of the correct tester directory structure that will be
used to compile this project.

[hw0-suite]: https://ccle.ucla.edu/mod/resource/view.php?id=2406651


### Step 2: transfer the local directory to the PIC web server

> Prerequisites:  
>
> 1.  Completion of Step 1 above.
> 1.  A _secure file transfer protocol_, or a _secure copy protocol_
>     software: [FileZilla][fz] (Win, Mac, Linux), [WinSCP][wscp] (Win),
>     [Cyberduck][cbd] (Mac, Win, iOS), [ES File Explorer][esfx] (Android), etc.
> 1.  Your linux PIC credentials: [follow this walk through][lin-acc], or get in
>     touch with us (instructor, TA, PIC lab assistant) to help you get started.
>
> Note: The screenshots below are specific to FileZilla. Other programs should
> be very similar; but if for some reason you run into troubles, get in touch
> with us to help you solve them.

In order to connect to the laguna server via a secure file transfer protocol
client you will need:

*   the name of the PIC web server: `laguna.math.ucla.edu`
*   your Linux PIC credentials: _login_ & _password_
    ([see this walk through][lin-acc]).
*   the port number: 22

Fire up your secure file transfer protocol app (_e.g.,_ FileZilla), and enter
the information described above in the corresponding text fields (see Figure 5).

| ![hw0-images/04-filezilla.png](hw0-images/04-filezilla.png) |  
|:-----:|  
| Figure 5: using FileZilla to connect to the PIC web server. |

When you connect to a server for the first time, you might see a message along
the lines of

    The authenticity of host ...
    ...
    Are you sure you want to continue connecting?

In this case, depending on your software, you might need to press a confirmation
button, or manually type `y`, or the word `yes`.

After going through this process, FileZilla should present you another pop up
window requesting information about what to do with your password.

| ![hw0-images/05-pwds.png](hw0-images/05-pwds.png) |  
|:-----:|  
| Figure 6: FileZilla's options to handle passwords. |

Here, the choice is up to you. I usually choose not to store the password in an
effort make it hard for myself to forget it. If I were to do so, I would have to
contact PIC support (_a.k.a._ the bugs office @ MS 6121) to have my password
reset.

The rest of the process should be simple: on the left panel (Local site), locate
the `pic10b-check-hw0` directory containing the files needed to compile the
project; then upload it to the PIC web server (Remote site). Figure 7 below
illustrates the state prior to the transfer (copy) of the local directory.

| ![hw0-images/06-to_upload.png](hw0-images/06-to_upload.png) |  
|:-----:|  
| Figure 7: Local, and Remote sites prior to uploading local folder. |

Once the folder has been copied (_e.g.,_ via right click on the local folder,
followed by clicking on a menu option that reads _upload_, _send_, _transfer_,
or similar), it should appear on the right panel (Remote site:):

| ![hw0-images/07-uploaded.png](hw0-images/07-uploaded.png) |  
|:-----:|  
| Figure 8: Local, and Remote sites after uploading local folder. |

> **Please note:** Depending on the way the zip file was expanded, you might
> have a _nested_ setup (see note in Step 1). If this is the case, make sure to
> send the _inner_ folder to the remote server. If you are not certain which
> folder is the one that was copied, you can right click on it in the right
> panel (Remote site:), and delete it via the appropriate menu entry.

[fz]: https://filezilla-project.org/
[wscp]: https://winscp.net/eng/index.php
[cbd]: https://cyberduck.io/
[esfx]: https://play.google.com/store/apps/details?id=com.estrongs.android.pop&hl=en_US
[lin-acc]: http://www.pic.ucla.edu/frequently-asked-questions/setting-up-your-pic-linuxemail-account/


### Step 3: test your project against the official course compiler

> Prerequisites:  
>
> 1.  Completion of Steps 1, and 2, above.
> 1.  A _secure shell_ client: for example [PuTTY][putty] (for Windows), or the 
>     built-in `ssh` client (for MacOS, and Linux) via a regular shell (_e.g.:_
>     terminal).
> 1.  Your linux PIC credentials.

If you are wondering why we need yet another piece of software, it is because
most _Secure File Transfer Protocol_ clients, are limited to sending, receiving,
and listing local and remote files[^two]. In order to be able to _"run the
recipes"_ that compile [run, clean, etc.,] our project, we need to be able to
run remote programs (_e.g.,_ GNU Make, g++, etc.).

#### Connecting to the PIC web server using PuTTY

Fire up PuTTY, then enter `laguna.math.ucla.edu` in the Host Name (or IP
address) text field. Before clicking on the _Open_ button at the bottom of the
window, make sure the _SSH_ radio button is selected: this will automatically
set the correct port number (see Figure 9 below).

| ![hw0-images/08-putty_laguna.png](hw0-images/08-putty_laguna.png) |  
|:-----:|  
| Figure 9: Connecting to the PIC web server via PuTTY. |

After you click connect, a new window will pop up. If this is the first time you
are connecting to the PIC server on your current device, you will be warned
about _the authenticity of the host_. In this case, you will have to type `y`,
or `yes`, to continue.

Once this is done, you will be prompted to enter your credentials (_i.e.,_ login
& password).

| ![hw0-images/09-laguna1.png](hw0-images/09-laguna1.png) |  
|:-----:|  
| Figure 10: Successful login. |

> **Please note:** Your password WILL NOT be echoed back to you. That is, you
> will not see the characters you type. This is a safety feature that makes it
> harder for others to figure out your password.


#### Connecting to the PIC web server via a terminal

If you are using a *NIX operating system (_e.g.,_ Mac OS, Linux), then simply
open a terminal window (via Applications &rarr; Utilities &rarr; Terminal in Mac
OS, or \<Ctrl\>-\<Alt\>-\<t\> in most Linux distros) and issue the command

    ssh YOUR_LOGIN@laguna.math.ucla.edu

where `YOUR_LOGIN` is the one you use when connecting to the PIC web server in
step 2 above.

If sucessful, you will be prompted to enter your password, which will not be
echoed back to you (see note above).


#### Compiling & testing your project

To compile your project, first you have to navigate to the correct location.
Assuming the folder you transferred in step 2 is named `pic10b-check-hw0`, at
the prompt (which typically reads either `laguna.N>`, or `laguna.N$`, where N is
a number that increases every time you issue a command) you need to type

    cd pic10b-check-hw0

If successful, the command will produce no output (see Figure 11 below). If this
is not the case, double check the issued command, keeping in mind that file
names are case sensitive.

> **Pro tip:** issue the command `ls` (or `ls -l`, that is _ell_ _ess_ space
> dash _ell_) to display a list of files in your remote account).

| ![hw0-images/10-make.png](hw0-images/10-make.png) |  
|:-----:|  
| Figure 11: Successful project compilation. |

Next, enter `ls` at the prompt to list the files in the current directory
(Figure 11 above). If the files displayed include `DriversDir`, `Makefile`, and
`SubmissionsDir`, congratulate yourself: you are ready to compile your project.

> **Please note:** if the output of `ls` consists of a single directory, namely
> `pic10b-check-hw0`, this indicates that in your local device you have a nested
> setup; moreover you transferred the _outer_ directory, instead of the _inner_
> one. To fix this, enter the inner directory by issuing (one more time) the
> command
>
>     cd pic10b-check-hw0
>
> then type `ls`. This time around you should see the correct set of files
> needed to compile your project. Feel free to high-five yourself if you so
> choose.

Finally, to compile your project, issue the command

    make

this will generate the binary (_i.e.,_ executable) files associated to the
provided drivers, as well as other secondary files and directories (see Figure
11 above).

If there are any compile/link errors, they will be saved in the file
`00-CompilationLogs.txt`. To display the contents issue the command

    more 00-CompilationLogs.txt

According to its manual, `more` _"is a filter for paging through [the contents
of a text file] one screenful at a time."_ To display the next screen simply
press enter.

> **Pro tip:** use FileZilla (or any other SFTP client) to connect to the PIC
> web server and _download_ a copy of the remote files to your local device. The
> executable files will likely not run, but you will be able to examine the
> contents of all the generated files using your favorite (local) software.

If your project compiles successfully, you can test it by issuing the command

    make test

This will run the programs and will capture their outputs. They will be stored
in text files inside the `OutputFilesDir` (created when this command is issued).
In addition, the contents of these files will also be displayed in your current
PuTYY [or terminal] window (Figure 12).

| ![hw0-images/11-make_test.png](hw0-images/11-make_test.png) |  
|:-----:|  
| Figure 12: Test results for a _correctly coded_ project. |

This should do it. For actual assignments, I might add other recipes (`Makefile`
rules) that allow you to perform other tasks. This might come in handy in the
event of a regrade request, or to clarify a possible score deduction.

As a last [optional] step, you might want to _clean_ your remote directory by
eliminating all those extra files generated by this process. If that is the
case, issue the command

    make clean

This will remove all files not present at the beginning of Step 3.


## Troubleshooting

If for some reason you are not able to complete this walk through, make sure to
get in touch with us to help you correct any issues.

[putty]: https://www.putty.org/
[^two]: WinSCP does provide a way to run remote commands, but this feature is
  rather rudimentary.

---

[Return to main PIC course website][main]

[main]: ../..

