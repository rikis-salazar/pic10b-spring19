<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <title>readme</title>
  <style type="text/css">
      code{white-space: pre-wrap;}
      span.smallcaps{font-variant: small-caps;}
      span.underline{text-decoration: underline;}
      div.column{display: inline-block; vertical-align: top; width: 50%;}
  </style>
  <link rel="stylesheet" href="../../pandoc.css" />
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<h1 id="handout-getting-started-with-homework-assignments">Handout: getting started with homework assignments</h1>
<p>During this quarter, you will be coding mostly libraries instead of complete programs. To help you make sure your libraries are correctly set up, every assignment assignment will provide a <em>tester suite</em> (<code>.zip</code> file). This <em>suite</em> will typically consist of a compressed directory structure, together with a set of <em>recipes</em> (<code>Makefile</code>) to compile/run your projects. To make use of this suites you’ll need to make sure that your homework files are placed in the correct location, then you’ll need to upload the directory structure to the PIC server and <em>run the appropriate recipes</em> (<code>make</code> commands).</p>
<h2 id="a-fictitious-homework-assignment">A fictitious homework assignment</h2>
<p>For this handout to make a bit more sense, let us pretend you are to turn in two files, namely <code>hw0.h</code>, and <code>hw0.cpp</code>, which altogether form part of a <em>more complex</em> homework project. Let us further assume that somewhere in the assignment description, the following statement can be found</p>
<blockquote>
<p>[F]or this assignment, you are to code a function with signature:<br />
<code>void print_message();</code></p>
</blockquote>
<p>To complicate things further, the assignment description also states that</p>
<blockquote>
<p>[Y]ou are to use a <em>3-file layout</em>: a <em>driver file</em> containing the <code>main()</code> routine, plus a <em>library declaration</em> file (<code>.h</code>), plus a <em>library definition file</em> (<code>.cpp</code>).</p>
</blockquote>
<p>Some other details about the library files are to be found within the assignment description as well. <em>E.g.:</em></p>
<blockquote>
<ul>
<li><code>hw0.h</code>, should contain the function declaration (<strong>without the function definition</strong>); and</li>
<li><code>hw0.cpp</code>, should contain the function body (<em>i.e.,</em> the function definition).</li>
</ul>
</blockquote>
<p>Lastly, you are told that you are not to code the driver. Instead, <em>test files</em> will be provided to you:</p>
<blockquote>
<p>The <em>drivers</em> listed below should give you and idea of how your library will be tested. To grade your assignment, other drivers not listed here could be used. However, if your project compiles against these files, it should also compile against the rest of the drivers that might be used against your project.</p>
<ul>
<li><a href="hw0-driver-files/hw0_drv1.cpp"><code>hw0_drv1.cpp</code></a>: calls the function <code>print_message()</code> once.</li>
<li><a href="hw0-driver-files/hw0_drv2.cpp"><code>hw0_drv2.cpp</code></a>: calls the function <code>print_message()</code> three times.</li>
</ul>
</blockquote>
<h2 id="what-are-you-to-do-in-this-scenario">What are you to do in this scenario?</h2>
<p>First, and foremost: <strong>make sure you know how to properly setup your IDE<a href="#fn1" class="footnote-ref" id="fnref1"><sup>1</sup></a> to compile projects consisting of several files</strong>. If you are uncertain how this is done, you should approach your TA, or myself. We will be happy to help you get started.</p>
<p>Next, you should get to work. In this case, you can either code the library files yourself, or you can use the files in <a href="hw0-sample-files/">this directory</a>.</p>
<p>I am certain you have been told this already, but it cannot be overemphasized enough:</p>
<ol type="1">
<li>Always think before you code: it is better to spend several days thinking about how to implement an idea, then write some code; than start coding right away, only to realize later that you have no idea what works, and what doesn’t.</li>
<li>Writing <a href="https://www.dreamincode.net/forums/topic/220121-stub-functions/"><em>stubs</em></a> is a good way to make sure you are heading in the right direction. A project with the correct set of stubs will compile, even if it does not run the way it is supposed to.</li>
<li>Always code in small increments: by adding a few lines of code at a time, you are more likely to catch a compile/logic error sooner rather than later.</li>
</ol>
<p>Once your project is ready for submission, you should test it against the official compiler: <code>g++</code> hosted at the PIC web server (see details below).</p>
<h2 id="checking-whether-or-not-your-project-compiles">Checking whether or not your project compiles</h2>
<p>The process can be broken down into 3 steps:</p>
<ol type="1">
<li>prepare a local directory structure and place your files in the correct location; then</li>
<li>upload the directory structure to the PIC web server; and lastly<br />
</li>
<li>connect to the PIC web server and run a series of commands to compile, and run your project.</li>
</ol>
<h3 id="step-1-prepare-a-local-directory">Step 1: prepare a local directory</h3>
<blockquote>
<p>Prerequisites: a completed project assignment; more specifically, the files <code>hw0.h</code>, and <code>hw0.cpp</code>.</p>
</blockquote>
<p>For this step, I suggest you place your project files in an <em>easy-to-access</em> directory. For example, I created a directory (<code>tester_demo</code>) in my Desktop folder (Figure 1).</p>
<table>
<thead>
<tr class="header">
<th style="text-align: center;"><img src="hw0-images/00-tester_demo_contents.png" alt="hw0-images/00-tester_demo_contents.png" /></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;">Figure 1: <code>tester_demo</code> folder in Desktop.</td>
</tr>
</tbody>
</table>
<p>Next, you need to download the tester suite. For this assignment <a href="https://ccle.ucla.edu/mod/resource/view.php?id=2406651">it is located here</a>. Very likely, the zip file will be downloaded to a special folder in your computer. In my case, it wound up in my <code>Downloads</code> folder. You need to locate this zip file and unzip it. To make my life a little easier, I transferred the zip file over to the <code>tester_demo</code> folder (see Figure 2).</p>
<table>
<thead>
<tr class="header">
<th style="text-align: center;"><img src="hw0-images/01-tester_demo_2.png" alt="hw0-images/01-tester_demo_2.png" /></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;">Figure 2: tester suite inside <code>tester_demo</code> folder.</td>
</tr>
</tbody>
</table>
<p>This is how it looks in my computer after the zip file is expanded:</p>
<table>
<thead>
<tr class="header">
<th style="text-align: center;"><img src="hw0-images/02-extracted.png" alt="hw0-images/02-extracted.png" /></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;">Figure 3: expanded contents of <code>pic10b-check-hw0.zip</code>.</td>
</tr>
</tbody>
</table>
<blockquote>
<p><strong>Please note:</strong> depending on the software you use to expand the contents of <code>pic10b-check-hw0.zip</code>, the result might be either:</p>
<ol type="i">
<li>a folder named <code>pic10b-check-hw0</code> inside the <code>tester_demo</code> folder; or</li>
<li>a folder named <code>pic10b-check-hw0</code> inside another folder named <code>pic10b-check-hw0</code>, which in turn is inside the <code>tester_demo</code> folder. We’ll call this a <em>nested setup</em>.</li>
</ol>
<p>In my case, as you can see from Figure 3, the expansion lead to the nested setup described in ii. above.</p>
</blockquote>
<p>To complete the setup, we simply move [or copy] the submission files to their correct location: the <code>SubmissionsDir</code> folder located inside the [<em>inner</em>] <code>pic10b-check-hw0</code> folder.</p>
<table>
<thead>
<tr class="header">
<th style="text-align: center;"><img src="hw0-images/03-transferred.png" alt="hw0-images/03-transferred.png" /></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;">Figure 4: expanded contents of <code>pic10b-check-hw0.zip</code>.</td>
</tr>
</tbody>
</table>
<p>This should do it for us. At this point, if everything went well, we have a local <em>“working”</em> copy of the correct tester directory structure that will be used to compile this project.</p>
<h3 id="step-2-transfer-the-local-directory-to-the-pic-web-server">Step 2: transfer the local directory to the PIC web server</h3>
<blockquote>
<p>Prerequisites:</p>
<ol type="1">
<li>Completion of Step 1 above.</li>
<li>A <em>secure file transfer protocol</em>, or a <em>secure copy protocol</em> software: <a href="https://filezilla-project.org/">FileZilla</a> (Win, Mac, Linux), <a href="https://winscp.net/eng/index.php">WinSCP</a> (Win), <a href="https://cyberduck.io/">Cyberduck</a> (Mac, Win, iOS), <a href="https://play.google.com/store/apps/details?id=com.estrongs.android.pop&amp;hl=en_US">ES File Explorer</a> (Android), etc.</li>
<li>Your linux PIC credentials: <a href="http://www.pic.ucla.edu/frequently-asked-questions/setting-up-your-pic-linuxemail-account/">follow this walk through</a>, or get in touch with us (instructor, TA, PIC lab assistant) to help you get started.</li>
</ol>
<p>Note: The screenshots below are specific to FileZilla. Other programs should be very similar; but if for some reason you run into troubles, get in touch with us to help you solve them.</p>
</blockquote>
<p>In order to connect to the laguna server via a secure file transfer protocol client you will need:</p>
<ul>
<li>the name of the PIC web server: <code>laguna.math.ucla.edu</code></li>
<li>your Linux PIC credentials: <em>login</em> &amp; <em>password</em> (<a href="http://www.pic.ucla.edu/frequently-asked-questions/setting-up-your-pic-linuxemail-account/">see this walk through</a>).</li>
<li>the port number: 22</li>
</ul>
<p>Fire up your secure file transfer protocol app (<em>e.g.,</em> FileZilla), and enter the information described above in the corresponding text fields (see Figure 5).</p>
<table>
<thead>
<tr class="header">
<th style="text-align: center;"><img src="hw0-images/04-filezilla.png" alt="hw0-images/04-filezilla.png" /></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;">Figure 5: using FileZilla to connect to the PIC web server.</td>
</tr>
</tbody>
</table>
<p>When you connect to a server for the first time, you might see a message along the lines of</p>
<pre><code>The authenticity of host ...
...
Are you sure you want to continue connecting?</code></pre>
<p>In this case, depending on your software, you might need to press a confirmation button, or manually type <code>y</code>, or the word <code>yes</code>.</p>
<p>After going through this process, FileZilla should present you another pop up window requesting information about what to do with your password.</p>
<table>
<thead>
<tr class="header">
<th style="text-align: center;"><img src="hw0-images/05-pwds.png" alt="hw0-images/05-pwds.png" /></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;">Figure 6: FileZilla’s options to handle passwords.</td>
</tr>
</tbody>
</table>
<p>Here, the choice is up to you. I usually choose not to store the password in an effort make it hard for myself to forget it. If I were to do so, I would have to contact PIC support (<em>a.k.a.</em> the bugs office @ MS 6121) to have my password reset.</p>
<p>The rest of the process should be simple: on the left panel (Local site), locate the <code>pic10b-check-hw0</code> directory containing the files needed to compile the project; then upload it to the PIC web server (Remote site). Figure 7 below illustrates the state prior to the transfer (copy) of the local directory.</p>
<table>
<thead>
<tr class="header">
<th style="text-align: center;"><img src="hw0-images/06-to_upload.png" alt="hw0-images/06-to_upload.png" /></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;">Figure 7: Local, and Remote sites prior to uploading local folder.</td>
</tr>
</tbody>
</table>
<p>Once the folder has been copied (<em>e.g.,</em> via right click on the local folder, followed by clicking on a menu option that reads <em>upload</em>, <em>send</em>, <em>transfer</em>, or similar), it should appear on the right panel (Remote site:):</p>
<table>
<thead>
<tr class="header">
<th style="text-align: center;"><img src="hw0-images/07-uploaded.png" alt="hw0-images/07-uploaded.png" /></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;">Figure 8: Local, and Remote sites after uploading local folder.</td>
</tr>
</tbody>
</table>
<blockquote>
<p><strong>Please note:</strong> Depending on the way the zip file was expanded, you might have a <em>nested</em> setup (see note in Step 1). If this is the case, make sure to send the <em>inner</em> folder to the remote server. If you are not certain which folder is the one that was copied, you can right click on it in the right panel (Remote site:), and delete it via the appropriate menu entry.</p>
</blockquote>
<h3 id="step-3-test-your-project-against-the-official-course-compiler">Step 3: test your project against the official course compiler</h3>
<blockquote>
<p>Prerequisites:</p>
<ol type="1">
<li>Completion of Steps 1, and 2, above.</li>
<li>A <em>secure shell</em> client: for example <a href="https://www.putty.org/">PuTTY</a> (for Windows), or the built-in <code>ssh</code> client (for MacOS, and Linux) via a regular shell (<em>e.g.:</em> terminal).</li>
<li>Your linux PIC credentials.</li>
</ol>
</blockquote>
<p>If you are wondering why we need yet another piece of software, it is because most <em>Secure File Transfer Protocol</em> clients, are limited to sending, receiving, and listing local and remote files<a href="#fn2" class="footnote-ref" id="fnref2"><sup>2</sup></a>. In order to be able to <em>“run the recipes”</em> that compile [run, clean, etc.,] our project, we need to be able to run remote programs (<em>e.g.,</em> GNU Make, g++, etc.).</p>
<h4 id="connecting-to-the-pic-web-server-using-putty">Connecting to the PIC web server using PuTTY</h4>
<p>Fire up PuTTY, then enter <code>laguna.math.ucla.edu</code> in the Host Name (or IP address) text field. Before clicking on the <em>Open</em> button at the bottom of the window, make sure the <em>SSH</em> radio button is selected: this will automatically set the correct port number (see Figure 9 below).</p>
<table>
<thead>
<tr class="header">
<th style="text-align: center;"><img src="hw0-images/08-putty_laguna.png" alt="hw0-images/08-putty_laguna.png" /></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;">Figure 9: Connecting to the PIC web server via PuTTY.</td>
</tr>
</tbody>
</table>
<p>After you click connect, a new window will pop up. If this is the first time you are connecting to the PIC server on your current device, you will be warned about <em>the authenticity of the host</em>. In this case, you will have to type <code>y</code>, or <code>yes</code>, to continue.</p>
<p>Once this is done, you will be prompted to enter your credentials (<em>i.e.,</em> login &amp; password).</p>
<table>
<thead>
<tr class="header">
<th style="text-align: center;"><img src="hw0-images/09-laguna1.png" alt="hw0-images/09-laguna1.png" /></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;">Figure 10: Successful login.</td>
</tr>
</tbody>
</table>
<blockquote>
<p><strong>Please note:</strong> Your password WILL NOT be echoed back to you. That is, you will not see the characters you type. This is a safety feature that makes it harder for others to figure out your password.</p>
</blockquote>
<h4 id="connecting-to-the-pic-web-server-via-a-terminal">Connecting to the PIC web server via a terminal</h4>
<p>If you are using a *NIX operating system (<em>e.g.,</em> Mac OS, Linux), then simply open a terminal window (via Applications → Utilities → Terminal in Mac OS, or &lt;Ctrl&gt;-&lt;Alt&gt;-&lt;t&gt; in most Linux distros) and issue the command</p>
<pre><code>ssh YOUR_LOGIN@laguna.math.ucla.edu</code></pre>
<p>where <code>YOUR_LOGIN</code> is the one you use when connecting to the PIC web server in step 2 above.</p>
<p>If sucessful, you will be prompted to enter your password, which will not be echoed back to you (see note above).</p>
<h4 id="compiling-testing-your-project">Compiling &amp; testing your project</h4>
<p>To compile your project, first you have to navigate to the correct location. Assuming the folder you transferred in step 2 is named <code>pic10b-check-hw0</code>, at the prompt (which typically reads either <code>laguna.N&gt;</code>, or <code>laguna.N$</code>, where N is a number that increases every time you issue a command) you need to type</p>
<pre><code>cd pic10b-check-hw0</code></pre>
<p>If successful, the command will produce no output (see Figure 11 below). If this is not the case, double check the issued command, keeping in mind that file names are case sensitive.</p>
<blockquote>
<p><strong>Pro tip:</strong> issue the command <code>ls</code> (or <code>ls -l</code>, that is <em>ell</em> <em>ess</em> space dash <em>ell</em>) to display a list of files in your remote account).</p>
</blockquote>
<table>
<thead>
<tr class="header">
<th style="text-align: center;"><img src="hw0-images/10-make.png" alt="hw0-images/10-make.png" /></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;">Figure 11: Successful project compilation.</td>
</tr>
</tbody>
</table>
<p>Next, enter <code>ls</code> at the prompt to list the files in the current directory (Figure 11 above). If the files displayed include <code>DriversDir</code>, <code>Makefile</code>, and <code>SubmissionsDir</code>, congratulate yourself: you are ready to compile your project.</p>
<blockquote>
<p><strong>Please note:</strong> if the output of <code>ls</code> consists of a single directory, namely <code>pic10b-check-hw0</code>, this indicates that in your local device you have a nested setup; moreover you transferred the <em>outer</em> directory, instead of the <em>inner</em> one. To fix this, enter the inner directory by issuing (one more time) the command</p>
<pre><code>cd pic10b-check-hw0</code></pre>
<p>then type <code>ls</code>. This time around you should see the correct set of files needed to compile your project. Feel free to high-five yourself if you so choose.</p>
</blockquote>
<p>Finally, to compile your project, issue the command</p>
<pre><code>make</code></pre>
<p>this will generate the binary (<em>i.e.,</em> executable) files associated to the provided drivers, as well as other secondary files and directories (see Figure 11 above).</p>
<p>If there are any compile/link errors, they will be saved in the file <code>00-CompilationLogs.txt</code>. To display the contents issue the command</p>
<pre><code>more 00-CompilationLogs.txt</code></pre>
<p>According to its manual, <code>more</code> <em>“is a filter for paging through [the contents of a text file] one screenful at a time.”</em> To display the next screen simply press enter.</p>
<blockquote>
<p><strong>Pro tip:</strong> use FileZilla (or any other SFTP client) to connect to the PIC web server and <em>download</em> a copy of the remote files to your local device. The executable files will likely not run, but you will be able to examine the contents of all the generated files using your favorite (local) software.</p>
</blockquote>
<p>If your project compiles successfully, you can test it by issuing the command</p>
<pre><code>make test</code></pre>
<p>This will run the programs and will capture their outputs. They will be stored in text files inside the <code>OutputFilesDir</code> (created when this command is issued). In addition, the contents of these files will also be displayed in your current PuTYY [or terminal] window (Figure 12).</p>
<table>
<thead>
<tr class="header">
<th style="text-align: center;"><img src="hw0-images/11-make_test.png" alt="hw0-images/11-make_test.png" /></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;">Figure 12: Test results for a <em>correctly coded</em> project.</td>
</tr>
</tbody>
</table>
<p>This should do it. For actual assignments, I might add other recipes (<code>Makefile</code> rules) that allow you to perform other tasks. This might come in handy in the event of a regrade request, or to clarify a possible score deduction.</p>
<p>As a last [optional] step, you might want to <em>clean</em> your remote directory by eliminating all those extra files generated by this process. If that is the case, issue the command</p>
<pre><code>make clean</code></pre>
<p>This will remove all files not present at the beginning of Step 3.</p>
<h2 id="troubleshooting">Troubleshooting</h2>
<p>If for some reason you are not able to complete this walk through, make sure to get in touch with us to help you correct any issues.</p>
<hr />
<p><a href="../..">Return to main PIC course website</a></p>
<section class="footnotes">
<hr />
<ol>
<li id="fn1"><p>IDE = Integrated Development Environment. Some common IDEs are: Visual Studio, Xcode, Code::Blocks, etc.<a href="#fnref1" class="footnote-back">↩</a></p></li>
<li id="fn2"><p>WinSCP does provide a way to run remote commands, but this feature is rather rudimentary.<a href="#fnref2" class="footnote-back">↩</a></p></li>
</ol>
</section>
</body>
</html>
