#include <iostream>
#include "hw0.h"

int main(){
    std::cout << "Running driver 2.\n";
    std::cout << "This is supposed to be a more complex driver file...\n";
    display_message();
    display_message();
    display_message();
    std::cout << "End of driver 2.\n\n";
    return 0;
}
