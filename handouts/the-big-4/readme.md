# The Big 4

In preparation for the near future, where we will be _home-cooking_ our own
vector class, we need to understand the so-called _big 4_. More specifically, we
want to know _their default behaviors_, and more importantly, _how to override
them_ should we wish to change this behavior.

The members of this group are:

*   the _default constructor_,
*   the _copy constructor_,
*   the _destructor_, and
*   the _assignment operator_.

In the paragraphs below we will

i.  figure out the signatures of these special functions, and
i.  figure out how they behave (regardless of the class they belong to). 

To help us with this task, let us code a `Thing` class with a couple of
_primitive_ fields (_i.e.,_ variables), as well as a couple of _non-primitive_
ones (_i.e.,_ objects). Also, to keep things simple we'll use a `struct`[^one]
as opposed to a `class`.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}  
#include <string>  // std::string
#include <vector>  // std::vector

using namespace::std;  // For didactic purposes, try to avoid.

struct Thing {
    // Fields [primitive types]
    int the_int_var;
    double the_double_var;

    // Fields [non-primitive types]
    string the_string_obj;
    vector<bool> the_vector_obj;

    // The big 4 will be declared here as we _figure out_ their signatures
};
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  

[^one]: Recall that a `struct` is a _public class_.


## The default constructor

For this function the signature is simple: 

*   we need the name of the class (_e.g.,_ `Cosa`, `Thing`, `vector`, etc.), and
*   we need to make sure no parameters are provided.

And this is how our `Thing` class should look now

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
struct Thing {
    // Fields [primitive and non-primitive]
    ...

    // Default constructor (declaration)
    Thing();
};

// Default constructor (definition)
Thing::Thing(){
    // Initialization of fields according to _default_ behavior
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 


## The copy constructor

Since this is also a constructor it should be named after the class. In
addition, it should receive one parameter: an existing object belonging to the
same class.


### The _wrong_ way

The statement above might lead us to think that the signature (as it would
appear inside the class) should look like this:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
class Thing{
    ...
    Thing( Thing );
};
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

However, the statement above does not compile. A long time ago it used to, but
fortunately for all of us, compilers have gotten better at preventing developers
from _shooting themselves in the foot_. The problem with the proposed signature
is the attempt to pass the parameter **by value**. As you know, a value
parameter is not sent to a function, instead, a copy of the parameter is made,
and then this copy is the one that the function receives. What you might not
know, is that in theory, the copy is made by the copy constructor of the
corresponding class[^two].

In other words, if the statement above were to compile, then whenever the copy
constructor was called, it would...

> attempt to make a copy of the parameter by calling the copy constructor. This
> instance of the copy constructor would then...  
>
> attempt to make a copy of the parameter by calling the copy constructor. This
> instance of the copy constructor would then...  
>
> attempt to make a copy of the parameter by calling the copy constructor. This
> instance of the copy constructor would then...  

The result is known as **infinite recursion**. In theory, this could go on
forever. However in real life sometimes _infinity is closer than one thinks_.
In this case every call to the copy constructor requires allocation of memory
for the `Thing` object that is supposed to be created. Since there are multiple
calls to the copy constructor, multiple requests for memory are made.
Eventually the memory allocated for the full program runs out, and the execution
terminates due to something known as a _stack overflow_.

[^two]: This statement is actually false. In practice, the compiler might decide
  not to make a copy, or it might decide to use yet another special constructor
  that shall remain unnamed for now.


### The _right_ way

To fix the issue described above, one can simply use a reference parameter
instead. Either of the two signatures below (as written outside of the class) do
actually compile.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
// Copy constructor: parameter passed by reference
Thing::Thing( Thing& source ){
    ...
}
// Copy constructor: parameter passed by `const` reference
Thing::Thing( const Thing& source ){
    ...
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

However, in most cases the version that takes the parameter by `const` reference
is preferred. _Can you explain why?_


## The destructor

This one is also easy. The only difference in this function's signature and the
one for the default constructor is the addition of one character, namely `~`.

The signature is then `Thing::~Thing()`.


## The assignment operator

This operator needs a `Thing` object (parameter) to be used to update the
contents of the implicit object. As such, one might think that the signature (as
written inside the class interface) should be: `void operator=( Thing )`.
However, in order to allow for _chaining_ (_e.g.,_ `t1 = t2 = Thing();`), we
will return a `Thing` object by reference (see _rule of thumb for types of
return_, page 565 in Horstmann's _Big C++_).

The passing of the parameters can either be by value, or by reference. To avoid
unnecessary copies of `Thing` objects, we will pass the parameter by `const`
reference. Here is our proposed signature

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
// Assignment operator (definition)
Thing& Thing::operator=( const Thing& rhs ){
    ...
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 


## Testing the big 4

Now that we have figured out the signatures of _the big 4_, let us discuss the
behavior of these functions. In other words, let us understand what is the code
the compiler provides [if any] for these special functions. For starters, let us
write a simple program that puts together our proposed definition of the `Thing`
class, as well as a `main()` routine that ensures all of the big 4 functions are
called.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp .numberLines}
#include <string>
#include <vector>

using namespace::std;  // For didactic purposes, try to avoid.

struct Thing {
    // The fields...
    int the_int_var;
    double the_double_var;
    string the_string_obj;
    vector<bool> the_vector_obj;
};

int main(){
    Thing t1;           // Default constructor

    Thing t2(t1);       // Copy constructor
    Thing t3 = t1;      // Copy constructor. [Wait! What?]

    t1 = t2;            // Assignment operator

    return 0;           // Destructor (x3)
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

[Click here to access the code above][simple]

> **Important:** Please note that the statement `Thing t3 = t1;` in line 18,
> **is not** a call to the assignment operator. To see this, observe that since
> the object `t3` does not exist, the _compiler version_ of the assignment
> operator, namely `t3.operator=(t1);` doesn't make sense at all.  Instead the
> object `t3` needs to be created (_i.e.,_ a constructor should be used) via
> [the information enclosed in] the object `t1`.

[simple]: big4-files/simple-thing.cpp


## The default behavior: 1st attempt

Since we are not overriding any of the big 4, and since these functions are
being called within the driver, the compiler provides code for them. It goes
through the list of member fields, and performs the default action: 

*   for the _default constructor_, this action corresponds to either reserving
    memory for variables, or calling the default constructor for objects; 
*   for the _destructor_, this action corresponds to either releasing memory for
    variables, or calling the class destructor for objects;
*   for the _copy constructor_, this action corresponds to the reservation of
    memory and/or construction of objects with the information provided via the
    explicit parameter. Lastly,
*   for the _assignment operator_, this action corresponds to a _member-wise_
    assignation of values of the class member fields. They are assigned the
    corresponding values from the explicit parameter.

For example, the current state of our class is equivalent[^three] to the
following implementation of the big 4:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
struct Thing {
    // The fields...
    int the_int_var;
    double the_double_var;
    string the_string_obj;
    vector<bool> the_vector_obj;

    // Default constructor
    Thing(){
        // No need to worry about `the_int_var`, nor `the_double_var`. Memory
        // was already reserved for them at this point.

        the_string_obj = string();       // Calls deflt. constr. for string
        the_vector_obj = vector<bool>(); // Calls deflt. constr. for vector
    }

    // Destructor
    ~Thing(){
        // No need to worry about stack objects, the memory associated to them
        // will be automatically released. In the case of objects, a call to
        // the corresponding destructor will be made. If we had to make these
        // calls ourselves, they would look like this:
        // the_string_obj.~string();          // Calls string destructor
        // the_vector_obj.~vector<bool>();    // Calls vector destructor
    }

    // Copy constructor
    Thing( const Thing& source ){
        the_int_var = source.the_int_var;
        the_double_var = source.the_double_var;
        the_string_obj = source.the_string_obj;
        the_vector_obj = source.the_vector_obj;
    }

    // Assignment operator
    Thing& operator=( const Thing& rhs ){
        the_int_var = rhs.the_int_var;
        the_double_var = rhs.the_double_var;
        the_string_obj = rhs.the_string_obj;
        the_vector_obj = rhs.the_vector_obj;

        return *this;
    }
};
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

Notice that the body of the copy constructor and the assignment operator, are
very similar. This is because our current implementation of these functions is
not very efficient. For example, in the case of the copy constructor, first all
fields are created/initialized with default values[^four], and afterwards they
are replaced with the contents of the fields of `source` (via a member-wise call
to the corresponding assignment operators).

[^three]: Here by equivalent we mean that the final state of the `Thing` objects
  are indistinguishable from each other, although in most cases, the code
  provided by the compiler will be more efficient.
[^four]: This also happens when our current implementation of the default
  constructor is called.


## The default behavior: 2nd attempt

Let us rewrite our class with an emphasis on being more efficient. More
specifically, we will use _initializer lists_ to make sure possible extra calls
to default constructors for object fields are avoided. Also, to convince
ourselves that we are actually overriding _the big 4_, let us add simple
messages indicating when functions are being called.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
#include <iostream>    // std::cout
...

struct Thing {
    int the_int_var;
    double the_double_var;
    string the_string_obj;
    vector<bool> the_vector_obj;

    Thing()
      : the_string_obj(), the_vector_obj() {
        // Memory for primitive types has already been reserved once we reach
        // this point, although these memory locations might contain garbage.
        // For non-primitives, we indicated that we specifically want the
        // default constructor to be used.
        cout << "Default constructor\n";
    }

    ~Thing(){
        // All stack objects are released/destroyed here.
        cout << "Destructor\n";
    }

    Thing( const Thing& source )
      : the_int_var( source.the_int_var ), the_double_var( source.the_double_var ),
        the_string_obj( source.the_string_obj ), the_vector_obj( source.the_vector_obj ) {
        // All fields are already initialized here... Moreover we indicated that
        // we specifically want the copy constructor to be used.
        cout << "Copy constructor\n";
    }

    Thing& operator=( const Thing& rhs ){
        the_int_var = rhs.the_int_var;
        the_double_var = rhs.the_double_var;
        the_string_obj = rhs.the_string_obj;
        the_vector_obj = rhs.the_vector_obj;
        cout << "Assignment operator\n";

        return *this;
    }
};
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 


## Summary

As you can see from this example, the default behavior of _the big 4_ is
independent of the class they belong to. That is, the compiler doesn't need to
to know the specifics of the class to provide default code for them:

*   for the _default constructor_ 
    -   all non-primitive fields are initialized with calls to corresponding
        default constructors;
    -   all primitive fields are assigned a memory location to store their
        future values (these locations might already house _garbage_);
*   for the _copy constructor_ all fields are initialized with calls to
    corresponding copy constructors;
*   for the _assignment operator_ all fields are [re]assigned  via calls to
    corresponding assignment operators; and
*   for the _destructor_, all fields are released/destroyed  via calls to
    corresponding class destructors.

---

## Test code

For your convenience, [here is a file][example] containing the code discussed in
this handout.

[example]: big4-files/thing.cpp


---

[Return to main PIC course website][main]

[main]: ../..

