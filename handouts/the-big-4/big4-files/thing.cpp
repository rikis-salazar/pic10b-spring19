#include <iostream>
#include <vector>
#include <string>

using namespace::std;  // For didactic purposes, try to avoid.

// The `Thing`
struct Thing {
    int the_int_var;
    double the_double_var;
    string the_string_obj;
    vector<bool> the_vector_obj;

    Thing()
      : the_string_obj(), the_vector_obj() {
        // Memory for primitive types has already been reserved once we reach
        // this point, although these memory locations might contain garbage.
        // For non-primitives, we indicated that we specifically want the
        // default constructor to be used.
        cout << "Default constructor\n";
    }

    ~Thing(){
        // All stack objects are released/destroyed here.
        cout << "Destructor\n";
    }

    Thing( const Thing& source )
      : the_int_var( source.the_int_var ), the_double_var( source.the_double_var ),
        the_string_obj( source.the_string_obj ), the_vector_obj( source.the_vector_obj ) {
        // All fields are already initialized here... Moreover we indicated that
        // we specifically want the copy constructor to be used.
        cout << "Copy constructor\n";
    }

    Thing& operator=( const Thing& rhs ){
        the_int_var = rhs.the_int_var;
        the_double_var = rhs.the_double_var;
        the_string_obj = rhs.the_string_obj;
        the_vector_obj = rhs.the_vector_obj;
        cout << "Assignment operator\n";

        return *this;
    }
};


// The driver
int main(){

    Thing t1;           // Default constructor

    Thing t2(t1);       // Copy constructor
    Thing t3 = t1;      // Copy constructor. [Wait! What?]

    t1 = t2;            // Assignment operator

    return 0;           // Destructor (x3)
}


/** 
    OUTPUT:

        Default constructor
        Copy constructor
        Copy constructor
        Assignment operator
        Destructor
        Destructor
        Destructor
**/
