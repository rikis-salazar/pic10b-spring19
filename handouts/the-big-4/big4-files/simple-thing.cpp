#include <string>
#include <vector>

using namespace::std;  // For didactic purposes, try to avoid.

struct Thing {
    // The fields...
    int the_int_var;
    double the_double_var;
    string the_string_obj;
    vector<bool> the_vector_obj;
};

int main(){
    Thing t1;           // Default constructor

    Thing t2(t1);       // Copy constructor
    Thing t3 = t1;      // Copy constructor. [Wait! What?]

    t1 = t2;            // Assignment operator

    return 0;           // Destructor (x3)
}
