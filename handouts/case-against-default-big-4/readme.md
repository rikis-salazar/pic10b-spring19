# The case against the default Big 4 behavior

This is handout is based on the code presented during lecture[^one]. The goal is
to make the case that, for certain classes, the programmer needs to take control
of the big 4. That is, it needs to code _class-specific_ versions of these functions.

[^one]: Recall that we discussed a similar class, namely `TheInt` class.

## The `BasicNode` class

Later during this quarter, we will discuss data structures that are composed of
_nodes_ (_e.g.,_ list, binary trees, etc.). These nodes are primarily used to
store data; however, in some cases they also provide extra functionality: like
the ability to find other nodes, and to modify the data stored within them.

For this handout, we want a basic node that stores some data (`the_value`), as
well as a way for us to handle this data. To make things a little more
interesting, we will add a pointer to the data, and for a reason that will
become apparent later[^two], **we will only access the data through the
pointer**.

[^two]: If you need a reason to make sense of this handout, let us just say
  that, after connecting the pointer to the data, we'll suspiciously _forget_
  the name of the variable that holds said data. 


## The member fields

From the previous statements we gather that we need two member fields:

*   an `int` to hold `the_value`, and
*   a pointer to this value.

To declare a pointer we need to let the compiler know what _type [of pointer]_
we want[^three]: if we plan to use it to handle an `int` variable, then we want
an _int pointer_ (`int*`); if instead we want it to handle a `double`, we need a
_double pointer_ (`double*`), etc.

In this case, since `the_value` will hold an `int`, we want an _int pointer_
(`int*`), which we will call `the_ptr`.

The class interface should then look like this

~~~~~ {.cpp}  
class BasicNode {
  private:
    int the_value;
    int* the_ptr;
  . . .
};
~~~~~  

[^three]: Actually this is not quite correct; in a sense _all pointers are
  equal_ as they all store memory addresses, and these addresses look the same
  regardless of whether they _store_ an `int`, a `double`, a `Fraction`, etc.


## A few points[^four] about _pointers_

When dealing with pointers **it is of paramount importance that they be
properly initialized**. This is because

*    pointers are primitive (built-in) types, and
*    they allow you to access the memory locations whose addresses they store.

The first point above roughly translates into the statement:

> _The compiler will reserve memory for the ~~variable~~ pointer, but the stored
> ~~value~~ address will likely be garbage_.

This alone should be enough to convince you that uninitialized pointers are a
dangerous proposition. But to try to drive the point further, let us pretend
that pointers are like _house keys_. Moreover, whether we like it or not, these
_keys_ are always tagged with a house address.

Under this analogy,

*   declaring a pointer corresponds to receiving a key with a _random_ house
    address tagged on it;

whereas

*   initializing a pointer corresponds to _re-tagging_ the key with a house
    address that we [hopefully] determined to be safe to access.

Now imagine yourself trying to use a ~~pointer~~ house key that wasn't properly
~~initialized~~ re-tagged. When you try to enter the house at the random
address, say [_12345 Westwood Ave_][no-house], several things could happen:

i.  you successfully unlock the door and enter the house; or
i.  while trying to unlock the door, the police shows up and you are arrested;
    or
i.  you wasted time and effort as there is no house at this address.

> Question: _Which of the two scenarios above would you rather be in?_

To avoid any of the previously described scenarios make sure pointers are always
initialized to a proper (_i.e.,_ valid) and safe (_i.e.,_ under control of your
program) memory address.


**Is there a _default_ pointer that can be used?**

Why, yes! The C++11 standard introduced the `nullptr` keyword (_e.g.,_ `int* ptr
= nullptr;`).

> For older standards, the choices are zero (as in `int* ptr = 0;`), or
> `NULL`[^five] (as in `int* ptr = NULL;`).[^six]


[link]: https://stackoverflow.com/questions/176989/do-you-use-null-or-0-zero-for-pointers-in-c
[no-house]: https://www.google.com/maps/@33.9995371,-118.3988326,3a,75y,180h,90t/data=!3m7!1e1!3m5!1s9JIYC4vvSDh4XgXQ05B_cg!2e0!6s%2F%2Fgeo2.ggpht.com%2Fcbk%3Fpanoid%3D9JIYC4vvSDh4XgXQ05B_cg%26output%3Dthumbnail%26cb_client%3Dmaps_sv.tactile.gps%26thumb%3D2%26w%3D203%26h%3D100%26yaw%3D53.921455%26pitch%3D0%26thumbfov%3D100!7i13312!8i6656

[^four]: Pun intended.
[^five]: The macro `NULL` is defined in `<cstdlib>`, `<cstddef>`, as well as
  some other standard libraries.
[^six]: While using `NULL` might make code a bit more readable, it also forces
  you to avoid implicit conversions between pointers and boolean values. Have a
  look at [this link][link] to find out more about it.


## The class constructors

Once we have settled on the member fields, and have learned about the dangers of
uninitialized pointers, we should turn our attention to the class constructors.
In this case, we would like the statements

~~~~~ {.cpp}
BasicNode a;            // Default constructor
BasicNode b(2019);      // 1-parameter constructor
~~~~~

to _produce_ the following objects

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ___________________________        ___________________________
   /             a             \      /             b             \
  +-----------------------------+    +-----------------------------+
  |  the_ptr          +------+  |    |  the_ptr          +------+  |
  |     * ----------> |    0 |  |    |     * ----------> | 2019 |  |
  |                   +------+  |    |                   +------+  |
  +-----------------------------+    +-----------------------------+
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

where

*   the star symbol (`*`) inside the objects represents the member pointer,
*   _the box_ represents the integer variable inside the node, and
*   _the arrow_ emanating from `*` is a visual representation of the fact that a
    memory address is actually stored in the pointer variable.

> **Note:**
>
> To be consistent with other statements in this document, the name of the int
> variable `the_data`[^pq0] will not appear inside these visual representations.

The actual code is not very different from other classes we have studied. The
snippet below will do the trick.

~~~~~ {.cpp}  
class BasicNode {
  . . .

  public:
    BasicNode( int n = 0 ){
        // The operand `&` returns the memory address of its right hand side.
        the_ptr = &the_value;
        *the_ptr = n;
        // The statement above is equivalent to
        //     the_value = n;
    }
  . . .
};
~~~~~  

[^pq0]: Pop quiz 0 was here. ~~Yes, I know. The actual member field is called
  `the_value`, not `the_data`. However, this intentional typo is here to give me
  an idea of how many students are reading the footnotes.~~ Apparently only two
  students read the footnotes. ~~If you are one of them, next time you meet me,
  please hand over a piece of paper with your SID number, as well as the text
  "Pop quiz 0: `BasicNode::the_data` should be `BasicNode::the_value`".~~ No
  more quiz submissions are being accepted.

## The big 3 (default behavior)

Since the goal of this document is to illustrate the dangers of letting the
compiler provide these members for us, let us first explicitly ask for the
default versions of these functions. This can be done with the following
additional lines of code:

~~~~~ {.cpp}  
class BasicNode {
  . . .

  public:
    . . .

    BasicNode( const BasicNode& ) = default;
    BasicNode& operator=( const BasicNode& ) = default;
    ~BasicNode() = default;
};
~~~~~  

> **Note:**
>
> The last three statements within the class interface are actually not needed
> (as the compiler provides _the big 3_ anyway). I am including them here
> because the code discussed during lecture containing explanations about the
> `default` keyword will not be published.


## Other functions

We'll keep it simple. Let us add

*   a _setter_ to make it possible to alter the values of a node after its
    construction; and
*   an overload of `operator<<` to send the contents of the node to an `ostream`
    object.

> **Note:**
>
> Since we have no _getters_, the non-member `<<` will be a _friend_ of the
> class.

Here are the extra lines of code:

~~~~~ {.cpp}  
class BasicNode {
  . . .

  public:
    . . .

    void set_value( int n ){ *the_ptr = n; }

  friend std::ostream& operator<<( std::ostream&, const BasicNode& );
};

std::ostream& operator<<( std::ostream& out, const BasicNode& node ){
    out << "The memory address " << node.the_ptr
        << ", holds the data " << *node.the_ptr << ".\n";
    return out;
}
~~~~~  

> **Note:**
>
> No parenthesis are needed in the expression `*node.the_ptr`. This is because
>
> i.  `node` is an object for whom the _dot_ operator makes sense,
> i.  `the_ptr` is a pointer for whom the _star_ operator makes sense, and
> i.  the _dot_ operator has precedence over the _star_ operator.
>
> Compare this to another expression that we have seen during lecture, namely
> `(*this).a_member_field`. In this case, the parenthesis are needed because
>
> i.  `this` is a pointer for whom the _dot_ operator **does not** makes sense
>     (it does make sense for `*this` which is an object); and
> i.  the _dot_ operator has precedence over the _star_ operator.


## The driver

Again, we'll keep it simple. We want to

1.  Construct a default object `a` (see visual representation above).
1.  Construct a non-default object `b` (see visual representation above).
1.  Inspect `a` and `b` (_e.g.,_ by _sending them_ to the console).
1.  Create a copy of `b` using the compiler provided copy constructor.
1.  Inspect the copy described in the previous step.
1.  Replace the contents of `a` with the ones in `b` via the compiler provided
    assignment operator.
1.  Re-inspect `a` to verify that changes took place.

The snippet below should work for us:

~~~~~ {.cpp}
int main(){
    BasicNode a;
    BasicNode b(2019);
    std::cout << "a:\t" << a;
    std::cout << "b:\t" << b;

    BasicNode b_copy(b);
    std::cout << "b_copy:\t" << b_copy;

    a = b;
    std::cout << "a:\t" << a;
    . . .
}
~~~~~


## The output

At first glance it seems that the class is working just fine. Here is a sample
output produced by the lines of code from the previous section

~~~~~
a:      The memory address 0x7ffd2d21b900, holds the data 0.
b:      The memory address 0x7ffd2d21b910, holds the data 2019.
b_copy: The memory address 0x7ffd2d21b910, holds the data 2019.
a:      The memory address 0x7ffd2d21b910, holds the data 2019.
~~~~~

However, further inspection reveals something curious: `b` and its copy
`b_copy`, _share_ the same memory location.

We seem to have the following scenario

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ___________________________        ___________________________
   /             b             \      /           b_copy          \
  +-----------------------------+    +-----------------------------+
  |  the_ptr          +------+  |    |  the_ptr          +------+  |
  |     * ----------> | 2019 | <---------- *             | ???? |  |
  |                   +------+  |    |                   +------+  |
  +-----------------------------+    +-----------------------------+
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

as opposed to the following one

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ___________________________        ___________________________
   /             b             \      /           b_copy          \
  +-----------------------------+    +-----------------------------+
  |  the_ptr          +------+  |    |  the_ptr          +------+  |
  |     * ----------> | 2019 |  |    |     * ----------> | 2019 |  |
  |                   +------+  |    |                   +------+  |
  +-----------------------------+    +-----------------------------+
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

which is the one we intend for these nodes.


## The driver (revisited)

To convince ourselves that we indeed have a big issue, let us add some extra
steps to the driver. In addition to the ones listed above, let us

8.  Reset the internal value of `a` back to zero.
8.  Re-inspect the contents of the three nodes.

This lines should do the trick:

~~~~~ {.cpp}
int main(){
    . . .
    std::cout << "\nReseting 'a' and re-inspecting objects...\n";
    a.set_value(0);
    std::cout << "a:\t" << a;
    std::cout << "b:\t" << b;
    std::cout << "b_copy:\t" << b_copy;
    . . .
}
~~~~~

Now it should be clear from the full output

~~~~~
a:      The memory address 0x7ffd2d21b900, holds the data 0.
b:      The memory address 0x7ffd2d21b910, holds the data 2019.
b_copy: The memory address 0x7ffd2d21b910, holds the data 2019.
a:      The memory address 0x7ffd2d21b910, holds the data 2019.

Reseting 'a' and re-inspecting objects...
a:      The memory address 0x7ffd2d21b910, holds the data 0.
b:      The memory address 0x7ffd2d21b910, holds the data 0.
b_copy: The memory address 0x7ffd2d21b910, holds the data 0.
~~~~~

that we cannot trust the compiler with the big 4.

> [_¡Oh! ¿Y ahora quién podrá ~~defenderme~~ ayudarnos?_][chapulin]


[chapulin]: https://www.youtube.com/watch?v=IMZt2hocabs&start=46

## Test code

For your convenience, [here is a file][example] containing the code discussed in
this handout.

[example]: case-against-files/basic-node-default.cpp


---

[Return to main PIC course website][main]

[main]: ../..

