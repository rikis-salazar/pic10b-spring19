# A `DoublyLinkedList` class  

## The goal

To provide a _working_ `DoublyLinkedList<T>` class that you can use to code your
very own _Stack<T>_ class needed for one of your homework assignments.


### What is a _doubly linked list_, anyway?

During lecture we agreed that the main characteristic of a linked list is that
_insert/delete operations are fast $\left(\mathbf{O}(1)\right)$, regardless of
where the operation takes place_. In addition, since other data structures like
_stacks_, or _queues_, are also supposed to be fast at handling specific
insert/delete operations, linked lists become the perfect candidates to act as
underlying containers and provide the desired functionality (see container
adapters section).


## The building blocks

To implement our list, we will use three classes: `Node`, `Iterator`, and
`DoublyLinkedList`. Here are some of their main features:

*   A `Node` object
    -   is **created** only _when needed_ with the purpose of storing data.
    -   by itself cannot _enter_ a list. Other objects will have to handle this
        process.
    -   **has** records of its _neighbors_, but cannot _broadcast_ this
        information to the public. Other entities have to know the internal
        structure of a node in order to _get_ this information.

    Summarizing:

    > A template class with three member fields (`data`, `next`, `prev`), and a
    > single public member function (constructor). It is also pretty useless by
    > itself and needs friends to serve a purpose. Node objects are only
    > requested from _the heap_, and they are released (deleted) as soon as
    > possible.  

*   An `Iterator` object
    -   is **created** by a list object with the purpose of _marking one of its
        spots_ (nodes).
    -   often is asked to travel the list forward or backwards; as such it needs
        to know the internal structure of a node in order to locate the node's
        neighbors.
    -   is able to determine whether or not the spot it is marking is in the
        list.
    -   allows a list object to modify/change the spot it is marking.

    Summarizing:

    > A template class with one member field (`the_node`), a constructor, and
    > functions that allow the iterator to travel the list and detect when
    > `the_node` is a valid one. Objects from this class _know their way
    > through_ nodes, as well as lists; and they also let list objects handle
    > its only field. In other words, iterator objects are friends of node and
    > list objects, and they also let list objects _befriend_ them.

*   A `DoublyLinkedList` object
    -   manages the list. It is in charge of all lists operations: construct,
        insert, delete, copy, etc.
    -   creates/destroys nodes whenever it is needed. It should make sure
        dynamic memory is properly handled.
    -   knows where is the head, and the tail of the list. It also lets
        iterators use this information to mark the first, and last nodes.

    Summarizing:

    > A template class with two member fields (`head`, and `tail`), a
    > constructor, and all sorts of functions needed to manage a list. It _knows
    > its way around_ iterators and nodes, and it is in charge of implementing
    > proper overloads of the big 4. It also lets iterators acces information
    > regarding the head and tail of the list. In other words, list objects are
    > friends of nodes and iterarors, and let iterators befriend them.


### Why so many templates?

The short answer is: ~~because I say so!~~ to simplify the implementation by not
having to worry between the intricate relations between these three classes.

~~The long answer is: $\mbox{b e c a u s e ~~ I ~~ s a y ~~ so !}$~~


## Implementation 

Without further ado, let us _home-cook_ our list.


### The `Node<T>` class

The class summary (see previous section), translates to:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template <typename T>
class Node {
  private:
    T data;
    Node* prev;
    Node* next;

  public:
    Node( const T& , Node* /*p*/ = nullptr, Node* /*n*/ = nullptr );

  friend class Iterator<T>;
  friend class DoublyLinkedList<T>;
};

template <typename T>
Node<T>::Node( const T& d, Node<T>* p, Node<T>* n )
  : data(d), prev(p), next(n) { }
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Note:**

For your _Hanoi puzzle_ assignment you can use a very similar `Stack_Node`
class. In this case only one link (say `node_below`) is needed, and the class
constructor is very similar to the one coded here.


### The `Iterator<T>` class

The class summary (see previous section), translates to:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template <typename T>
class Iterator {
  private:
    Node<T>* the_node;

  public:
    Iterator( Node<T>* /*n*/ = nullptr );
    T get_value() const;
    bool is_null() const;
    void move_forward();
    void move_backward();

  friend class DoublyLinkedList<T>;
};

template <typename T>
Iterator<T>::Iterator( Node<T>* n ) : the_node(n) { }

template <typename T>
T Iterator<T>::get_value() const {
    return the_node->data;
}

template <typename T>
T Iterator<T>::is_null() const {
    return the_node == nullptr;
}

template <typename T>
void Iterator<T>::move_forward(){
    the_node = the_node->next;
}

template <typename T>
void Iterator<T>::move_backward(){
    the_node = the_node->prev;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Note:**

For your _Hanoi puzzle_ assignment iterators are not needed. However, since the
member functions are _one-liners_, you might want to consider implementing a
similar class[^only_after] for practice purposes.  

[^only_after]: You should only attempt to code this class once your project
  works as expected without it. There is no extra credit for a project that
  implements stack iterators.


### The `Iterator<T>` class

The class summary (see previous section), translates to:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template <typename T>
class DoublyLinkedList {
  private:
    Node<T>* head;
    Node<T>* tail;

  public:
    DoublyLinkedList();
    void push_back( const T& );
    void pop_back();
    ...

    ~DoublyLinkedList();
    DoublyLinkedList( const DoublyLinkedList& );
    DoublyLinkedList& operator=( const DoublyLinkedList& );
    ...

  private:
    void deep_copy();
    void release_memory();

  friend class Iterator<T>;
};
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


### Warm up before the big 3

This is the class that actually has to manage dynamic memory (_i.e.,_ the big
4), hence we are expected to code the big 3. To make our life easier, let us
code the default constructor, as well as a couple of helper functions that will
make our code more readable.

The default constructor is fairly straightforward.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template <typename T>
DoublyLinkedList<T>::DoublyLinkedList() : head(nullptr), tail(nullptr) { }
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Next, we deal with insertions at the back/front of the list...

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template <typename T>
void DoublyLinkedList<T>::push_back( const T& data ){
    // Get a new node and store the data
    Node<T>* new_node = new Node<T>(data);

    // If the list is empty, point head & tail to this node
    if ( head == nullptr )
        head = tail = new_node;
    // If not, rearrange the links and update tail.
    else{
        tail->next = new_node;
        newi_node->prev = tail;
        tail = new_node;
    }
}

template <typename T>
void DoublyLinkedList<T>::push_front( const T& data ){
    // Your turn!
    ...
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

... as well as deletions at both ends of the list

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template <typename T>
void DoublyLinkedList<T>::pop_back(){
    // Only pop a node if there is one.
    if ( head != nullptr ){
        // Mark the node that will be deleted.
        Node<T>* old_tail = tail;

        // At least two nodes? reorganize.
        if ( head != tail ){
            tail = tail->prev;
            tail->next = nullptr;
        }
        else // Only one node is left: reset head and tail.
            head = tail = nullptr;

        // Release memory
        delete old_tail;
    }
}

template <typename T>
void DoublyLinkedList<T>::pop_front(){
    // Your turn!
    ...
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Lastly, let us code a _helper_ function `deep_copy` that will come in handy when
implementing the big 3.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template <typename T>
void DoublyLinkedList<T>::deep_copy( const DoublyLinkedList<T>& b ){
    Iterator<T> iter(b.head);
    while ( !it.is_null() ){
        push_back( iter.get_value() );
        iter.move_forward();
    }
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Note:**

For your _Hanoi puzzle_ assignment you can follow a similar approach to the one
presented here. Just keep in mind that iterators might not be available to you.
If this is the case, in order to code a `deep_copy` function you will have to
work with raw `Stack_Node<T>*` pointers.


## The big 3

Turns out our job is now greatly simplified if we put to use the functions we
coded in the previous section. 


### The copy constructor

Could not be any easier (as long as you do not forget to correctly initialize
the class fields).

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template <typename T>
DoublyLinkedList<T>::DoublyLinkedList( const DoublyLinkedList<T>& b )
  : head(nullptr), tail(nullptr) {
    deep_copy(b);
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


### The destructor

Follows a simple principle: _if the list is not empty, pop a single node; repeat
as needed._ Also, since memory will also have to be released when the assignment
operator is called, we will place the common code inside a member helper
function `release_memory`.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template <typename T>
DoublyLinkedList<T>::~DoublyLinkedList(){
    release_memory();
}

template <typename T>
void DoublyLinkedList<T>::release_memory(){
    while ( head != nullptr )
        pop_back();
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


### The assignment operator

Not bad at all, just remember the four steps: _self-assignment?, release, deep
copy,_ and _return_.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template <typename T>
DoublyLinkedList<T>& DoublyLinkedList<T>::operator=( DoublyLinkedList<T>& rhs ){
    if ( this != &rhs ){
        release_memory();
        deep_copy(rhs);
    }
    return *this;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Note:**

For your _Hanoi puzzle_, assuming you have a `deep_copy` function available, and
that this function works as expected, the approach presented here should be
enough for you to implement the big 3.


## Other member functions

We are certainly not done yet! We are missing several other _basic_ functions
(_e.g.,_ insert, insert_before, etc.). However, the purpose of this document is
not to explain in detail the implementation of our _home-made_ list, instead it
hopes to provide you with the tools you need to code your _Hanoi puzzle_
assignment.


## Is there a 'working' version oft this class I can play with?

[Why, yes!][the-list] Go ahead, check it out. It is "incomplete" so that you can
complete it, break it, fix it, break it again, etc.

The three classes discussed here are implemented in separate library files, plus
a _forward declaration_ file.

Files:

*   [`dl_declarations.h`][decl]: _Dear compiler, these are the classses that
    will be used. Thanks._
*   [`dl_node.h`][node]: The `Node<T>` class.
*   [`dl_iterator.h`][iter] The `Iterator<T>` class.
*   [`dl_list.h`][list] The `DoublyLinkedList<T>` class.
*   [`driver.cpp`][driver]: A simple driver.

[the-list]: dl-list-files
[decl]: dl-list-files/dl_declarations.h
[node]: dl-list-files/dl_node.h
[iter]: dl-list-files/dl_iterator.h
[list]: dl-list-files/dl_list.h
[driver]: dl-list-files/driver.cpp

---

[Return to main PIC course website][main]

[main]: ../..

