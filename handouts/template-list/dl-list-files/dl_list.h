#ifndef DL_LIST_H
#define DL_LIST_H

#include <cstddef>           // std::size_t, NULL (not used here)

#include "dl_declarations.h"
#include "dl_node.h"         // Node<T>
#include "dl_iterator.h"     // Iterator<T>


// Interface
template <typename T>
class DoublyLinkedList {
  private:
    Node<T>* head;
    Node<T>* tail;

    // (3.i) Common statements in copy constructor and `operator=`
    void deep_copy( const DoublyLinkedList& );
    void release_memory();


    public:
    DoublyLinkedList();

    // (1) `head` and `tail` are private. To know where the list begins/ends we 
    //      need accessors.
    Iterator<T> first() const;
    Iterator<T> last() const;
    // Note:
    //     "Real" iterators use:
    //     - `begin()` instead of first, and
    //     - `end()`, which is the node in `last()->next`

    // (2) Insertion and deletion of nodes (the easy cases)
    void push_front( const T& );
    void pop_front();
    void push_back( const T& );
    void pop_back();

    // (3.ii) The rest of 'the big 4'. See (3.i) in the 
    //        private section of this interface
    ~DoublyLinkedList();
    DoublyLinkedList( const DoublyLinkedList& );
    DoublyLinkedList& operator=( const DoublyLinkedList& );

    // (4) Addition and deletion of nodes (the not-so-easy cases)
    void insert_before( const Iterator<T>& , const T& );
    void erase( Iterator<T>& iter );

    // (5) How about you try to implement
    void insert_after( Iterator<T> , const T& );
    bool is_empty() const;

    // (6) Let us make the list look a little more like a vector
    DoublyLinkedList( const T& /*default val*/, size_t /*size*/);
    size_t size() const; 
    T& operator[]( size_t );
    T operator[]( size_t ) const;

};



template <typename T>
DoublyLinkedList<T>::DoublyLinkedList() : head(nullptr), tail(nullptr) { }


// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// (1) 
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
template <typename T>
Iterator<T> DoublyLinkedList<T>::first() const {
    Iterator<T> iter;
    iter.the_node = head;

    return iter;
}

template <typename T>
Iterator<T> DoublyLinkedList<T>::last() const {
    Iterator<T> iter;
    iter.the_node = tail;

    return iter;
}


// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// (2)
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
template <typename T>
void DoublyLinkedList<T>::push_front( const T& data ){
    // create a new node and populate it with data.
    Node<T>* new_node = new Node<T>(data);

    // If the list is empty, head and tail point to this node.
    if ( head == nullptr ){
        head = tail = new_node;
    }
    // If not, set new position for head, and rearrange links.
    else{
        head->prev = new_node;
	new_node->next = head;
	head = new_node;
    }

    return;
}

template <typename T>
void DoublyLinkedList<T>::push_back( const T& data ){
    // Create a new node and populate it with data.
    Node<T>* new_node = new Node<T>(data);

    // If the list is empty, head and tail point to this node.
    if ( head == nullptr ){
        head = tail = new_node;
    }
    // If not, set new position for tail, and rearrange links.
    else{
        tail->next = new_node;
	new_node->prev = tail;
	tail = new_node;
    }

    return;
}

template <typename T>
void DoublyLinkedList<T>::pop_back(){
    // Only proceed if there are elements in the list.
    if ( head != nullptr ){
        Node<T>* pos = tail;

	// If there are at least two nodes, reorganize the list.
	if ( head != tail ){
	    tail = tail->prev;
	    tail->next = nullptr;
	}
        // If not, calling delete will be fine (as `delete nullptr` is a no op) 
	else{
	    head = tail = nullptr;
	}

	delete pos;
    }

    return;
}

template <typename T>
void DoublyLinkedList<T>::pop_front(){
    // Only proceed if there are elements in the list
    if ( head != nullptr ){
        Node<T>* pos = head;

	// If there are at least two nodes, reorganize the list.
	if ( head != tail ){
	    head = head->next;
	    head->prev = nullptr;
	}
        // If not, calling delete will be fine (as `delete nullptr` is a no op) 
	else{
	    head = tail = nullptr;
	}

	delete pos;
    }

    return;
}



// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// (3.i)
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// To avoid repetition of code extract the common parts
// used in the copy constructor and the = operator

// To copy, continuously push_back new elements
template <typename T>
void DoublyLinkedList<T>::deep_copy( const DoublyLinkedList<T>& b ){
    Iterator<T> iter = b.first();
    while( !iter.is_null() ){
        push_back( iter.get_value() );
	iter.move_forward();
    }
}

// To release memory, continuously pop_back() until head points to null
template <typename T>
void DoublyLinkedList<T>::release_memory(){
    while ( head != nullptr )
        pop_back();
}


// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// (3.ii)
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template <typename T>
DoublyLinkedList<T>::~DoublyLinkedList(){
    release_memory();
}

template <typename T>
DoublyLinkedList<T>::DoublyLinkedList( const DoublyLinkedList<T>& b ) 
  : head(nullptr), tail(nullptr) {
    deep_copy(b);
}

// The assignment operator is almost the same as the copy constructor
template <typename T>
DoublyLinkedList<T>& DoublyLinkedList<T>::operator=(
        const DoublyLinkedList<T>& b ){

    // 1.  Only proceed if not a self-assignment
    if ( this != &b ){
        // 2.  Delete existing list... 
        release_memory();

	// 3.  Deep copy the list ( new memory is requested via push_back() )
	deep_copy(b);
    }

    // 4.  Return the implicit object.
    return *this;
}



// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// (4) 
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/**
   This is a delicate function. There is high potential for memory leaks
   and/or data corruption. One needs to make sure the iterator points 
   to a valid element of the list. In practice this is done by embedding
   the Iterator<T> class inside the private section of DoublyLinkedList<T>.
   Yes, that is a class inside another class... since our goal is not to 
   make this data structure 'bullet proof' but rather understanding the
   basic ideas behind the implementation we are going to assume all 
   pointers are pointing to a valid memory address and that they 'have the
   right' to modify the data.
*/
template <typename T>
void DoublyLinkedList<T>::insert_before( const Iterator<T>& iter,
        const T& data ){
    
    // If at the beginning of the list, simply push_front the new data.
    // Note the condition is true if both pointers point to null. 
    if ( iter.the_node == head ) 
        push_front(data);

    else{
	// iter.the_node is a long name. Let's copy it and use the copy instead.
	// This copy will disappear when this function exits and it does not
	// occupy a lot of space in memory.
	Node<T>* pos = iter.the_node;

        // Create a new node.
	Node<T>* newNode = new Node<T>(data);

	// Rearrange the pointers.
	newNode->next = pos;
	newNode->prev = pos->prev;
	(pos->prev)->next = newNode;
	pos->prev = newNode;
    }

    return;
}


// Once again, we assume the iterator is a valid one...  We also 'destroy' the
// iterator passed to the function (by making it point to `nullptr`). This is
// to avoid a dangling pointer.
template <typename T> void DoublyLinkedList<T>::erase( Iterator<T>& iter ){
    // We could also assume the list is not empty, but we'll check this
    // condition as it is just one extra line of code.
    if ( head != nullptr ) {
        // Make a temporary copy of the pointer iter.the_node.  The name is
        // shorter and it will be destroyed when the function exists. iter can
        // then be set to `nullptr`. 
        Node<T>* pos = iter.the_node;

        // If we do not take care of this step we can leave a dangling pointer
        // because the node `iter` points to will be deleted
        iter.the_node = nullptr;

        // We already have two erase functions, reuse them if appropriate.
        if ( iter.the_node == head )
            pop_front();
        else if ( iter.the_node == tail )
            pop_back();
        else {
	    // Rearrange pointers.
	    (pos->next)->prev = pos->prev;
	    (pos->prev)->next = pos->next;

	    // Release memory.
	    delete pos;
	}
    }

    return;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// (5)  Your turn ...
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// (6)  Other useful functions...
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template <typename T>
DoublyLinkedList<T>::DoublyLinkedList( const T& defaultValue, size_t numNodes ) 
  : head(nullptr), tail(nullptr) {
    // Assume/assert numNodes is positive

    for ( int i = 1 ; i <= numNodes ; i++ )
        push_back(defaultValue);

}

// Note: 
//     - This is an O(n) operation.
//     - Out current setup does not allow for an O(1) implementation.
template <typename T>
size_t DoublyLinkedList<T>::size() const { 
    size_t count = 0;
    Iterator<T> iter = first();

    while( !iter.is_null() ){
        count++;
	iter.move_forward();
    }

    return count;
}


// Note our indices run from 1 to size() unlike vectors
template <typename T>
T& DoublyLinkedList<T>::operator[]( size_t index ){
    // Assume/assert index is bigger or equal to 1

    Node<T>* position = head;
    for ( int i = 1 ; i < index ; i++ )
        position = position->next;

    return position->data; 

    // This really should be done with Iterators. That is precisely what they
    // are for. It would look like this.
    /*
        Iterator<T> it = first();
        for ( int i = 1 ; i < index ; i++ )
            it.move_forward();

        return it.get_value();
    */


}

template <typename T>
T DoublyLinkedList<T>::operator[]( size_t index ) const {
    // Assume/assert index is bigger or equal to 1

    Node<T>* position = head;
    for ( int i = 1 ; i < index ; i++ )
        position = position->next;

    return position->data; 
}

#endif
