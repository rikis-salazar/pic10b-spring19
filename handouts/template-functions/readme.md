# Template functions

This is a set of sections that will eventually be turned into a more complete
handout. It is based on the code we discussed during lecture regarding our
_home-made_ `Pic10b::min` function(s).


## The problem

Often times we find ourselves having to code various versions of the same
function, even if the code looks more or less the same in all of these versions.

Here is a simple example

~~~~~ {.cpp}
// A function that determines if the first int parameter is smaller than the
// second int parameter.
bool first_less_than_second( const int& a, const int& b ){
    if ( a < b )
        return true;
    else
        return false;
}

int main(){
    . . .
    int p = 0;
    int q = 1;
    if ( first_less_than_second(p,q) )
        std::cout << "0 is less than 1.\n";
    . . .
}
~~~~~

Suppose we wanted to add the following statements in `main()`:

~~~~~ {.cpp}
    double x = 0.0;
    double y = 1.1;
    if ( first_less_than_second(x,y) )
        std::cout << "0.0 is less than 1.1\n";
~~~~~

You might be surprised to find out that our current single overload of
`first_less_than_second(...)` actually manages to handle all of the statements
above. As a matter of fact, the output produced by the program is

~~~~~
0 is less than 1.
0.0 is less than 1.1
~~~~~

> [Use this `cpp.sh` link to confirm the claim above][link1], or [click here to
> access a file that you can compile in your favorite IDE][file1].

[link1]: http://cpp.sh/9v354
[file1]: test-code/test-01.cpp

However, this is merely a direct consequence of implicit conversions. If we add
the statements

~~~~~ {.cpp}
    x = 1.0;
    y = 1.1;
    if ( first_less_than_second(x,y) )
        std::cout << "1.0 is less than 1.1\n";
~~~~~

The output does not change. Or in other words, our single overload of
`first_less_than_second(...)` does not seem to realize that `1.0` is actually
strictly smaller than `1.1`.

> [Use this `cpp.sh` link to confirm the claim above][link2], or [click here to
> access a file that you can compile in your favorite IDE][file2].

[link2]: http://cpp.sh/6ttdx
[file2]: test-code/test-02.cpp


## The [bad] solution

No big deal, right? To fix this we simply add an overload of
`first_less_than_second` that takes `double` types:

~~~~~ {.cpp}
// A function that determines if the first int parameter ...
bool first_less_then_second( const int& a, const int& b ){
    if ( a < b )
        return true;
    else
        return false;
}

// A function that determines if the first double parameter ...
bool first_less_than_second( const double& a, const double& b ){
    if ( a < b )
        return true;
    else
        return false;
}

int main(){
    . . .
}
~~~~~

The same statements in `main()` above now produce the output:

~~~~~
0 is less than 1.
0.0 is less than 1.1
1.0 is less than 1.1
~~~~~

> [Use this `cpp.sh` link to confirm the claim above][link3], or [click here to
> access a file that you can compile in your favorite IDE][file3].

[link3]: http://cpp.sh/4sfnr
[file3]: test-code/test-03.cpp

Now imagine we wanted to extend this code to `std::string` objects

~~~~~ {.cpp}
    std::string r = "Erre";  // <-- 'R' in Spanish
    std::string s = "Ese";   // <-- 'S' in Spanish
    if ( first_less_than_second(r,s) )
        std::cout << "R is less than S\n";
~~~~~

or even to `Fraction` objects

~~~~~ {.cpp}
    Fraction half(1,2);
    Fraction one(1);
    if ( first_less_than_second(half,one) )
        std::cout << "half is less than one\n";
~~~~~

The proposed solution would then translate to more overloads: each and every
one of them with the exact same function body, and very similar signatures.

~~~~~ {.cpp}
// A function that determines if the first int parameter ...
bool first_less_than_second( const int& a, const int& b ){
    if ( a < b )
        return true;
    else
        return false;
}

// A function that determines if the first double parameter ...
bool first_less_than_second( const double& a, const double& b ){
    // Same code as above
    . . .
}

// A function that determines if the first std::string parameter ...
bool first_less_than_second( const std::string& a, const std::string& b ){
    // Same code as above
    . . .
}

// A function that determines if the first Fraction parameter ...
bool first_less_than_second( const Fraction& a, const Fraction& b ){
    // Same code as above
    . . .
}
~~~~~

This is a bad solution for several different reasons. In particular it does not
follow the _single point of maintenance_ principle. Moreover, it is very
tempting to simply _copy/paste_ code from one overload, leading to possible
multiplication of bugs.


## A _variable_ for types

Note that all of the overloads above follow the form:

~~~~~ {.cpp}
bool first_less_than_second( const ItemType& a, const ItemType& b ){
    . . .
}
~~~~~

Just like one can _store_ different values in a variable (_e.g.,_ given the
declaration `int x;` one can then set `x = 1;`, or `x = 2;`, etc.); it would be
nice to be able to _declare a variable-like_ identifier (say `ItemType`) that
allows us to _store_ name types, as in `ItemType = int`, or `ItemType = double`,
or `ItemType = std::string`, etc.

Fortunately for us, this is possible. This _variable-like_ identifier is known
as a **template parameter**. To be able to use it we need to _declare it_ via
the following syntax:

~~~~~ {.cpp}
template <typename ItemType>
~~~~~

`ItemType` is now an identifier that the compiler understands, and its _value_
could be a primitive type (_e.g.,_ `int`, `char`, `bool`, `double`, etc.), or
a class name (_e.g.,_ `std::string`, `Fraction`, `Pic10b::vector`, etc.).


## The [better] solution

With template parameters, all of the several different overloads of
`first_less_than_second(...)` can then be _encompassed_ with the following
single piece of code:

~~~~~ {.cpp}
// Determines if the first parameter is smaller than the second parameter.
template <typename ItemType>
bool first_less_than_second( const ItemType& a, const ItemType& b ){
    if ( a < b )
        return true;
    else
        return false;
}
~~~~~

When used in combination with the following driver

~~~~~ {.cpp}
int main(){
    int p = 0;
    int q = 1;
    if ( first_less_than_second(p,q) )
        std::cout << "0 is less than 1.\n";

    double x = 0.0;
    double y = 1.1;
    if ( first_less_than_second(x,y) )
        std::cout << "0.0 is less than 1.1\n";

    x = 1.0;
    y = 1.1;
    if ( first_less_than_second(x,y) )
        std::cout << "1.0 is less than 1.1\n";

    std::string r = "Erre";  // <-- 'R' in Spanish
    std::string s = "Ese";   // <-- 'S' in Spanish
    if ( first_less_than_second(r,s) )
        std::cout << "R is less than S\n";


    Fraction half(1,2);
    Fraction one(1);
    if ( first_less_than_second(half,one) )
        std::cout << "half is less than one\n";

    return 0;
}
~~~~~

The project works as expected and produces the following output:

~~~~~
0 is less than 1.
0.0 is less than 1.1
1.0 is less than 1.1
R is less than S
half is less than one
~~~~~

> [Use this `cpp.sh` link to confirm the claim above][link4], or [click here to
> access a file that you can compile in your favorite IDE][file4].

[link4]: http://cpp.sh/3727u
[file4]: test-code/test-04.cpp


## Other things to keep in mind

*   **Finite Scope**

    The scope of the `template <typename ItemType>` statement is limited to the
    body of the function that it applies to.

    ~~~~~ {.cpp}
    template <typename ItemType>
    void template_fun( const ItemType& a, std::vector<ItemType> v_copy ){
        ItemType copy_of_a(a);        // OK. ItemType is within valid scope.
        . . .
        std::vector<ItemType> w;      // Empty vector of ItemType values
        for ( int i = 0 ; i < v_copy.size() ; ++i )
            w.push_back( v_copy[i] );
        . . .
    }

    // New default variable/object of type ItemType?
    //
    //     ItemType new_item;
    //
    // No can do (outside of scope). Compiler doesn't know what ItemType is.
    ~~~~~

*   **Local Scope**

    The identifier `ItemType` in `template <Typename ItemType>` is _local_: it
    can be used in different scopes, and every instance is independent of each
    other.
    
    ~~~~~ {.cpp}
    template <typename ItemType>
    ItemType return_parameter( ItemType a ){
        return a;
    }

    template <typename ItemType>
    void to_cout( ItemType a ){
        std::cout << a;
    }

    int main(){
        int x = 10;
        double pi = 3.14;
        to_cout( return_parameter(x) ); // Here: ItemType = int
        to_cout(pi);                    // Here: ItemType = double
        . . .
    }
    ~~~~~

*   **The name of a template function**

    Adding `template <typename ... >` to the declaration/definition of a
    function alters its name. In the example discussed here, the _full_ name of
    the `first_less_than_second` function is

    > `first_less_than_second<ItemType>`

    In most cases, we do not have to use it because the compiler figures out
    what `ItemType` is, based on the parameters that are sent to the function.

    ~~~~~ {.cpp}
    int p = 0;
    int q = 1;
    if ( first_less_than_second(p,q) )
    // The compiler sees:
    //     if ( first_less_than_second<int>(p,q) )
    ~~~~~

    We can use the _full_ name to specify which is the function that we want
    (_e.g.,_ in the case the compiler cannot choose the correct `ItemType`
     because of ambiguity).

    ~~~~~ {.cpp}
    int q = 1.0;
    double y = 1.1;
    // if ( first_less_than_second(q,y) )  // Nope. Is ItemType int, or double?

    if ( first_less_than_second<double>(q,y) )
        std::cout << "1.0 is less than 1.1.\n";

    if ( first_less_than_second<int>(q,y) )
        std::cout << "Apparently 1.0 is NOT less than 1.1.\n";
    ~~~~~

    _Can you explain why the output of the previous snippet is_

    ~~~~~
    1.0 is less than 1.1.
    ~~~~~

> [Use this link to confirm the claim above.][link5]

[link5]: http://cpp.sh/6w6av3
 
*   **More than one `typename` declarations**

    We are not restricted to just one ~~_variable-like_ identifier for types~~
    _template parameter_. We can use as many as we need

    ~~~~~ {.cpp}
    template <typename ItemType1, typename ItemType2>
    void print_them( ItemType1 a, ItemType2 b ){
        std::cout << "This function uses TWO template parameters\n";
        std::cout << "1st: " << a << "\t2nd: " << b << '\n';
    }

    template <typename ItemType>
    void print_them( ItemType a, ItemType b ){
        std::cout << "This function uses ONE template parameter\n";
        std::cout << "1st: " << a << "\t2nd: " << b << '\n';
    }
    ~~~~~

    If we pair the template functions above with the statements

    ~~~~~ {.cpp}
    int p = 0;
    int q = 1;
    double y = 1.1;
    print_them(p,q);
    print_them(q,y);
    print_them<int,int>(y,y);
    ~~~~~
    
    the output is

    ~~~~~
    This function uses ONE template parameter
    1st: 0	2nd: 1
    This function uses TWO template parameters
    1st: 1	2nd: 1.1
    This function uses TWO template parameters
    1st: 1	2nd: 1
    ~~~~~

    _Can you explain why?_

> [Use this link to confirm the claim above.][link6]

[link6]: http://cpp.sh/95ur2

*   **Template parameters as regular types**

    Just like regular types, a template parameter can be passed by value, or by
    reference; it can be returned by value, or reference; it can be used to
    create vectors and other containers, etc.

    ~~~~~ {.cpp}
    template <typename ItemType>
    ItemType template_fun( const ItemType& a, std::vector<ItemType>& v ){
        ItemType copy_of_a = a;       // Calls copy ctor.
        for ( int i = 0 ; i < v.size() ; ++i )
            v[i] = copy_of_a;         // Vector populated with copies of a
        return a;                     // Returns by value
    }
    ~~~~~

*   **Template parameters as _ideas_/_concepts_**

    Perhaps the best way to think about template parameters is by describing
    them in terms of the idea/concept they represent. For example, the following
    implementation of the linear search algorithm applies to arrays, vectors,
    and other containers, provided the template parameters are initialized with
    the correct objects/variables.

    ~~~~~ {.cpp}
    template <typename Iterator, typename ItemType>
    Iterator linear_search( Iterator begin, Iterator end, ItemType value ){
        Iterator it = begin;
        while ( it != end ){
            if ( *it == value )
                return it;       // it "points" to where value is located
            ++it;
        }
        return end;              // value is not in the range [begin,end)
    }
    ~~~~~

*   **Template specialization**

    Even if a template function exists, we might not want its code to apply to a
    specific type. If this is the case, we can always overload said function and
    provide a specialization. For a rather specific example, consider the
    following template function that is supposed to _cut in half_ both of the
    explicit parameters:

    ~~~~~ {.cpp}
    template <typename F, typename S> // F = FirstItemType, S = SecondItemType
    void cut_in_half( F& first_param, S& second_param ){
        first_param = first_param / 2;
        second_param = second_param / 2;
    }
    ~~~~~

    This function, or rather set of functions, would work just fine if the
    template parameters `F`, and `S`, are _numeric_. But what if we want to
    _cut_ a `std::string` in half? Since `operator/` is not overloaded for
    `std::string` objects, the above template function doesn't work. To be able
    to perform this task (cut a string in half), we can either provide an
    overload of the division operator[^two], or we can provide a specialized
    version of the template function.

    The code below takes care of most of the needed specialized overloads:

    ~~~~~ {.cpp}
    template <typename T>    // No need for F or S, as there is only one type.
    void cut_in_half( T& first_param, std::string& second_param ){
        first_param = first_param / 2;
        // To `cut a string in half` we pick up the first part.
        second_param = second_param.substr(0, second_param.length()/2 );
    }

    template <typename T>    // No need for F or S, as there is only one type.
    void cut_in_half( std::string& first_param, T& second_param ){
        cut_in_half( second_param, first_param );    // Reuse > repeat
    }
    ~~~~~

    Note that since only one template parameter is needed, there is no need for
    a template parameter declaration of the form `template<typename F, typename
    std::string>`. In this case, it is enough to simply reduce the number of
    template parameters by one.

    To complete this example, we need one more overload: namely, the one that
    takes _two_ `std::string` objects. Here is one possible way to do it:

    ~~~~~ {.cpp}
    void cut_in_half( std::string& first_param, std::string& second_param ){
        int dummy_variable = 0;
        cut_in_half( first_param, dummy_variable );
        cut_in_half( dummy_variable, second_param );
    }
    ~~~~~

    However, the function above **is not a specialized template function**. It
    is rather a _regular_ function that happens to be able to handle the
    _special_ case when two string objects are sent as parameters. In this
    specific scenario, it does the trick. That is, it allows us to be able to
    _cut in half_ pairs of parameters that involve combinations of _numeric_
    and string types.

    If for some rather technical reason, a specialized template function that
    handles string objects is needed. We can modify the code above to include an
    _empty_ template declaration:

    ~~~~~ {.cpp}
    template <>    // Makes it part of the template family of _cut_in_half_.
    void cut_in_half( std::string& first_param, std::string& second_param ){
        int dummy_variable = 0;
        cut_in_half( first_param, dummy_variable );
        cut_in_half( dummy_variable, second_param );
    }
    ~~~~~

    [^two]: Can you figure out the signature of this operator overload as a
    member-function of `std::string`? How about as a non-member?

*   **Precedence of overloads**

    Regarding the `cut_in_half` family of functions. If the first parameter is a
    `std::string` object and the second is a `double`, there are two template
    functions that could potentially be called. Their signatures are:

    i.  `void cut_in_half<F,S>( F&, S& )`, with `F = std::string`, `S = double`.
    ii. `void cut_in_half<S>( std::string&, S& )`, with `S = double`.

    The function with signature _i._ (the one with two template parameters),
    cannot actually be used because there is no overload of `operator/` that
    works for type `F` (`std::string`). So the choice is easy: the second
    function is the one that the compiler will use.

    If on the other hand, `cut_in_half` is called with two `std::string`
    objects. Then the potential choices are:

    iii. `void cut_in_half<F,S>( F&, S& )`, with `F = S = std::string`.
    iv. `void cut_in_half<S>( std::string&, S& )`, with `S = double`.
    v.  `void cut_in_half<S>( S&, std::string& )`, with `S = double`.
    vi. `void cut_in_half( std::string&, std::string& )`.

    In this case, the compiler chooses version _vi._, because non-template
    functions ~~are first class citizens~~ have precedence over template
    functions.

    > **Note:** If _vi._ is not available, the compiler cannot provide valid
    > code. _iii._ fails because of missing `operator/`; whereas _iv._ and _v._
    > lead to ambiguity (same signatures with different/conflicting code).

    Lastly, if for some reason, the non-template function _vi._ above was
    replaced with the template specialization

    vii. `void cut_in_half<>( std::string&, std::string& )`;

    the compiler would choose this one. This is because overloads of a function
    with less template parameters have precedence.

---

[Return to main PIC course website][main]

[main]: ../..

