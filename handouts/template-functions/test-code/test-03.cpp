#include <iostream>

// min for int types
bool first_less_than_second( const int& a, const int& b ){
    if ( a < b )
        return true;
    else 
        return false;
}

// min for double types
bool first_less_than_second( const double& a, const double& b ){
    if ( a < b )
        return true;
    else 
        return false;
}

int main(){
    int p = 0;
    int q = 1;
    if ( first_less_than_second(p,q) )
        std::cout << "0 is less than 1.\n";

    double x = 0.0;
    double y = 1.1;
    if ( first_less_than_second(x,y) )
        std::cout << "0.0 is less than 1.1\n";
        
    x = 1.0;
    y = 1.1;
    if ( first_less_than_second(x,y) )
        std::cout << "1.0 is less than 1.1\n";

    return 0;
}


/**

SAMPLE OUTPUT:

0 is less than 1.
0.0 is less than 1.1
1.0 is less than 1.1

**/
