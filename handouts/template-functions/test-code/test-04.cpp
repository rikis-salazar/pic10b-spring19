#include <iostream>
#include <string>

// Asimplified Fraction public class (struct)
struct Fraction{
    Fraction( int n = 0, int m = 1 ) : numer(n), denom(m) { }

    int numer;
    int denom;
};

bool operator<( const Fraction& lhs, const Fraction& rhs ){
    int p = lhs.numer;
    int q = lhs.denom;
    int r = rhs.numer;
    int s = rhs.denom;
    return p*s < r*q;
}


// An overload that works for any type for whom this code makes sense
template <typename ItemType>
bool first_less_than_second( const ItemType& a, const ItemType& b ){
    if ( a < b )
        return true;
    else 
        return false;
}


int main(){

    int p = 0;
    int q = 1;
    if ( first_less_than_second(p,q) )
        std::cout << "0 is less than 1.\n";

    double x = 0.0;
    double y = 1.1;
    if ( first_less_than_second(x,y) )
        std::cout << "0.0 is less than 1.1\n";

    x = 1.0;
    y = 1.1;
    if ( first_less_than_second(x,y) )
        std::cout << "1.0 is less than 1.1\n";

    std::string r = "Erre";  // <-- 'R' in Spanish
    std::string s = "Ese";   // <-- 'S' in Spanish
    if ( first_less_than_second(r,s) )
        std::cout << "R is less than S\n";


    Fraction half(1,2);
    Fraction one(1);
    if ( first_less_than_second(half,one) )
        std::cout << "half is less than one\n";

    return 0;
}


/**

SAMPLE OUTPUT:

0 is less than 1.
0.0 is less than 1.1
1.0 is less than 1.1
R is less than S
half is less than one

**/
