# Container adapters

> **Note:** Originally the contents of this handout were part of the [doubly
> linked list handout][list-handout], you might want to read/review it before
> reading the contents of this document.

[list-handout]: ../template-list/


## The goal

To code _working_ template container adapters `Pic10b::stack` and
`Pic10b::queue`.


# What are container adapters?

Roughly speaking, _adapters_ are data types that adapt a container to provide a
specific interface. For example, instead of coding a `Stack` from scratch, we
can simply adapt our `DoublyLinkedList` class to _act like_ a stack[^do-not].

[^do-not]: _Warning:_ This approach (adapting) is not allowed in your _Hanoi_
  assignment.


### The `Pic10b::stack` class

To differentiate this container adapter from the one you are to code for your
assignment, we will enclose it in our old namespace friend: `Pic10b`.

~~~~~ {.cpp}
namespace Pic10b {

    template <typename T>
    class stack {
      private:
        DoublyLinkedList<T> the_list;

      public:
        stack() /* : the_list() */ { }   // <-- Not a typo! Actual constructor.

        // The big 3 are not needed. Why not?

        bool is_empty() const {
            return the_list.is_empty();  // See (*) below
        }

        void push( const T& v ) {
            the_list.push_back(v);       // back of list = top of stack
        }

        void pop() {
            the_list.pop_back();
        }

        T top() const {
            return the_list.last().get_value();
            //              ^^^^^^  See (**) below
        }
    };
    // (*) The function `DoublyLinkedList<T>::is_empty()` is not discussed in
    //     the doubly linked list handout but it is fairly easy to code. I would
    //     suggest you try writing your own version, then compare it to the code
    //     in the `dl_list.h` file that accompanies the list handout.
    //(**) This function is also not discussed in the handout, but the code can
    //     be found in the working implementation of the doubly linked list at
    //     the end of the handout.
}
~~~~~

As you can see, adapters take advantage of functions that are already available
for the underlying container. Moreover, in some cases, the programmer is the one
who decides which underlying container is to be used. For example, the
`std::queue` class available in the `<queue>` library, has the following
_forward declaration_:

~~~~~ {.cpp}
namespace std {
    template< class T, class Container = std::deque<T> >
    class queue;
}
~~~~~

Which roughly states that a programmer can specify the underlying `Container`
class, however, if s/he fails to do so, a `std::deque<T>`[^deque] is to be
used.[^def-templ]

[^deque]: The [irregular] acronym _deque_ stands for **d**ouble **e**nded
  **que**ue. It is a data structure that provides fast look ups, as well as fast
  insertion/deletion at both of its ends (front and back).
[^def-templ]: This is known as a _default template value parameter_. It is the
  template version of a _default value parameter_.


### The `Pic10b::queue` class

Following the ideas from the previous subsection, let us code our own `queue`
class. This time however, we'll follow a more _standard_ approach.

*   We'll let the programmer choose the container, but the following
    restrictions apply:
    -   It must be a template class.
    -   It must provide the functions: `back()`, `front()`, `push_back()`,
        `push_front()`, `empty()`, and `size()` with standard semantics:
        *   `front()` returns the value at the front end of the container.
        *   `back()` returns the value at the back end.
        *   `empty()` returns `true` if the container is empty, and `false`
            otherwise .
        *   `size()` returns the number of values stored in the container.

Very well then, here we go:

~~~~~ {.cpp}
namespace Pic10b {

    template < class T, class Container = std::deque<T> >
    class queue {
      private:
        Container c;

      public:
        // queue() : c() { }  // We can let the compiler handle this constructor

        bool empty() const { return c.empty(); }
        size_t size() const { return c.size(); }
        void push( T v ) { c.push_back(v); }
        void pop() { c.pop_front(); }
        T front() const { return c.front(); }
        T back() const { return c.back(); }
    };

}
~~~~~

That was easy, wasn't it? `:-)`

---

[Return to main PIC course website][main]

[main]: ../..

