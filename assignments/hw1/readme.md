# A grade calculator

| [ ![pics/xibalba.png](pics/xibalba.png) ][xibalba] |  
|:-----:|  
| Figure 1: _Xibalba_, the ruler of _the land of the forgotten_.<br>He is a good student, but if left unchecked he might be tempted to cheat. |  

[xibalba]: https://en.wikipedia.org/wiki/Xibalba


## The backstory

_Odracir Razalas_ teaches a programming language called `SULP-SULP-EES` at
_IELEISULL_ (a 4-year university located in a place known as _Ainrofilac_,
_ASU_). He hopes that by disclosing some grading guidelines in his syllabus,
students know precisely how many points they need to score in their exams,
assignments, etc., to earn the grade they want. However, for some reason at the
end of every quarter some students seem to have trouble computing their final
scores.

In addition to the grading breakdown in figure 2, _Odracir_ wrote the following
regarding homework assignments:

> There will be 7 or 8 homework assignments throughout the quarter. No late
> homework will be accepted for any reason but your lowest homework score will
> be  dropped.

| ![grade-breakdown.png](pics/grade-breakdown.png) |  
|:-----:|  
| Figure 2: `SULP-SULP-EES` course grading breakdown. |  

Your job is to write a library file that helps students compute their final
score once the quarter is over.


## The assignment

You are to implement a library file (`.h`) containing the function
`compute_overall_score(...)`. This function should receive two string variables
as parameters. The first parameter corresponds to the name of a text file that
contains the grades earned by a student throughout the quarter. On the other
hand, the second parameter contains the name of a log file where information
will be recorded.

This is [in principle] a very simple assignment. Since the idea behind it is to
make sure you review some (or most) of the concepts from Pic 10a, you need to
make sure your project follows these rules:

1.  Your project should use a **three-file layout**.
    i.  Simple driver ([`hw1-driver.cpp`][driver]).
    i.  User-defined library ([`hw1.h`][h-file]).
    i.  Implementation of the functions defined in the library
        ([`hw1.cpp`][cpp-file]).
1.  You need to use **at least one class**. This is entirely up to you. It could
    be a very simple one with no member functions, or you could use a complex
    setup with several different classes.
1.  You need to pass **at least one parameter by reference** to a function.
1.  You need to use **at least one pointer** (do not worry if you did not cover
    this topic in 10A, I will briefly mention during class the easiest way to
    fulfill this requirement).
1.  You need to use **at least one vector or array** (the choice is up to you).
1.  When your project is run, the overall score should be displayed to the
    console, as well as being sent to the log file specified in the driver. Said
    score should be reported as a **percentage with two decimals** (_e.g.,_
    74.47%). You may also report the scores corresponding to the two different
    grading breakdowns.

[driver]: code/hw1-driver.cpp
[h-file]: code/hw1.h
[cpp-file]: code/hw1.cpp

For this project you may assume that the user will supply a valid file
containing the scores of a student, and that this file will be correctly
formatted:

*   The first line will contain _eight_ integers between 0 and 20, separated by
    a _blank space_; these numbers represent homework scores.
*   The second line will contain _two_ integers between 0 and 100 separated by a
    _tab character_ [`\t`]; they represent midterm scores.
*   Lastly, the third line will contain _one_ integer between 0 and 100,
    representing the final exam score.

For testing purposes you may use the file [`xibalba.txt`][xib-file]; or you can
create your own (_e.g.,_ [`file1.txt`][test-file]), as long as it adheres to the
guidelines given above. 

When my implementation is run with the data inside [`xibalba.txt`][xib-file],
the output it produces looks more or less like this:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Score A: 89.0786
Score B: 88.9986
Overall score: 89.08%
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

[xib-file]: input_files/xibalba.txt
[test-file]: input_files/file1.txt


## What is this assignment about?

Review. Plain and simple review. Some of you took Pic 10a recently and are very
familiar with these topics, whereas some of  you took it a while back and might
need to go over your notes. In any case it is always a good idea to make sure we
are aware of these basic concepts.


## Technical note

Unfortunately, different operating systems (_e.g.,_ windows, Mac OS) treat text
files in slightly different ways. If for some reason the test files provided
here appear to consist of just one line with 11 integers, copy/paste the
information into a similarly named file created with your own computer.


## Submission

Make sure your files are named `hw1.cpp` and `hw1.h` (all lowercase). If your
files are not correctly named, your homework might not be graded. Your code
should **contain useful comments** as well as **your name**, **the date**, and a
**brief description of what the library files do**. Upload your files to the
[assignments section of the CCLE website][ccle-hw]. These files will be
automatically collected at the date and time listed in _"submission status"_
table at the bottom of the CCLE assignment description corresponding to this
project.

> Notice that you **do not need to upload the driver** (`hw1-driver.cpp`), as we
> will be testing your library files against a possibly more complex driver.

[ccle-hw]: https://ccle.ucla.edu/course/view.php?id=73519&section=2


## Grading rubric

| **Category** | **Description** | **Pts** |  
|:-------------|:---------------------------------------------------|:-------:|  
| Correctness | No errors. The project compiles and the setup (3 files) is as described in the assignment. | 3 |  
| Classes | At least one class is used in a way that is consistent with the ideas behind _object oriented programming_. | 3 |  
| File I/O | The program reads from the supplied input files and writes the final score to the correct output file. | 2 |  
| Containers   | At least one vector/arrays is used. | 2 |  
| Parameters | At least on reference parameter is used; `compute_overall_score(...)` employs the correct number and type of parameters. | 2 |  
| Pointers | At least one pointer [or iterator] is used. | 2 |  
| Computations | The reported final score is accurate and it is correctly formatted. | 4 |  
| Coding style | The code is readable, efficient, and easy to follow. | 2 |  
| | | |  
| Total | | 20 |  


## List of files

The following files are included with this assignment:

*   Current directory:
    -   [`index.html`][assignment]: this file.
    -   [`readme.md`][assignment-md]: this file's `markdown` source code.
    -   [`Makefile`][makefile]: the _recipe_ that converts `readme.md` into
        `index.html`.
*   `code` directory:
    -   [`hw1-driver.cpp`][driver]: use it to test your library but **do not
        submit it** to CCLE. I will use a slightly different tester.
    -   [`hw1.h`][h-file]: place the declaration of your classes and/or
        functions here. Submit it to CCLE once your project is complete.
    -   [`hw1.cpp`][cpp-file]: place the definition (implementation) of your
        classes and/or functions here. Submit it to CCLE once your project is
        complete.
*   `input_files` directory:
    -   [`xibalba.txt`][xib-file]: _Xibalba's_ scores.
    -   [`file1.txt`][test-file]: another sample test file.
*   `pics` directory:
    -   [`xibalba.png`][xibalba-img]: _Xibalba_ in his throne located in the
        _land of the forgotten_.
    -   [`grade-breakdown.png`][breakdown]: grading breakdown for _Odracir's_
        course.

[assignment]: index.html
[assignment-md]: readme.md
[makefile]: Makefile
[xibalba-img]: pics/xibalba.png
[breakdown]: pics/grade-breakdown.png

---

[Return to main course website][PIC]

[PIC]: ../..

