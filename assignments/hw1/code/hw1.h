// W A R N I N G ! ! !
// You should ALWAYS comment your code. Points will be deducted if you do not
// remove this warning.

#ifndef HW1_H
#define HW1_H

#include <string>
// Other libraries you need should be placed here:



/** 
    Forward declaration of auxiliary functions. At the very least, you need to
    implement a function with the signature:
	void compute_overall_score(std::string, std::string);
*/



// Other functions you need should be placed here:




/** 
	The declaration of user defined classes should be placed here.
	E.G.

	class MyClass{
		public:
			// Constructor(s)
			// Other member functions

		private:
			// Member fields
	};
*/

#endif
