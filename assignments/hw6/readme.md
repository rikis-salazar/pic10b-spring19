# The _Hanoi_ puzzle

| [ ![pics/mariachi-stack.png](pics/mariachi-stack.png) ][que_se_cae] |  
|:-----:|  
| Figure 1: _Manolo Sánchez_ about to be _pushed into_ a stack of _'mariachis'_. |  

[que_se_cae]: media/cayendo.gif


## The backstory

Everybody in _San Ángel_ thinks _Manolo_ passed away because he was bitten by
_Xibalba's_ two-headed snake. However, it turns out he actually sustained a life
threatening injury when he was serenading _María_ while standing at the top of a
stack of _'mariachi'_ brothers. At that time he didn't think his fall was such a
big deal and didn't bother checking in with a doctor. This was his second major
mistake! The first one, you may ask? Well, that's an easy one: during his time
at _IELEISULL_ university, while he was taking a `SULP-SULP-EES` class, he
simply didn't care about taking notes on stacks or queues. Had he payed
attention, he would have known how to stack things 'the right way'.

Poor _Manolo_! Now he finds himself fighting bulls and singing apology songs
while _Chakal_ (the leader of the bandits) is on its way to destroy _San Ángel_.


## The assignment

Since I wouldn't want you to make the same mistakes _Manolo_ made, I need to
make sure you know how to work with stacks. Your assignment is fairly
straightforward: implement a `Stack` class and use it to solve the _Hanoi
puzzle_.

From the wikipedia entry corresponding to [Tower of Hanoi][wiki-hanoi]:

> "_The objective of the puzzle is to move the entire stack to another rod,
> obeying the following simple rules:_
>
> 1.  _Only one disk can be moved at a time._
> 1.  _Each move consists of taking the upper disk from one of the stacks and
>     placing it on top of another stack (i.e., a disk can only be moved if
>     it is the uppermost disk on a stack)._
> 1.  _No disk may be placed on top of a smaller disk._"

[wiki-hanoi]: http://en.wikipedia.org/wiki/Tower_of_Hanoi

If unlike _Manolo_, you were paying attention to our discussion of recursion,
you probably remember that a solution to this problem can be found somewhere in
the [Old code [deprecated]][CCLE-code] section of CCLE.

[CCLE-code]: https://ccle.ucla.edu/course/view/19S-COMPTNG10B-2?section=6

So, if the solution to the puzzle is provided, then this assignment should be a
piece of cake, right? Well, you still have to implement a stack! And by '_you
have to_' I mean that **you really have to do it**. A 'wrapper' class, like the
one we discussed for queues, will simply not cut it for this assignment.

Oh, I almost forgot! Your program should be able to display a detailed list of
the movements needed to solve the puzzle for a given number of disks. As with
the previous assignment, a driver program could request your program to display
this list to the console, or to a file.

To give you an idea of how your program should behave, in the screenshot in
figure 2 you will find the output that my implementation sends to the console
when [`hanoi-driver.cpp`][the-driver] is the program in charge.

| ![pics/hanoi-screenshot.png](pics/hanoi-screenshot.png) |  
|:-----:|  
| Figure 2: The output my implementation produces when compiled against [`hanoi-driver.cpp`][the-driver] . |  

[the-driver]: drivers/hanoi-driver.cpp

As usual, you are free to code as many member functions, and to use as many
fields as you deem necessary. However, at the very minimum your project should:

*   implement a template class `Stack`. You can start your work form scratch, or
    you can base it on [code presented during lectures][BB-DLL][^click] and/or
    available at CCLE.  The _home-made_ `DoublyLinkedList` class we studied
    during lecture might come in handy for this task.

*   implement _the big 3_ (_copy constructor_, _assignment operator_ and
    _destructor_) for the `Stack` class described above. Please note that we did
    not discuss this functions for a _wrapper_ `Stack` class, but we did so for
    a linked list. Once again, our _home-made_ `DoublyLinkedList` might prove
    useseful.

*   implement a non-default `Stack` constructor that receives a `std::string`
    object as parameter. This value corresponds to the name we want a stack to
    display when reporting the list of movements needed to solve the puzzle. To
    complement this constructor, you should also provide a member function
    `get_name()`, that returns the value that was passed to this constructor
    during the creation of the object.

    Some examples of valid calls to this constructor are:

    ~~~~~ {.cpp}  
    Stack<bool> aStackNamed("Desire");  
    Stack<int> leftPeg("poste izquierdo");  
    ~~~~~  

    Similarly, the statements below should work as expected.

    ~~~~~ {.cpp}  
    cout << "'aStackNamed' has name: " << aStackNamed.get_name() << ".\n";  
    cout << "Mueve el disco 1 al " << leftPeg.get_name() << ".";  
    ~~~~~  

*   implement a specialized non-default constructor that is only expected to
    create objects of type `Stack<int>`. In addition to the `std::string`
    parameter decribed above, this constructor should expect a positive integer
    as a second parameter. The resulting stack should be ready to be used in the
    _Hanoi_ puzzle. For example, if the positive number $n$ is passed to the
    constructor, it should create a stack containing $n$ nodes. The `int` value
    $n$ should be at the bottom of the stack, whereas the `int` value 1 should
    be at the top.

*   overload the _shift-left operator_ `operator<<`. This overload should be
    able to handle objects of type `Stack<int>`, and it should display the
    contents of the stack from the bottom (biggest disk) to the top (smallest
    disk).

*   implement a non-member function `hanoi(...)` that receives 5 parameters. The
    first three parameters should be of type `Stack<int>`, the fourth should be
    an `int`, and the fifth should be of type `std::ostream`. This function
    should provide a solution to the _Hanoi_ puzzle, making sure a detailed list
    of moves is sent to the ostream object passed on to it.

    Here are more details about the parameters:

    -   Parameter 1 represents the _source_ peg; that is, the one that holds the
        initial stack of disks.
    -   Parameter 2 represents the _auxiliary_ peg. It can be used to aid with
        the transfer of disks, but it should be empty at the end.
    -   Parameter 3 represents the _target_ peg; that is, the one that will end
        up with the stack disks.
    -   Parameter 4 represents the number of disks that will be transferred from
        the _source_ peg, to the _target_ peg.
    -   Lastly, the `ostream` object received by the function is to be used to
        "display the solution" of the puzzle. As with previous assignments, a
        driver could pass `std::cout`, or a file stream.

[^click]: See also the [companion handout at the PIC class website][handout].

[BB-DLL]: ../../handouts/template-list/
[handout]: ../../handouts/template-list/


## What is this assignment about?

Implementation of data structures. Displaying of information inside a recursive
function, and specialization of member functions in a template class.


## Is the _solution_ correct?

Follow the detailed list of moves that your program produces, you can then
verify whether or not you have a sequence of valid moves, and hence a solution
to the _Hanoi_ puzzle. Once you are satisfied with a list of moves, you should
experiment with different number of disks as well as with the order in which you
pass the stacks to the `hanoi(...)` function.

Do I have to display the pegs in order like in the screenshot above? The answer
is no[^fine-print]. You will get full correctness credit for the assignment if
your program:

i.  works as expected with the provided driver; and
i.  works as expected with a driver designed to test the template part of your
    project.

On the other hand, a "sorted" output might help compensate for penalties your
project might incur in other areas (see rubric below).

[^fine-print]: Be aware that you actually **do need** to display the list of
  movements needed to solve the puzzle, but your program does not have to
  display the pegs in order. In figure 2 they are always displayed following the
  pattern: _top_, _middle_, _bottom_.


## Submission

Make sure your file is named `hanoi.h` (all lowercase). If your file is not
correctly named, your homework might not be graded. Your code should **contain
useful comments** as well as **your name**, **the date**, and a **brief
description of what the library file does**. Upload your file to the
[assignments section of the CCLE website][ccle-hw]. This file will be
automatically collected at the date and time listed in _"submission status"_
table at the bottom of the CCLE assignment description corresponding to this
project.

> Notice that you **do not need to upload any driver**, as we will be testing
> your library files against possibly different drivers.

[ccle-hw]: https://ccle.ucla.edu/course/view.php?id=73519&section=2


## Grading rubric

| **Category** | **Description** | **Points** |  
|:-------------|:---------------------------------------------------|:-------:|  
| Correctness | The project compiles against a driver[^not-here] that tests the _template_ part of your project. | 4 |  
| Correctness | The project compiles against [`hanoi-driver`][the-driver]. | 4 |  
| Output | A detailed list of moves is produced and the list solves the Hanoi puzzle. | up to 6 |  
| Efficiency | No major overhead costs are associated to `hanoi(...)`. | 4 |  
| Coding style | Are you still not writing _self-commenting_[^seasoned] code? | 4 |  
| | | |  
| Total | | 20 (max) |  

[^not-here]: This driver is not provided here.
[^seasoned]: Remember that _experienced_ programmers almost never comment their
  code, instead they let it speak for itself.

## List of files

The following files are included with this assignment:

*   Current directory:
    -   [`index.html`][assignment]: this file.
    -   [`readme.md`][assignment-md]: this file's `markdown` source code.
    -   [`Makefile`][makefile]: the _recipe_ that converts `readme.md` into
        `index.html`.
*   `drivers` directory:
    -   [`hanoi-driver.cpp`][the-driver]: feel free experiment with the number
        of disks, as well as to change the order `Stack` objects are passed to
        `hanoi(...)`. 
*   `media` directory:
    -   [`cayendo.gif`][humpty-dumpty]: ~~Humpty Dumpty~~ _Manolo_ ~~sat~~ stood
        on a ~~wall~~ pile of 'mariachi' brothers. ~~Humpty Dumpty~~ _Manolo_
        had a big fall.
*   `pics` directory:
    -   [`mariachi-stack.png`][pila]: &#127900; I know I belong &#127901;
        &#127900; when I sing this song &#127901;
    -   [`hanoi-screenshot.png`][captura]: apparently this assignment used to be
        number 5, back in 2015.

[assignment]: index.html
[assignment-md]: readme.md
[makefile]: Makefile
[humpty-dumpty]: media/cayendo.gif
[pila]: pics/mariachi-stack.png
[captura]: pics/hanoi-screenshot.png

---

[Return to main course website][PIC]

[PIC]: ../..

