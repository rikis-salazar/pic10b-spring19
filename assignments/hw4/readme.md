# Our very own `Pic10b::vector` class

| |  
|:-----:|  
| [ ![pics/manolo-vivo.png](pics/manolo-vivo.png) ][jorge-gutierrez] |  
| ![pics/manolo-calaca.png](pics/manolo-calaca.png) |  
| Figure 1: _Manolo Sánchez_ about to enter _La Plaza de San Ángel_ (top). _Manolo 'calaca' Sánchez_ arriving in _The Land of the Remembered_ (bottom). |  

[jorge-gutierrez]: http://www.latimes.com/entertainment/movies/la-et-mn-book-of-life-director-jorge-gutierrez-20141019-story.html#page=1


## The backstory

_María Posada_ is 100% sure _Manolo_ will find a way to come back to _San
Ángel_. However, she has no clue of which _Manolo_ will eventually show up. Will
it be the original _Manolo_, or will it be the _'calaca'_ version? She would
like to know in advance so that she can make the appropriate preparations for
his comeback party. She could take the easy way out, and buy two versions of
every item needed for the party. For example, she could buy two cakes: one black
and white in case _Manolo 'calaca'_ returns; and one with multiple colors in
case the original _Manolo_ shows up. 

Oh, what a dilemma! If she only knew about templates, her life would be much
easier...


## The assignment

You are to template the `Pic10b::vector` class that we have been studying during
lecture. You can either

*   start your code from scratch, or
*   adapt [this _non-templated_ implementation][base-vector], or
*   base your work on the `MyVector` class available at the ["Old code
    [deprecated]"][CCLE-link] section of this course CCLE website. 

[base-vector]: src/pic10b_vector_non_template.cpp
[CCLE-link]: https://ccle.ucla.edu/course/view/19S-COMPTNG10B-2?section=6

I suggest you start by making sure your implementation works for `int`, as well
as `double` types. The drivers below should help you test your functions. 

*   [`01-driver_int.cpp`][int-driver]: a driver for the class
    `Pic10b::vector<int>`.
*   [`02-driver_double.cpp`][double-driver]: a driver for the class
    `Pic10b::vector<double>`.

[int-driver]: drivers/01-driver-int.cpp
[double-driver]: drivers/02-driver-double.cpp

When implemented correctly, the drivers combined with your implementation should
output something similar to the contents of the following files:

*   [`output_int.txt`][int-output]: console output produced by
    [`01_driver_int.cpp`][int-driver].
*   [`output_double.txt`][double-output]: console output produced by
    [`02_driver_double.cpp`][double-driver].

[int-output]: samples/01-output-int.txt
[double-output]: samples/02-output-double.txt

Notice that your program **is expected to display messages to the console every
time one of _the big 4_ functions is called**. The message should:

a)  start with 3 or more lower case _'ex'_ characters without spaces between
    them; and
a)  indicate which one of _the big 4_ functions is being called.

For example, if the destructor is called, valid messages include:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
xxx: Destructor...
xxxxxxxx: This is the destructor :xxxxxxxx
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Failure to follow this guidelines might result in points being deducted from
your final score. 

Once your program works as expected with numeric types, you should then
specialize some of the functions so that they can handle strings. Assuming we
have the following setup

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
using std::string;
. . .

Pic10b::vector<string> v;
v.push_back("entry_1");
v.push_back("entry_2");
v.push_back("entry_3");
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

the functions that need to be specialized, as well as details about their
implementations are listed below:

i.  `operator<<` acting on objects of type `std::ostream` and
    `Pic10b::vector<string>`. 

    Unlike the overload that handles _numeric_ data types, this function should
    replace the delimiting curly braces (`{`,`}`) with square brackets
    (`[`,`]`). In addition, blank spaces should be inserted to make the contents
    of the vector more readable.

    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
    cout << v ;
    // displays: [ entry_1, entry_2, entry_3 ]
    //
    // note(s):
    //     - there is a blank space after '[', and one before ']', and
    //     - there is no trailing comma after entry_3.
    //
    //     For reference, numeric types are displayed {1, 4, 6, 3}.
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

i.  `operator*` acting on two objects of type `string` and
    `Pic10b::vector<string>`.

    This operator should return a `Pic10b::vector<string>` object whose _k_-th
    entry is the concatenation (_i.e.,_ string addition) of the string parameter
    and the _k_-th entry of the `Pic10b::vector<string>` parameter. This
    concatenation should add padding (blank spaces), and it should preserve the
    order in which the parameters were passed.

    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
    string a = "This is:";
    string b = "this is!";
    cout << a * v;
    // displays: [ This is: entry_1, This is: entry_2, This is: entry_3 ]
    cout << v * b;
    // displays: [ entry_1 this is!, entry_2 this is!, entry_3 this is! ]
    //
    // note(s):
    //     - there is a blank space between 'a' and 'v[k]', and
    //     - there is a blank space between 'v[k]' and 'b'.
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Your are free to decide whether to implement the operators above as member or
non-member functions. The driver below should help you test your specialized
templates.

*   [`03-driver_string.cpp`][string-driver]. A driver for the class
    `Pic10b::vector<std::string>`.

[string-driver]: drivers/03-driver-string.cpp

When implemented correctly, your program should then output something similar to
the contents of [`output_string.txt`][string-output].

[string-output]: samples/03-output-string.txt


## What is this assignment about?

Template functions, template classes, and specialized templates.


## Technical note

When working with templates the usual three-file-layout (`library.h` +
`library.cpp` + `driver.cpp`) produces a link error.  You should instead use a
two-file-layout (`library.h` + `driver.cpp`), where `library.h` contains the
class interface, as well as the details of the class implementation.


## Submission

Make sure your file is named `pic10b_vector.h` (all lowercase). If your file is not
correctly named, your homework might not be graded. Your code should **contain
useful comments** as well as **your name**, **the date**, and a **brief
description of what the library file does**. Upload your file to the
[assignments section of the CCLE website][ccle-hw]. This file will be
automatically collected at the date and time listed in _"submission status"_
table at the bottom of the CCLE assignment description corresponding to this
project.

> Notice that you **do not need to upload any driver**, as we will be testing
> your library files against possibly different drivers.

[ccle-hw]: https://ccle.ucla.edu/course/view.php?id=73519&section=2


## Grading rubric

| **Category** | **Description** | **Points** |  
|:-------------|:---------------------------------------------------|:-------:|  
| Correctness | No errors when tested against numeric types. | 4 |  
| `vector<int>` | The output resembles the provided file. | 5 |  
| `vector<double>` | The output resembles the provided file. | 5 |  
| `vector<std::string>` | The output resembles the provided file. | 5 |  
| Coding style | The code is efficient and easy to follow. | 4 |  
| | | |  
| Total | | 20 (max) |  

> **Note:**
>
> Even though the total points adds up to 22, the maximum score you could
> receive for this assignment is 20. I expect the specialization of the template
> functions to handle strings to be a little more complicated than the
> corresponding functions that handle numeric types. In principle, even if your
> assignment does not handle strings very well you could still get full credit
> for this assignment.


## List of files

The following files are included with this assignment:

*   Current directory:
    -   [`index.html`][assignment]: this file.
    -   [`readme.md`][assignment-md]: this file's `markdown` source code.
    -   [`Makefile`][makefile]: the _recipe_ that converts `readme.md` into
        `index.html`.
*   `src` directory:
    -   [`pic10b_vector_non_template.cpp`][base-vector]: our very own
        (non-template) vector [of `int`s].
*   `drivers` directory:
    -   [`01-driver-int.cpp`][int-driver]: your submission will be tested
        against this very same file.
    -   [`02-driver-double.cpp`][double-driver]: your submission will also be
        tested against this file.
    -   [`03-driver-string.cpp`][string-driver]: your submission... blah, blah,
        blah.
*   `samples` directory:
    -   [`output-int.txt`][int-output]
    -   [`output-double.txt`][double-output]
    -   [`output-string.txt`][string-output]

    Compare the output of your project to the contents of these files. It is OK
    if the number of debuggin messages (_e.g.,_ "`xxx: Constructor`") do not
    match.

*   `pics` directory:
    -   [`Manolo-vivo.png`][manolo-sanchez]: _Manolo Sánchez_, son of _Carlos
        Sánchez[^two]_.
    -   [`Manolo-calaca.png`][manolo-calaca-sanchez] _Manolo 'calaca' Sánchez_.
        He is the result of a [double?] bite from a two-headed snake.

[^two]: Who in turn is son of _Luis "el super macho" Sánchez_. I believe he also
  has two brothers named _Jorge_ and _Carmelo_, but do not quote me on this.


[assignment]: index.html
[assignment-md]: readme.md
[makefile]: Makefile

[manolo-sanchez]: pics/manolo-vivo.png
[manolo-calaca-sanchez]: pics/manolo-calaca.png


---

[Return to main course website][PIC]

[PIC]: ../..





