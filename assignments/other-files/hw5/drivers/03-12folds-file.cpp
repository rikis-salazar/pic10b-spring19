#include "dragon.h"     // generate_dragon_sequence(...)
#include <fstream>      // std::ofstream
#include <vector>       // std::vector<T>

using std::vector;
using std::ofstream;

int main(){
    vector<bool> dragon;

    // Open file
    ofstream fout;
    fout.open("dragon.txt");

    // Sequence (12 folds) sent to a file
    generate_dragon_sequence( dragon, 12, fout );

    // close file
    fout.close();

    return 0;
}
