#include "dragon.h"     // generate_dragon_sequence(...)
#include <iostream>     // std::cout
#include <vector>       // std::vector<T>

using std::vector;
using std::cout;

int main(){
    vector<bool> dragon;

    // Sequence (4 folds) sent to the console
    generate_dragon_sequence( dragon, 4, cout );

    return 0;
}
