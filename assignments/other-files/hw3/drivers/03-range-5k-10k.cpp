#include <iostream>   // std::cout
#include <ctime>      // time()
#include <cstdlib>    // RAND_MAX, rand(), srand()
#include <fstream>    // std::ofstream
#include <iomanip>    // std::fixed, std::setprecision
#include "hw3.h"      // average_complexity(...)


int const NUM_TRIALS = 100;
int const MIN_N = 5000;
int const MAX_N = 10000;


int main(){

    std::ofstream fout("data.txt");
    fout << std::setprecision(2) << std::fixed ;

    for ( int k = 0 ; k < NUM_TRIALS ; k++ ){

        int n = MIN_N + rand() % (MAX_N - MIN_N + 1);
        double T_n = average_complexity(n);

        fout << n << '\t' << T_n << '\n';
    }

    fout.close();

    return 0;
}
