#include "hw1.h"
#include <string>

int main(){

	compute_overall_score("xibalba.txt", "xibalba_overall.txt");
	compute_overall_score("test00.txt", "00_overall.txt");
	compute_overall_score("test01.txt", "01_overall.txt");
	compute_overall_score("test02.txt", "02_overall.txt");
	compute_overall_score("test03.txt", "03_overall.txt");
	compute_overall_score("test04.txt", "04_overall.txt");
	compute_overall_score("test05.txt", "05_overall.txt");
	compute_overall_score("test06.txt", "06_overall.txt");

	return 0;
}
