#include <iostream>
#include "time_class.h"

using std::cout;

int main(){
   /**
      Time setup [not needed in San Angel]
      Time::set_hr_in_day(24);
      Time::set_min_in_hr(60);
    */

   cout << "*****************************************\n";
   cout << "Additional testing!!!\n";
   cout << "*****************************************\n";

   Time a(61);
   const Time b(5);

   cout << "\tTime a(61);\t\t";
   cout << "a = " << a << "\n";

   cout << "\nTesting `const` correctness:\n";
   cout << "\tTime b(" << b.minutes() << ");\t";
   cout << "PASSED\t";
   cout << "b = " << b << "\n\n";


   cout << "\nTesting boolean expressions:\n\t";
   cout << " a != 5\t\t";
   if ( a != 5 ) cout << "PASSED\n\t";
   else cout << "FAILED!\n";

   cout << " b == 5\t\t";
   if ( b == 5 ) cout << "PASSED\n\t";
   else cout << "FAILED!\n";

   cout << " 5 != a\t\t";
   if ( 5 != a ) cout << "PASSED\n\t";
   else cout << "FAILED!\n";

   cout << " 5 == b\t\t";
   if ( 5 == b ) cout << "PASSED\n";
   else cout << "FAILED!\n";

   cout << '\n';
   return 0;
}
