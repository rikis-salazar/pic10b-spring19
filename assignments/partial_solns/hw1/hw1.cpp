// This file is part of the _partial_ solution to the _review_ class assignment.

#include "hw1.h"
#include <fstream>
#include <iostream>
#include <iomanip>


void compute_overall_score(std::string inputFile, std::string outputFile){
    GradeCalculator gc;
    gc.read_scores_from_file(inputFile);
    gc.compute_overall_scores();
    gc.report_grade();    // <-- Defaults to `std::cout`

    std::ofstream fout;
    fout.open(outputFile);
    gc.report_grade(fout);
    fout.close();

    return;
}


GradeCalculator::GradeCalculator()
  : the_scores(ASSIGNMENTS), mid1_score(0), mid2_score(0), final_score(0),
    schema_A(0.0), schema_B(0.0) {
    // dump_fields();
}


// . . .
