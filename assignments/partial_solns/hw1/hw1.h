// This file is part of the _partial_ solution to the _review_ class assignment.

#ifndef HW1_H
#define HW1_H

#include <string>
#include <vector>
#include <iostream>


const int ASSIGNMENTS = 8;
const int PRECISION = 2;

const double HW_WEIGHT = 25.0;
const double MID_WEIGHT_A = 20.0;
const double MID_WEIGHT_B = 30.0;
const double FIN_WEIGHT_A = 35.0;
const double FIN_WEIGHT_B = 44.0;

const double HW_MAX = 20.0;
const double MID1_MAX = 100.0;
const double MID2_MAX = 100.0;
const double FINAL_MAX = 100.0;


void compute_overall_score(std::string, std::string);


class GradeCalculator{
  public:
    GradeCalculator();
    void read_scores_from_file(std::string the_file);
    void compute_overall_scores();
    void report_grade(std::ostream& = std::cout) const;

  private:
    std::vector<int> the_scores;
    int mid1_score;
    int mid2_score;
    int final_score;
    double schema_A;
    double schema_B;

    // For debugging purposes
    void dump_fields() const;
};

#endif
