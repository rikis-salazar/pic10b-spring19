// This file is part of the _partial_ solution to the _dragon curve_ assignment.

#ifndef DRAGON_H
#define DRAGON_H

#include <fstream>
#include <vector>

using std::vector;
using std::ostream;
using std::endl;

ostream& operator<<( ostream& out , const vector<bool>& b){
    // . . .
    return out;
}

void generate_dragon_sequence( vector<bool>& v , int n , ostream& out ){
    if ( n == 0 ) 
        out << v;
    else {
        // . . .
	
	generate_dragon_sequence(v,n-1,out);
    }
}

#endif
