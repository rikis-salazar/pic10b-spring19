// This file is part of the _partial_ solution to the `Time` class assignment.

#ifndef __TIME_CLASS_H__
#define __TIME_CLASS_H__

#include <iostream>

class Time{
  public:
    Time();
    Time( int );
    Time( double );
    Time( int , int );  

    int minutes() const;
    void set_minutes( double );

    int hours() const;
    void set_hours( int );

    static void set_min_in_hr( int );
    static void set_hr_in_day( int );

    Time& operator+=( const Time& );

    // Friendship is really not needed. Why?
    friend bool operator<( const Time& , const Time& );
    friend bool operator<=( const Time& , const Time& );
    friend bool operator>( const Time& , const Time& );
    friend bool operator>=( const Time& , const Time& );
    friend bool operator==( const Time& , const Time& );
    friend bool operator!=( const Time& , const Time& );


  private:
    void normalize();
    int compare( const Time& ) const; 
      
    static int MIN_IN_HR; 
    static int HR_IN_DAY;

    int min;
};

std::ostream& operator<<( std::ostream& , const Time& );
Time operator+( Time , const Time& );  // Note: 1st param passed by value.

#endif
