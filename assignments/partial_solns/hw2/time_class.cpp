// This file is part of the _partial_ solution to the `Time` class assignment.

#include "time_class.h"

// Static member fields (initialization)
int Time::MIN_IN_HR = 60; 
int Time::HR_IN_DAY = 24;


// CONSTRUCTOR(S)  
Time::Time() : min(0) { }

Time::Time( int m ) : min(m) {
    normalize();
}

// . . .



// STATIC member functions
void Time::set_min_in_hr( int new_min_in_hr ){
   MIN_IN_HR = new_min_in_hr ;
}

void Time::set_hr_in_day( int new_hr_in_day ){
   HR_IN_DAY = new_hr_in_day ;
}



// NON-MEMBER functions/operators 
Time operator+( Time lhs , const Time& rhs ){
   return lhs += rhs ;
}

// . . .


// NON-MEMBER friends
bool operator<( const Time& lhs , const Time& rhs ) {
   return lhs.compare(rhs) < 0;
}

// . . .


// MEMBER functions/operators 
int Time::minutes() const { 
   return min % MIN_IN_HR ; 
}

// . . .

Time& Time::operator+=( const Time& rhs ){
   min += rhs.min;
   normalize();

   return *this;
}

void Time::normalize(){
   min %= MIN_IN_HR * HR_IN_DAY;
   return;
}

int Time::compare( const Time& b) const {
   return ( min - b.min );
}
