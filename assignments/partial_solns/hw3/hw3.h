// This file is part of the _partial_ solution to the _linear search_ assignment.

#ifndef HW3_H
#define HW3_H


#include <cstdlib>    // RAND_MAX, rand(), srand()
#include <ctime>      // time()
#include <cmath>      // fabs()
#include <vector>     // std::vector


// DECLARATION OF FUNCTIONS
double average_complexity( int );
void shuffleVector( std::vector<int>& );
int findPos( int , const std::vector<int>& );

double average_complexity(int j) {

    double const EPSILON = 0.001;

    std::vector<int> v(j);

    // Populate vector with numbers from `1` to `j`
    // . . .

    // Shuffle it...
    shuffleVector(v);

    // Generate a random number and look for it in the shuffled vector. Stop
    // when the average doesn't change much.
    double difference = v.size();
    double average = 0.0;
    double oldAverage;
    int i = 1;
    do{
        oldAverage = average;
        // . . .

        difference = fabs( oldAverage - average );
    }while( difference >=  EPSILON ); 

    return average;
}



// Shuffles a vector with n entries using a Fisher-Yates algorithm. 
// The algorithm:
// - For every index `i` from `(n-1)` to `1` 
//      Generate random index `j` between `0` and `i`
//      Exchange elements `v[i]` and `v[j]`.
void shuffleVector( std::vector<int>& v ){
    for ( int i = v.size() - 1 ; i > 0 ; i-- ){
        int j = rand() % (i+1) ; // <--  `0` <= `j` <= `i`
        if ( i != j ) {
            int temp = v[i];
            v[i] = v[j];
            v[j] = temp;
        }
    }
}



// A specific for of linear search
int findPos( int num , const std::vector<int>& v ){
    for ( size_t pos = 0 ; pos < v.size() ; pos++ ){
        if ( v[pos] == num )
            return pos;
        }
    return -1;
}

#endif
