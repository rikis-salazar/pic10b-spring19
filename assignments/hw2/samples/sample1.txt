*****************************************
Welcome to 'San Angel'!
Time setup:
	1 day = 24 hours,
	1 hour = 60 minutes.
*****************************************
Testing constructors:
	Time a;			a =  0:00
	Time b(5);		b =  0:05
	Time c(61);		c =  1:01
	Time d(47,59);		d = 23:59
	Time X(5.0);		X =  5:00
	Time Y(1.5);		Y =  1:30
	Time Z(25.1);		Z =  1:06
Testing operator+:
	e = b + c;		e =  1:06
	f = d + 2;		f =  0:01
	g = c + 2.75;		f =  3:46
Testing operator+=:
	c += 120;		c =  3:01
	c += 1.99166666;	c =  5:00
	c += 1.99166667;	c =  7:00
Testing other member functions:
	c.set_minutes(60);	c =  7:00
	c.set_minutes(123.45);	c =  7:03
	c.set_minutes(67.89);	c =  7:08
	c.set_hours(45);	c = 21:08
	c.set_hours(1.9);	c =  1:08
	c.set_hours(1.9999);	c =  1:08
Testing comparison operators:
 0:05 occurs earlier in the day than  1:08, hence b != c.
 1:08 occurs later in the day than  0:05, hence c != b.
