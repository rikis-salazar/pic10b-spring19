# A `Time` class

| [ ![pics/manolo_hiding_guitar.png](pics/manolo_hiding_guitar.png) ][manolo-carlos] |  
|:-----:|  
| Figure 1: _Manolo_ and his dad _Carlos Sánchez_ trying to improve _Manolo's_ ~~coding~~ bullfighting style. |  

[manolo-carlos]: https://www.youtube.com/watch?v=QsEVf9J72OI


## The backstory

_Manolo Sánchez_ is a boy who lives in _San Ángel_. He loves music, but coming
from a family of bullfighters, he seems destined to become a _'Matador'_ one
day. His father, _Carlos_, wants to make sure _Manolo_ becomes "the greatest
_Sánchez_ ever" and has begun training him. If _Carlos_ is to accomplish his
goal, he needs to make sure _Manolo's_ style and technique are the best.


## The assignment

You are to provide the interface as well as the definitions of the member
functions of a `Time` class. I will provide drivers and you will have to figure
out what fields and functions are needed so that your library works with them.

For this assignment you are free to:

*   name the private field(s) of the class,
*   implement operators as members, non-members, friends or non-friends, and
*   decide whether or not to implement `public`/`private` member functions in
    addition to those that appear on the driver file.

Please be aware that I will test your library with different drivers that might
not look like the ones I am providing in the next section. You should be very
careful and choose the right types (_e.g.,_ member _vs_ non-member) for your
functions.


## The drivers

The test files and output files below should help you figure out how to code
your class.

1.  [`driver1.cpp`][driver1]: this file does not require additional setup.

    > It models a typical day in this world (1 day = 24 hrs, 1 hour = 60 min).
    > The file [`sample1.txt`][sample1] contains the output produced by my
    > implementation.

1.  [`driver2.cpp`][driver2]: extra setup is needed; see note below.

    > It models a typical day in _the land of the remembered_ (1 day = 60 hrs,
    > 1 hour = 24 min). The file [`sample2.txt`][sample2] contains the output
    > produced by my implementation.

1.  [`driver3.cpp`][driver3]: extra setup is needed; see note below.

    > It models a typical day in _the land of the forgotten_ (1 day = 144 hrs,
    > 1 hour = 10 min). The file [`sample3.txt`][sample3] contains the output
    > produced by my implementation.

[driver1]: drivers/driver1.cpp
[driver2]: drivers/driver2.cpp
[driver3]: drivers/driver3.cpp
[sample1]: samples/sample1.txt
[sample2]: samples/sample2.txt
[sample3]: samples/sample3.txt


### Note:

In order for your library to compile against drivers 2 and 3 you will need to
add a couple of `static` _fields_ to your interface. I will discuss this type of
members in detail during lecture, in the meantime you can focus your efforts on
the first driver, where the static fields can be 'faked' with either constants,
or extra member fields.

In addition to the static fields described above, you will need to implement a
couple of `static` _member functions_. They are called at the beginning of
drivers 2 and 3.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
/** 
    Time setup [do needed in ... ] 
**/ 
Time::set_hr_in_day( ... );
Time::set_min_in_hr( ... ); 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

These calls also appear in [`driver1.cpp`][driver1] but they have been commented
out to illustrate the default values of the static fields. 

You may assume these functions will only be called one [and only one] time at
the beginning of every driver we use to test your library. However, if you want
an extra challenge, [`ultimate_driver.cpp`][driver-ultimate] should be able to
handle several calls to these functions within the same file. The output
corresponding to this driver can be found within the file itself, after the
`main()` routine.

[driver-ultimate]: drivers/ultimate_driver.cpp


## What is this assignment about? 

**Classes**: overloading (constructors, operators, functions), member _vs_
non-member, _friendship_, reference _vs_ value parameters, and the `static`
modifier.

**Style:** avoiding unnecessary statements, writing code that can be easily
reused and/or extended.


## Submission

Make sure your files are named `time_class.h` and `time_class.cpp` (all
lowercase). If your files are not correctly named, your homework might not be
graded. Your code should **contain useful comments** as well as **your name**,
**the date**, and a **brief description of what the library files do**. Upload
your files to the [assignments section of the CCLE website][ccle-hw]. These
files will be automatically collected at the date and time listed in
_"submission status"_ table at the bottom of the CCLE assignment description
corresponding to this project.

> Notice that you **do not need to upload any of the drivers**, as we will be
> testing your library files against possibly different versions of the drivers
> provided here.

[ccle-hw]: https://ccle.ucla.edu/course/view.php?id=73519&section=2


## Grading rubric

| **Category** | **Description** | **Points** |  
|:-------------|:---------------------------------------------------|:-------:|  
| Driver 1 | Project compiles against driver 1 and the output is as expected. | 8 |  
| Drivers 2 & 3 | Project compiles against drivers 2 and 3, and the output is as expected. | 4 |  
| Driver 4 | Project compiles against driver 4 (not provided here), and the output is as expected. | 2 |  
| Coding style | Up to 6 points can be deducted for bad style and/or inefficiencies (_e.g.,_ failing to be `const`-correct, making unnecessary copies of objects, etc.). | 6 |  
| Extra credit | The project compiles against [`ultimate_driver.cpp`][driver-ultimate] and the output is as expected. | up to 5 |  
| | | |  
| Total | | up to 25 |  

**Important:**

> Neither I nor your TA will provide hints and/or suggestions about the extra
> credit. You should only attempt to make your project work against the file
> [`ultimate_driver.cpp`][driver-ultimate] once you are certain it compiles [and
> works as expected] against drivers 1, 2, and 3. You will not receive extra
> points if your project works against [`ultimate_driver.cpp`][driver-ultimate]
> but fails against either of the other drivers provided here.


## List of files

The following files are included with this assignment:

*   Current directory:
    -   [`index.html`][assignment]: this file.
    -   [`readme.md`][assignment-md]: this file's `markdown` source code.
    -   [`Makefile`][makefile]: the _recipe_ that converts `readme.md` into
        `index.html`.
*   `drivers` directory:
    -   [`driver1.cpp`][driver1]: models a typical day in this world.
    -   [`driver2.cpp`][driver2]: models a typical day in _the land of the
        remembered_.
    -   [`driver3.cpp`][driver3]: models a typical day in _the land of the
        forgotten_.
    -   [`ultimate-driver.cpp`][driver-ultimate]: a challenge, for extra credit.
*   `samples` directory:
    -   [`sample1.txt`][sample1]: output produced by `driver1.cpp` when compiled
        against my implementation of the `Time` class.
    -   [`sample2.txt`][sample2]: output produced by `driver1.cpp` when compiled
        against my implementation of the `Time` class.
    -   [`sample3.txt`][sample3]: output produced by `driver1.cpp` when compiled
        against my implementation of the `Time` class.
*   `pics` directory:
    -   [`manolo_hiding_guitar.png`][manolo-image]: _Manolo_ hiding a guitar
        under his _'muleta'_.

[assignment]: index.html
[assignment-md]: readme.md
[makefile]: Makefile
[manolo-image]: pics/manolo_hiding_guitar.png

---

[Return to main course website][PIC]

[PIC]: ../..
