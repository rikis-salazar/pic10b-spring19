# Average complexity of the _Linear Search_ algorithm

| [ ![pics/manolo_about_to_be_judged.png](pics/manolo_about_to_be_judged.png) ][manolo-judged] |  
|:-----:|  
| Figure 1: _Manolo 'calaca' Sánchez_ getting ready to be judged, and to meet _The Candle Maker_. He is on a quest to find _La Catrina_ and explain her how _Xibalba_ cheated... again! |  

[manolo-judged]: https://www.youtube.com/watch?v=NBw5YScs8iQ&feature=youtu.be&t=2m4s


## The backstory

_Manolo Sánchez_ was tricked by _Xibalba_ into thinking that _María Posada_ had
passed away. As it was expected, _Manolo_ found a way to enter _The Land of the
Remembered_ and started looking for her.  Alas...  as it turns out, she is not
dead and _Manolo's_ chances of getting back to _San Ángel_ are slim (to say the
least). He needs to outsmart _Xibalba_ and enter _The Land of the Forgotten_.
Once there he needs to find _La Catrina_ and make his case to her.

To complicate matters even further, it is likely _Manolo_ will have to complete
a series of tasks in order to gain access to the _Cave of Souls_, the place
where the secret entrance to _The Land of the Forgotten_ can be found. Since he
took a `SULP-SULP-EES` class with professor _Razalas_, he is confident his
knowledge of algorithms will come in handy at some point. Unfortunately, class
met at 12:00 noon, and sometimes it was very hard for him to stay focused for
the whole class. Things were particularly bad when they were discussing
algorithm complexity and 'Big Oh' notation.

To be successful on his journey, _Manolo_ needs to be able to solve the tasks
presented to him as fast as possible; it is imperative that he understands how
much time an algorithm takes in the long run. Your job is to help _Manolo_
perform a series of simulations that allow him, and you as well, understand how
algorithm complexity works.


## The assignment

You are to write a function with signature

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
double average_complexity( int );
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

that, if coded correctly, should help you confirm the formula we obtained for
the average running time for the _linear search_ algorithm discussed in lecture.
This function should be declared and defined (_i.e.,_ implemented) in a library
file named `hw3.h`. To test whether or not your implementation is syntactically
correct, you may use the driver [`hw3-driver.cpp`][the-driver].

[the-driver]: drivers/hw3-driver.cpp

The code in this file contains a loop that generates 100 random numbers between
1 and the constant `MAX_N` defined within. Individually, every random number
(`n`) is passed as a parameter to `average_complexity(...)` and the return value
(`T_n`) is recorded; then a file stream object (`fout`) is used to store the
pair (`n`,`T_n`) in a new line of the file `data.tsv`.

In code:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
for ( ... ) {        // runs a fixed number of times
    int n = ... ;    // random number in [1,MAX_N]
    double T_n = average_complexity(n);
    fout << n << '\t' << T_n << '\n';
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Within the function `average_complexity(...)` you might want to consider
performing the following steps to obtain the number `T_n` described above:

*   Create a vector $\bar{v}_{j}$ with exactly $x_{j}$ entries, where $x_{j}$ is
    the integer (`n`) that is passed as parameter to your function the $j$-th
    time it is called. The vector $\bar{v}_{j}$ should contain every number from
    1 to $x_{j}$ in random order.

    > For example, if $x_{1} = 6$, then one possibility for this vector is
    > $\bar{v}_{1} = (5,3,4,6,1,2)$. 

*   Repeatedly generate random numbers between 1 and $x_{j}$ and search for them
    in the vector $\bar{v}_{j}$. Use the linear search function described during
    lecture, and make sure to keep track of the position where these numbers are
    located (as they roughly represent the number of operations that are
    performed by the algorithm). Also keep track of the average number of
    operations performed every time you search for a new random number.

    > For example, if $x_{1}$ and $\bar{v}_{1}$ are as before, and if your
    > program produces the random numbers $r_{1} = 2$, $r_{2} = 5$, $r_{3} = 3$,
    > and $r_{4} = 2$; then the evolution of the average is as follows:
    >
    > -   $r_{1}$ is the sixth entry of the vector and on average it took
    >     $\dfrac{6}{1} = 6$ comparisons to find a random number.
    > -   $r_{2}$ is the first entry of the vector and on average it took
    >     $\dfrac{6+1}{2} = 3.5$ comparisons to find a random number.
    > -   $r_{3}$ is the second entry of the vector and on average it took
    >     $\dfrac{6+1+2}{3} = 3$ comparisons to find a random number.
    > -   $r_{4}$ is the sixth entry of the vector and on average it took
    >     $\dfrac{6+1+2+6}{4} = 3.75$ comparisons to find a random number.

    If your implementation works the way it is expected, the average will
    eventually settle to the running time of the average case scenario we
    discussed during lecture. You should think of implementing a stopping
    condition that allows you to obtain accurate results without taking too much
    time.

    > **Note:** It would be much easier and accurate to use a loop to search for
    > every number between 1 and $x_{j}$ in the vector $\bar{v}_{j}$, and then
    > compute the average number of comparisons that we performed. If we did so,
    > our result would be exactly $\dfrac{x_{j}+1}{2}$. Here I am asking you not
    > to follow that approach, in part, because one of the goals of this
    > assignment is to help you develop some intuition regarding numerical
    > simulations. In these situations, it is often the case that we do not know
    > what the right answer is, but we have a pretty good idea of how to get to
    > it by employing a 'brute force' approach.

As described earlier, the driver file will populate the text file (`data.tsv`)
with the data collected from the 100 times your function is called. If we denote
by $y_{j}$ the value (`T_n`) returned by `average_complexity(...)`, then this
file should consist of 100 lines of the form

|              |              |              |  
|:------------:|:------------:|:------------:|  
| $x_{1}$      | `'\t'`       | $y_{1}$      |  
| $x_{2}$      | `'\t'`       | $y_{2}$      |  
| $\,\vdots$   | $\vdots$     | $\vdots$     |  
| $x_{100}$    | `'\t'`       | $y_{100}$    |  

where `'\t'` is an invisible _tab_ character.

The file extension (`.tsv`) stands for _"**t**ab **s**eparated **v**alues"_;
which is a type of file that most spreadsheet programs (_e.g.,_ Excel, Google
Sheets) can import. If you want to visualize your results, you can use the [_add
a chart/graph_][google-graph] feature from your favorite spreadsheet
application.  If your computations are correct, you should see a line with data
points more or less evenly distributed.

[google-graph]: https://www.google.com/search?q=plot+columns+google+sheets

Alternatively, if you are familiar with the Graphics classes provided by the
author of your textbook, you may attempt to compile [this graphing
program][plotter]. If successful, when the compiled program, say `display.exe`,
is run in the same folder where the `data.tsv` file is located, it should
produce a graph of the data contained in the file.

[plotter]: misc/simple_plotter.cpp

> For example in my implementation (with `MAX_N = 100000`) when `data.tsv` is
> fed to `display.exe`, it produces the graph in figure 2.
>
> | ![pics/N_100000.png](pics/N_100000.png) |  
> |:-----:|  
> | Figure 2: Visual representation of the average comparisons performed by the _linear search_ algorithm. |  

[lin-search-image]: pics/N_100000.png

One last thing: in the comments section of your library file you should indicate
(using 'big Oh' notation) the complexity of the running time of your program.

> For example you could write:
>
> ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
> // File: hw3.h
> // Author: Ricardo Salazar
> // Date: April 10, 2015
> //
> // Program that implements the function
> //     double average_complexity( int )
> // that repeatedly performs linear searches on a vector and computes
> // the average number of comparisons needed to find an element.
> //
> // This program has a running time T = O( N Log(N) ).
> ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

I recommend you run your program several times with different values of `MAX_N`
and keep track of how long it takes for it to return control to your operating
system. With this information, you should be able to make an educated guess of
its running time. I ran my simulation with `MAX_N` as small as 2000 (program
completed in 1 second), and as big as 200,000 (where it took almost 9 minutes).

As with most other drivers provided in the past, modify the one provided here at
your own risk. I will use a slightly different version of it to test your
program. However, if your library file compiles against
[`hw3-driver.cpp`][the-driver], it should have no issues against the driver used
for grading purposes. In particular, if you need extra libraries and/or want to
implement other functions/classes, you should add all of the extra code in your
library file.


## What is this assignment about? 

[It's all about ~~performance~~][i-dont-think-youll-understand], complexity, and
running times.

[i-dont-think-youll-understand]: https://www.youtube.com/watch?v=Vn29DvMITu4


## Technical notes

i.  If you need a refresher on how to generate random numbers you can take a
    look at my [slides from Pic 10a][random-numbers-pdf].

i.  If $\mu_{n}$ represents the average between the $n$ numbers $a_{1}, \dots,
    a_{n}$, and if $a_{n+1}$ is another number, then the average of all $n+1$
    numbers can be computed via the formula:

    $$\mu_{n+1} = \frac{ n\mu_{n} + a_{n+1} }{ n+1 }.$$

[random-numbers-pdf]: https://www.pic.ucla.edu/~rsalazar/pic10a/lessons/lesson13.pdf

## Submission

Make sure your file is named `hw3.h` (all lowercase). If your file is not
correctly named, your homework might not be graded. Your code should **contain
useful comments** as well as **your name**, **the date**, and a **brief
description of what the library file does**. Upload your file to the
[assignments section of the CCLE website][ccle-hw]. This file will be
automatically collected at the date and time listed in _"submission status"_
table at the bottom of the CCLE assignment description corresponding to this
project.

> Notice that you **do not need to upload any driver**, as we will be testing
> your library files against possibly different drivers.

[ccle-hw]: https://ccle.ucla.edu/course/view.php?id=73519&section=2


## Grading rubric

| **Category** | **Description** | **Points** |  
|:-------------|:---------------------------------------------------|:-------:|  
| Correctness | No errors. The project compiles and the setup is as instructed. | 4 |  
| Experiment data | The data reported in the file `data.tsv` is obtained via experimentation (statistical analysis will be applied to rule out 'perfect' data). | 4 |  
| Efficiency | A program that takes too long to process relatively small values of `MAX_N` will be penalized. | 4 |  
| Computations | The data obtained does verify that the complexity of linear search is linear. | 4 |  
| Coding style | The code is efficient and easy to follow. | 4 |  
| | | |  
| Total | | 20 |  


## List of files

The following files are included with this assignment:

*   Current directory:
    -   [`index.html`][assignment]: this file.
    -   [`readme.md`][assignment-md]: this file's `markdown` source code.
    -   [`Makefile`][makefile]: the _recipe_ that converts `readme.md` into
        `index.html`.
*   `drivers` directory:
    -   [`hw3-driver.cpp`][the-driver]: modify at your own risk!
*   `misc` directory:
    -   [`simple_plotter.cpp`][plotter]: source code for a simple plotter
        program; [click here to access various walk-throughs][ccc-graphics] for
        several different platforms, they should help you get started with
        _Horstmann's_ graphics libraries. Do not hesitate to get in touch with
        us should you need help with the setup.
*   `pics` directory:
    -   [`manolo_about_to_be_judged.png`][manolo-image]: _Manolo_ at the
        entrance entrance of _The Cave of Souls_.
    -   [`N_100000.png`][lin-search-image]: Visualization of experimental
        results (`MAX_N = 100000`).

[assignment]: index.html
[assignment-md]: readme.md
[makefile]: Makefile

[manolo-image]: pics/manolo_about_to_be_judged.png
[ccc-graphics]: https://www.pic.ucla.edu/~rsalazar/pic10a/handouts/

---

[Return to main course website][PIC]

[PIC]: ../..

