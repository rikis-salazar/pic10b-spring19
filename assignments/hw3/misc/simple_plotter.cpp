#include "ccc_win.h"
#include <cmath>
#include <stdlib.h>
#include <fstream>


enum plotType_t {BAR, _POINT, HIST, LINE};

// function declarations
void displayData( istream&, plotType_t, double );
void setViewingWindow( double, double, double, double, double );
void writeToLog( ostream&, double, double, double, double, double );
void preProcessDataFile( string, double&, double&, double&, double&, double& );


// ****************************************************
// function implementations 
// ****************************************************
void preProcessDataFile( string fileName,
                        double& X_min, double& X_max,
                        double& Y_min, double& Y_max,
                        double& D_x){

    // Open the file stream and read the first four values.  The first two are
    // the initial Max and Min. Then compute the initial Delta x. This value is
    // needed to display histograms. Once this is done, close the stream.
    double x1, y1, x2, y2;
    ifstream fin;

    fin.open( fileName.c_str() );
    if ( fin >> x1 >> y1 ) {
        if ( fin >> x2 >> y2 ){
            X_min = x1;
            Y_min = y1;
            X_max = x1;
            Y_max = y1;
            D_x = fabs( x2 - x1 );
        }
        else{
            fin.close();
            exit(2);    // Error: file does not contain at least two points.
        }
    }
    else{
        fin.close();
        exit(1);        // Error: file does not contain at least one point.
    }

    fin.close();

    // Open the file stream again and find the maximum and minimum values for x
    // and y, these values are stored in the parameters X_min, X_max, Y_min,
    // Y_max. They will be used to set the viewing window.  The minimum of all
    // differences between consecutive values of x is also found. It is stored
    // in the parameter D_x (delta x).
    // 
    // If we reach this point of the code we have at least two valid points in
    // the input file.
   fin.open( fileName.c_str() );
   fin >> x1 >> y1;
    while ( fin >> x2 >> y2 ){
        if ( x2 > X_max )
            X_max = x2;
        if ( x2 < X_min )
            X_min = x2;
        if ( y2 > Y_max )
            Y_max = y2;
        if ( y2 < Y_min )
            Y_min = y2;

        if ( fabs( x2 - x1 ) < D_x )
            D_x = fabs( x2 - x1 );

        x1 = x2;
        // y1 = y2;     // <-- not needed because delta y is not computed
    }
    fin.close();

    return; 
}


// Useful for debugging purposes. Reports the values found.
void writeToLog( ostream& out, double xmin, double xmax, 
                 double ymin, double ymax, double dx ){
    out << "X min: " << xmin << endl;
    out << "X max: " << xmax << endl;
    out << "Y min: " << ymin << endl;
    out << "Y max: " << ymax << endl;
    out << "Delta x: " << dx << endl;
    return;
}


// This function takes care of the plotting... It is missing the part that
// displays the graph of a function as 'a curve' (if enough data is provided).
void displayData( istream& in, plotType_t dispMode, double D_x ){
    double x,y;

    while ( in >> x >> y ) {
        Point Y(x,y);
        Point X(x,0);

        switch (dispMode){
            case HIST:
                cwin << Line(Point( x - D_x/3.0, 0) , Point(x - D_x/3.0, y) )
                     << Line(Point( x + D_x/3.0, 0) , Point(x + D_x/3.0, y) )
                     << Line(Point( x - D_x/3.0, y) , Point(x + D_x/3.0, y) );
                break;

            case BAR:
                cwin << Line(X,Y);
                break;

            case _POINT:
                cwin << Y;
                break;

            case LINE:
                /* Your implementation here! */
                break;
        }
    }

    return;
}

void setViewingWindow( double X_min, double X_max,
                       double Y_min, double Y_max, double D_x ){
    // 10% padding in both the X and Y directions
    double X_diff = ( X_max - X_min );
    double Y_diff = ( Y_max - Y_min );

    // Viewing window
    cwin.coord( X_min - 0.1 * X_diff, Y_max + 0.1 * Y_diff,
                X_max + 0.1 * X_diff, Y_min - 0.1 * Y_diff );

    // coordinate axis
    cwin << Line( Point(X_min - 0.05 * X_diff, 0 ), 
                  Point(X_min - 0.05 * X_diff, Y_max + 0.05 * Y_diff ) );
    cwin << Line( Point(X_max + 0.05 * X_diff, 0 ), 
                  Point(X_min - 0.05 * X_diff, 0 ) );

    return;
}


/* **********************************************************
                        M A I N   
   ********************************************************** */
int ccc_win_main(){  

    double X_min, X_max, Y_max, Y_min, D_x; 

    // Finds Max and Min x and y values to set the viewing window.
    preProcessDataFile( "data.tsv", X_min, X_max, Y_min, Y_max, D_x ); 

    // Reports data to a log (could be the console)
    ofstream fout;
    fout.open("output.txt");  
    writeToLog( fout, X_min, X_max, Y_min, Y_max, D_x );
    // writeToLog( cout, X_min, X_max, Y_min, Y_max, D_x );
    fout.close();

    // Prepare graphing area
    setViewingWindow( X_min, X_max, Y_min, Y_max, D_x );

    // read data and display to viewing window
    ifstream fin;
    fin.open("data.tsv");

    // select one line
    //displayData(fin, BAR, D_x);
    //displayData(fin, HIST, D_x);
    //displayData(fin, LINE, D_x); // <-- Not implemented!
    displayData(fin, _POINT, D_x); // VS does not like POINT... so _POINT it is.

    fin.close();

    return 0;
}
