::::::::::::::
OutputFiles/01-combined.txt
::::::::::::::
*****************************************
Welcome to 'San Angel'!
Time setup:
	1 day = 24 hours,
	1 hour = 60 minutes.
*****************************************
Testing constructors:
	Time a;			a = 0:00
	Time b(5);		b = 0:05
	Time c(61);		c = 0:01
	Time d(47,59);		d = 23:59
	Time X(5.0);		X = 5:00
	Time Y(1.5);		Y = 1:30
	Time Z(25.1);		Z = 25:06
Testing operator+:
	e = b + c;		e = 0:06
	f = d + 2;		f = 0:01
	g = c + 2.75;		f = 2:46
Testing operator+=:
	c += 120;		c = 0:01
	c += 1.99166666;	c = 1:00
	c += 1.99166667;	c = 2:59
Testing other member functions:
	c.set_minutes(60);	c = 2:59
	c.set_minutes(123.45);	c = 2:59
	c.set_minutes(67.89);	c = 2:59
	c.set_hours(45);	c = 2:59
	c.set_hours(1.9);	c = 2:59
	c.set_hours(1.9999);	c = 2:59
Testing comparison operators:
0:05 occurs earlier in the day than 2:59, hence b != c.
2:59 occurs later in the day than 0:05, hence c != b.
*****************************************
Welcome to 'the land of the remembered'!
Time setup:
	1 day = 60 hours,
	1 hour = 24 minutes.
*****************************************
Testing constructors:
	Time a;			a = 0:00
	Time b(5);		b = 0:05
	Time c(61);		c = 0:13
	Time d(47,59);		d = 47:11
	Time Z(59.5);		Z = 59:12
Testing operator+:
	e = b + c		e = 0:18
	f = d + 14		f = 48:01
Testing operator+=:
	c += 120;		c = 0:13
Testing other member functions:
	c.set_minutes(24);	c = 0:13
	c.set_minutes(63);	c = 0:13
	c.set_hours(456);	c = 0:13
Testing comparison operators:
0:05 occurs earlier in the day than 0:13, hence b != c.
0:13 occurs later in the day than 0:05, hence c != b.
*****************************************
Welcome to 'the land of the forgotten'!
Time setup:
	1 day = 144 hours,
	1 hour = 10 minutes.
*****************************************
Testing constructors:
	Time a;			a = 0:00
	Time b(5);		b = 0:05
	Time c(151,0);		c = 7:00
	Time d(47,59);		d = 47:09
	Time Y(0.45000001);	Y = 0:04
	Time Z(1.94999999);	Z = 1:09
Testing operator+:
	e = b + c		e = 7:05
	f = d + 1		f = 48:00
Testing operator+=:
	c += 120;		c = 7:00
Testing other member functions:
	c.set_minutes(24);	c = 7:00
	c.set_minutes(63);	c = 7:00
	c.set_hours(456);	c = 7:00
Testing comparison operators:
0:05 occurs earlier in the day than 7:00, hence b != c.
hence c != b.
::::::::::::::
OutputFiles/03-ultimate.txt
::::::::::::::
*****************************************
Welcome to 'San Angel'!
[ 1 day = 24 hours, 1 hour = 60 minutes ]

a = 0:00	b = 0:05	c = 0:01
d = 23:59	X = 5:00	Y = 1:30
		Z = 25:06
*****************************************
Welcome to the land of the remembered!
[ 1 day = 60 hours, 1 hour = 24 minutes ]

a = 0:00	b = 0:05	c = 0:01
d = 23:59	X = 5:00	Y = 1:30
		Z = 25:06
*****************************************
Welcome to the land of the forgotten!
[1 day = 144 hours,  1 hour = 10 minutes]

a = 0:00	b = 0:05	c = 0:01
d = 23:59	X = 5:00	Y = 1:30
		Z = 25:06
*****************************************
Welcome back to 'San Angel'!

a = 0:00	b = 0:05	c = 0:01
d = 23:59	X = 5:00	Y = 1:30
		Z = 25:06
