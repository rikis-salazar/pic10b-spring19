*****************************************
Welcome to 'San Angel'!
Time setup:
	1 day = 24 hours,
	1 hour = 60 minutes.
*****************************************
Testing constructors:
	Time a;			a = 0:0
	Time b(5);		b = 0:5
	Time c(61);		c = 1:1
	Time d(47,59);		d = 23:59
	Time X(5.0);		X = 5:0
	Time Y(1.5);		Y = 1:30
	Time Z(25.1);		Z = 1:6
Testing operator+:
	e = b + c;		e = 1:6
	f = d + 2;		f = 0:1
	g = c + 2.75;		f = 3:46
Testing operator+=:
	c += 120;		c = 3:1
	c += 1.99166666;	c = 5:0
	c += 1.99166667;	c = 6:59
Testing other member functions:
	c.set_minutes(60);	c = 7:0
	c.set_minutes(123.45);	c = 7:3
	c.set_minutes(67.89);	c = 7:8
	c.set_hours(45);	c = 21:8
	c.set_hours(1.9);	c = 1:8
	c.set_hours(1.9999);	c = 1:8
Testing comparison operators:
1:6 occurs earlier in the day than 1:8, hence b != c.
1:8 occurs later in the day than 1:6, hence c != b.
*****************************************
Welcome to 'the land of the remembered'!
Time setup:
	1 day = 60 hours,
	1 hour = 24 minutes.
*****************************************
Testing constructors:
	Time a;			a = 0:0
	Time b(5);		b = 0:5
	Time c(61);		c = 2:13
	Time d(47,59);		d = 49:11
	Time Z(59.5);		Z = 59:12
Testing operator+:
	e = b + c		e = 2:18
	f = d + 14		f = 50:1
Testing operator+=:
	c += 120;		c = 7:13
Testing other member functions:
	c.set_minutes(24);	c = 8:0
	c.set_minutes(63);	c = 10:15
	c.set_hours(456);	c = 36:15
Testing comparison operators:
2:18 occurs earlier in the day than 36:15, hence b != c.
36:15 occurs later in the day than 2:18, hence c != b.
*****************************************
Welcome to 'the land of the forgotten'!
Time setup:
	1 day = 144 hours,
	1 hour = 10 minutes.
*****************************************
Testing constructors:
	Time a;			a = 0:0
	Time b(5);		b = 0:5
	Time c(151,0);		c = 7:0
	Time d(47,59);		d = 52:9
	Time Y(0.45000001);	Y = 0:4
	Time Z(1.94999999);	Z = 1:9
Testing operator+:
	e = b + c		e = 7:5
	f = d + 1		f = 53:0
Testing operator+=:
	c += 120;		c = 19:0
Testing other member functions:
	c.set_minutes(24);	c = 21:4
	c.set_minutes(63);	c = 27:3
	c.set_hours(456);	c = 24:3
Testing comparison operators:
7:5 occurs earlier in the day than 24:3, hence b != c.
24:3 occurs later in the day than 7:5, hence c != b.
