--- a/hw2/submissions/705212767/time_class.h
+++ b/hw2/submissions/705212767/time_class.h
@@ -9,8 +9,8 @@ public:
 	Time(int a);
 	Time(int a, int b);
 	Time(double a);
-	int minutes();
-	int hours();
+	int minutes() const;
+	int hours() const;
 	int totalminutes();
 	void setParameters();
 	



--- a/hw2/submissions/705212767/time_class.cpp
+++ b/hw2/submissions/705212767/time_class.cpp
@@ -2,16 +2,18 @@
 #include <iostream>
 using namespace std;
 
-Linker Errors on Visual studio and Laguna server states that "hw2.app is not a binary file or does not exist"
+// Linker Errors on Visual studio and Laguna server states that "hw2.app is not a binary file or does not exist"
 
+int Time::min_in_an_hr = 60;
+int Time::hr_in_a_day = 24;
 
 void Time::setParameters() { HOURS = (totalMinutes / min_in_an_hr) % hr_in_a_day; MINUTES = totalMinutes % min_in_an_hr; }
 Time::Time() { totalMinutes = 0; setParameters(); }
 Time::Time(int a) { totalMinutes = a; setParameters();}
 Time::Time(int a, int b) { totalMinutes = (min_in_an_hr * a) + b; setParameters();}
 Time::Time(double a) { totalMinutes = static_cast<int>(min_in_an_hr*a); setParameters();}
-int Time::minutes() { return MINUTES; }
-int Time::hours() { return HOURS; }
+int Time::minutes() const  { return MINUTES; }
+int Time::hours() const { return HOURS; }
 int Time::totalminutes() { return totalMinutes; }
 
 Time& Time::operator+(Time a) { totalMinutes += a.totalminutes(); setParameters(); return *this; }
@@ -25,7 +27,7 @@ bool Time::operator!=(Time a) { if (totalMinutes != a.totalminutes()) return tru
 bool Time::operator>=(Time a) { if (totalMinutes >= a.totalminutes()) return true; else return false; }
 bool Time::operator<(Time a) { if (totalMinutes < a.totalminutes()) return true; else return false; }
 
-ostream& operator<<(ostream& output, Time a) {
+ostream& operator<<(ostream& output, const Time& a) {
 	output << a.hours() << ":" << a.minutes();
 	return output;}
 
