::::::::::::::							::::::::::::::
OutputFiles/01-combined.txt					OutputFiles/01-combined.txt
::::::::::::::							::::::::::::::
*****************************************			*****************************************
Welcome to 'San Angel'!						Welcome to 'San Angel'!
Time setup:							Time setup:
	1 day = 24 hours,						1 day = 24 hours,
	1 hour = 60 minutes.						1 hour = 60 minutes.
*****************************************			*****************************************
Testing constructors:						Testing constructors:
	Time a;			a =  0:00		      |		Time a;			a = 0:00
	Time b(5);		b =  0:05		      |		Time b(5);		b = 0:05
	Time c(61);		c =  1:01		      |		Time c(61);		c = 1:01
	Time d(47,59);		d = 23:59				Time d(47,59);		d = 23:59
	Time X(5.0);		X =  5:00		      |		Time X(5.0);		X = 5:00
	Time Y(1.5);		Y =  1:30		      |		Time Y(1.5);		Y = 1:30
	Time Z(25.1);		Z =  1:06		      |		Time Z(25.1);		Z = 1:06
Testing operator+:						Testing operator+:
	e = b + c;		e =  1:06		      |		e = b + c;		e = 1:06
	f = d + 2;		f =  0:01		      |		f = d + 2;		f = 0:01
	g = c + 2.75;		f =  3:46		      |		g = c + 2.75;		f = 3:46
Testing operator+=:						Testing operator+=:
	c += 120;		c =  3:01		      |		c += 120;		c = 3:01
	c += 1.99166666;	c =  5:00		      |		c += 1.99166666;	c = 5:00
	c += 1.99166667;	c =  7:00		      |		c += 1.99166667;	c = 7:00
Testing other member functions:					Testing other member functions:
	c.set_minutes(60);	c =  7:00		      |		c.set_minutes(60);	c = 7:00
	c.set_minutes(123.45);	c =  7:03		      |		c.set_minutes(123.45);	c = 7:03
	c.set_minutes(67.89);	c =  7:08		      |		c.set_minutes(67.89);	c = 7:08
	c.set_hours(45);	c = 21:08				c.set_hours(45);	c = 21:08
	c.set_hours(1.9);	c =  1:08		      |		c.set_hours(1.9);	c = 1:08
	c.set_hours(1.9999);	c =  1:08		      |		c.set_hours(1.9999);	c = 1:08
Testing comparison operators:					Testing comparison operators:
 0:05 occurs earlier in the day than  1:08, hence b != c.     |	0:05 occurs earlier in the day than 1:08, hence b != c.
 1:08 occurs later in the day than  0:05, hence c != b.	      |	1:08 occurs later in the day than 0:05, hence c != b.
*****************************************			*****************************************
Welcome to 'the land of the remembered'!			Welcome to 'the land of the remembered'!
Time setup:							Time setup:
	1 day = 60 hours,						1 day = 60 hours,
	1 hour = 24 minutes.						1 hour = 24 minutes.
*****************************************			*****************************************
Testing constructors:						Testing constructors:
	Time a;			a =  0:00		      |		Time a;			a = 0:00
	Time b(5);		b =  0:05		      |		Time b(5);		b = 0:05
	Time c(61);		c =  2:13		      |		Time c(61);		c = 2:13
	Time d(47,59);		d = 49:11				Time d(47,59);		d = 49:11
	Time Z(59.5);		Z = 59:12				Time Z(59.5);		Z = 59:12
Testing operator+:						Testing operator+:
	e = b + c		e =  2:18		      |		e = b + c		e = 2:18
	f = d + 14		f = 50:01				f = d + 14		f = 50:01
Testing operator+=:						Testing operator+=:
	c += 120;		c =  7:13		      |		c += 120;		c = 7:13
Testing other member functions:					Testing other member functions:
	c.set_minutes(24);	c =  7:00		      |		c.set_minutes(24);	c = 7:00
	c.set_minutes(63);	c =  7:15		      |		c.set_minutes(63);	c = 7:15
	c.set_hours(456);	c = 36:15				c.set_hours(456);	c = 36:15
Testing comparison operators:					Testing comparison operators:
 0:05 occurs earlier in the day than 36:15, hence b != c.     |	0:05 occurs earlier in the day than 36:15, hence b != c.
36:15 occurs later in the day than  0:05, hence c != b.	      |	36:15 occurs later in the day than 0:05, hence c != b.
*****************************************			*****************************************
Welcome to 'the land of the forgotten'!				Welcome to 'the land of the forgotten'!
Time setup:							Time setup:
	1 day = 144 hours,						1 day = 144 hours,
	1 hour = 10 minutes.						1 hour = 10 minutes.
*****************************************			*****************************************
Testing constructors:						Testing constructors:
	Time a;			a =  0:00		      |		Time a;			a = 0:00
	Time b(5);		b =  0:05		      |		Time b(5);		b = 0:05
	Time c(151,0);		c =  7:00		      |		Time c(151,0);		c = 7:00
	Time d(47,59);		d = 52:09				Time d(47,59);		d = 52:09
	Time Y(0.45000001);	Y =  0:05		      |		Time Y(0.45000001);	Y = 0:05
	Time Z(1.94999999);	Z =  1:09		      |		Time Z(1.94999999);	Z = 1:09
Testing operator+:						Testing operator+:
	e = b + c		e =  7:05		      |		e = b + c		e = 7:05
	f = d + 1		f = 53:00				f = d + 1		f = 53:00
Testing operator+=:						Testing operator+=:
	c += 120;		c = 19:00				c += 120;		c = 19:00
Testing other member functions:					Testing other member functions:
	c.set_minutes(24);	c = 19:04				c.set_minutes(24);	c = 19:04
	c.set_minutes(63);	c = 19:03				c.set_minutes(63);	c = 19:03
	c.set_hours(456);	c = 24:03				c.set_hours(456);	c = 24:03
Testing comparison operators:					Testing comparison operators:
 0:05 occurs earlier in the day than 24:03, hence b != c.     |	0:05 occurs earlier in the day than 24:03, hence b != c.
24:03 occurs later in the day than  0:05, hence c != b.	      |	24:03 occurs later in the day than 0:05, hence c != b.
::::::::::::::							::::::::::::::
OutputFiles/02-mystery.txt					OutputFiles/02-mystery.txt
::::::::::::::							::::::::::::::
*****************************************			*****************************************
Additional testing!!!						Additional testing!!!
*****************************************			*****************************************
	Time a(61);		a =  1:01		      |		Time a(61);		a = 1:01

Testing `const` correctness:					Testing `const` correctness:
	Time b(5);	PASSED	b =  0:05		      |		Time b(5);	PASSED	b = 0:05


Testing boolean expressions:					Testing boolean expressions:
	 a != 5		PASSED						 a != 5		PASSED
	 b == 5		PASSED						 b == 5		PASSED
	 5 != a		PASSED						 5 != a		PASSED
	 5 == b		PASSED						 5 == b		PASSED

::::::::::::::							::::::::::::::
OutputFiles/03-ultimate.txt					OutputFiles/03-ultimate.txt
::::::::::::::							::::::::::::::
*****************************************			*****************************************
Welcome to 'San Angel'!						Welcome to 'San Angel'!
[ 1 day = 24 hours, 1 hour = 60 minutes ]			[ 1 day = 24 hours, 1 hour = 60 minutes ]

a =  0:00	b =  0:05	c =  1:01		      |	a = 0:00	b = 0:05	c = 1:01
d = 23:59	X =  5:00	Y =  1:30		      |	d = 23:59	X = 5:00	Y = 1:30
		Z =  1:06				      |			Z = 1:06
*****************************************			*****************************************
Welcome to the land of the remembered!				Welcome to the land of the remembered!
[ 1 day = 60 hours, 1 hour = 24 minutes ]			[ 1 day = 60 hours, 1 hour = 24 minutes ]

a =  0:00	b =  0:05	c =  2:13		      |	a = 0:00	b = 0:05	c = 2:13
d = 59:23	X = 12:12	Y =  3:18		      |	d = 59:23	X = 12:12	Y = 3:18
		Z =  2:18				      |			Z = 2:18
*****************************************			*****************************************
Welcome to the land of the forgotten!				Welcome to the land of the forgotten!
[1 day = 144 hours,  1 hour = 10 minutes]			[1 day = 144 hours,  1 hour = 10 minutes]

a =  0:00	b =  0:05	c =  6:01		      |	a = 0:00	b = 0:05	c = 6:01
d = 143:09	X = 30:00	Y =  9:00		      |	d = 143:09	X = 30:00	Y = 9:00
		Z =  6:06				      |			Z = 6:06
*****************************************			*****************************************
Welcome back to 'San Angel'!					Welcome back to 'San Angel'!

a =  0:00	b =  0:05	c =  1:01		      |	a = 0:00	b = 0:05	c = 1:01
d = 23:59	X =  5:00	Y =  1:30		      |	d = 23:59	X = 5:00	Y = 1:30
		Z =  1:06				      |			Z = 1:06
