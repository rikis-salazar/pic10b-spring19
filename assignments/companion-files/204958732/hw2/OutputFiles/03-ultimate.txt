*****************************************
Welcome to 'San Angel'!
[ 1 day = 24 hours, 1 hour = 60 minutes ]

a = 0:00	b = 0:05	c = 1:01
d = 23:59	X = 5:00	Y = 1:30
		Z = 25:06
*****************************************
Welcome to the land of the remembered!
[ 1 day = 60 hours, 1 hour = 24 minutes ]

a = 0:00	b = 0:05	c = 1:01
d = 23:59	X = 5:00	Y = 1:30
		Z = 25:06
*****************************************
Welcome to the land of the forgotten!
[1 day = 144 hours,  1 hour = 10 minutes]

a = 0:00	b = 0:05	c = 1:01
d = 23:59	X = 5:00	Y = 1:30
		Z = 25:06
*****************************************
Welcome back to 'San Angel'!

a = 0:00	b = 0:05	c = 1:01
d = 23:59	X = 5:00	Y = 1:30
		Z = 25:06
