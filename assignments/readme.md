# Homework assignments

This is the place to find homework assignment descriptions. Please note that you
will not be able to submit your code via this setup. Instead every assignment
will contain a link to its corresponding CCLE assignment submission entry.

*   [Project 6: _the Hanoi puzzle_][HW6].

    > **Note:**
    >
    > In this assignment you will be using a lot of _raw_ pointers which will
    > very likely lead to run-time crashes. To try to minimize them, make sure
    > to study the code for a doubly linked list, and also read the
    > corresponding linked list handout. 

*   [Project 5: _a 'dragon' curve_][HW5].

    > **Note:**
    >
    > In order to receive credit for this assignment, your implementation **must
    > be recursive**.

*   [Project 4: _our very own_ `Pic10b::vector` _class_][HW4].

    > **Note:**
    >
    > This will be perhaps the most complicated project during this quarter. You
    > will be given more time to work on it, but make sure you do not leave
    > things until the very end.

*   [Project 3: _average complexity of the linear search algorithm_][HW3].

    > **Note:**
    >
    > To visualize your results, use your favorite spreadsheet application
    > (_e.g.,_ Excel, Google Sheets, etc.) to import the `.tsv` file that this
    > project will generate; then [add a graph based on this data][graph].

    [graph]: https://www.google.com/search?q=plot+columns+google+sheets

*   [Project 2: _a_ `Time` _class_][HW2].

    > "**Note:**
    >
    > In order for your library to compile against drivers 2 and 3 you will need
    > to add a couple of `static` fields to your interface. I will discuss this
    > type of members in detail during lecture, in the meantime you can focus
    > your efforts on the first driver, where the static fields can be _faked_
    > with either constants, or extra member fields."

*   [Project 1: _a grade calculator_][HW1].

    > **Important:**
    >
    > In order to get started with this assignment you will need to place text
    > files (_e.g.,_ `xibalba.txt`, `test0.txt`, etc.) in a location where your
    > IDE (_e.g.,_ Visual Studio, Xcode, etc.) can find them. [This handout][h2]
    > might come in handy. If you cannot succesfully set your IDE, make sure to
    > get in touch with us as soon as possible.

*   [Project 0 (mock assignment): _siete y medio (cards game)_][HW0].

    This assignment **will not** be collected nor graded. In some previous
    quarters, this is the project students enrolled in my _PIC 10A_ course are
    expected to submit by the end of the corresponding quarter.

    > **Important:**
    >
    > If you find this assignment too difficult for you, make sure you review
    > your notes from previous courses and/or get in touch with us (TA,
    > instructor) as soon as possible to make sure you are not _left behind_.

[h2]: ../handouts/working-directory-handout/
[HW6]: hw6/
[HW5]: hw5/
[HW4]: hw4/
[HW3]: hw3/
[HW2]: hw2/
[HW1]: hw1/
[HW0]: hw0/


---

## Compilation results

*   [Assignment 1 compilation results][log1]: the deadline to request a regrade
    is April 26.  
*   [Assignment 2 compilation results][log2]: the deadline to request a regrade
    is May 8.  
*   [Assignment 3 compilation results][log3]: the deadline to request a regrade
    is May 22.  
*   [Assignment 4 compilation results][log4]: the deadline to request a regrade
    is May 31.  

[log1]: hw1-compilation-log.txt
[log2]: hw2-compilation-log.txt
[log3]: hw3-compilation-log.txt
[log4]: hw4-compilation-log.txt

---

## Processed assignments

To understand the _score reports_ sent to the email address on record on your
_MyUcla_ profile you will need three components:

1.  The output files and/or data your projects produced while running. [These
    files can be found here][results]. Click the link, then find the directory
    corresponding to your Student ID (_e.g.,_ `123456789`).  Within this
    directory, a structure similar to the following can be found:

    ~~~~~
     123456789
     ├── hw1
     │   ├── LogsAndReports
     │   └── OutputFiles
     ├── hw2
     │   ├── LogsAndReports
     │   └── OutputFiles
     ├── hw3
     │   ├── Data
     │   ├── LogsAndReports
     │   └── Plots
     └── hw4
         ├── LogsAndReports
         └── OutputFiles
    ~~~~~

    As mentioned above, these directories contain output, data, and log files
    associated to your project. In some cases, a `diff.txt` file can also be
    found indicating the changes needed (if any) to _fix_ your project, for
    example if it crashed while running. 

1.  The driver files and/or test cases that were used to process your projects.
    [These can be found here][drivers]. They are grouped by assignment.

1.  Last, but not least: [a set of _partial_ solutions][solutions] (also grouped
    by assignment).  Since I get to reuse most of these assignments all the
    time, I do not release complete solutions; however I do give away some key
    elements of my implementations. Since comparing your code against mine might
    not be possible, you are encouraged instead to check the class interfaces,
    as well as the way these classes and/or functions are declared. Pay especial
    attention to the signatures and whether or not they are coded as _members_,
    _non-members_ of a class/namespace.


[results]: companion-files/
[drivers]: other-files/
[solutions]: partial_solns/


[Return to main course website][PIC]

[PIC]: ..

