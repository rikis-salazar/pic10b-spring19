# _Home-made_ classes

During this quarter we will be coding some _standard-like_ classes from scratch.
The idea is for us to learn how these classes _work under the hood_. Please note
that our versions will likely be _much weaker_ than other similarly-named
classes that provide better functionality.

*   The template `DoublyLinkedList<T>` class: this material was started during
    the _2016 Summer_ quarter. It attempts to explain the ideas and technical
    concepts behind a doubly linked list container.

    -   [The `DoublyLinkedList<T>`: handout][list-h]
    -   [The `DoublyLinkedList<T>`: code][list-c]

[list-h]: ../handouts/template-list/
[list-c]: ../handouts/template-list/dl-list-files/


*   The non-template `Pic10b::vector` class: this material was started during
    the _2016 Summer_ quarter. It attempts to explain the ideas and technical
    concepts behind a vector container.

    -   [A non-template `Pic10b::vector`: handout][vector-h]
    -   [A non-template `Pic10b::vector`: code][vector-c]

[vector-h]: ../handouts/non-template-vector/
[vector-c]: https://bitbucket.org/rikis-salazar/10b-hw-pic10b-vector/raw/master/src/pic10b_vector_non_template.cpp


*   The `BasicNode` class: this material was started during the _2019 Spring_
    quarter. It attempts to make the case that the code provided by the compiler
    for the so-called _Big 4_ make not be appropriate for some classes.

    -   [The case against the default Big 4 behavior: handout][case-against]
    -   [A `BasicNodeClass` class: code][basic-node]

[case-against]: ../handouts/case-against-default-big-4/
[basic-node]: ../handouts/case-against-default-big-4/case-against-files/basic-node-default.cpp


*   The `Thing` class: this material was started during the _2016 Summer_
    quarter. It attempts to explain the ideas and technical concepts behind the
    so-called _Big 4_.

    -   [A `Thing` class: handout][thing]
    -   [A `Thing` class: code][thing-code]

[thing]: ../handouts/the-big-4/
[thing-code]: ../handouts/the-big-4/big4-files/thing.cpp

*   The `Cosa` class: this material was generated during the _2018 Winter_
    quarter. The code from the slides corresponds to a hybrid of the code we
    discussed during lecture.

    Use the [slides (summary)][cosa-slide] to
    
    i.  review concepts related to the big 4 (_e.g.,_ what are they and what are
        their signatures), and to
    i.  help you figure out how to use `static` member functions and fields.

    -   Lecture 1: [summary (slides)][cosa-slide], [code][cosa-code],
        [code (clean version)][cosa-clean-code].

[cosa-slide]: cosa/home-made-cosa_slides.pdf
[cosa-code]: cosa/lec1_code/cosa.cpp
[cosa-clean-code]: cosa/lec1_code/clean-cosa.cpp


*   The `Fraction` class: the material posted here was generated during the
    _2018 Winter_ quarter at UCLA. In particular, the slides might contain
    statements along the line of
                          
    > _during lecture, we discussed X notion, ... we also coded Y function..._ 

    While technically those statements might actually be false, rest assured
    that all topics/functions mentioned in these slides will be, covered at some
    point during this current quarter.

    -   Fractions 1: [summary (slides)][lec1-slide], [code][lec1-code].
    -   Fractions 2: [summary (slides)][lec2-slide], [code][lec2-code],
        [code (clean version)][lec2-clean-code].
    -   Fractions 3: [summary (slides)][lec3-slide], [code][lec3-code],
        [code (clean version)][lec3-clean-code], [output file][out3].
    -   Fractions 4: [summary (slides)][lec4-slide], [code][lec4-code],
        [code (clean version)][lec4-clean-code].
    -   Fractions 5: [summary (slides)][lec5-slide], [code][lec5-code],
        [code (clean version)][lec5-clean-code].
    -   Fractions 6: [summary (slides)][lec6-slide], [code][lec6-code],
        [code (clean version)][lec6-clean-code].

Additionally, take a look at [this mostly equivalent implementation][eq] of our
`Fraction` class. It corresponds to the approach I followed during the 2017
summer session. Notice that unlike the code in the bullet points above, here

i.  the arithmetic operators (`operator+`, `operator-`, etc.) are implemented in
    terms of the _shortcut-like_ member ones (`Fraction::operator+=`,
    `Fraction::operator-=`, etc.), and

i.  the _helper_ functions `Fraction::normalize()`, as well as `Fraction::gcd()`
    have been implemented.

[lec1-slide]: fraction/Lecture1_slides.pdf
[lec1-code]: fraction/lec1_code/fraction.cpp

[lec2-slide]: fraction/Lecture2_slides.pdf
[lec2-code]: fraction/lec2_code/fraction.cpp
[lec2-clean-code]: fraction/lec2_code/clean-fraction.cpp

[lec3-slide]: fraction/Lecture3_slides.pdf
[lec3-code]: fraction/lec3_code/fraction.cpp
[lec3-clean-code]: fraction/lec3_code/clean-fraction.cpp
[out3]: fraction/lec3_code/output.txt

[lec4-slide]: fraction/Lecture4_slides.pdf
[lec4-code]: fraction/lec4_code/fraction.cpp
[lec4-clean-code]: fraction/lec4_code/clean-fraction.cpp

[lec5-slide]: fraction/Lecture5_slides.pdf
[lec5-code]: fraction/lec5_code/fraction.cpp
[lec5-clean-code]: fraction/lec5_code/clean-fraction.cpp

[lec6-slide]: fraction/Lecture6_slides.pdf
[lec6-code]: fraction/lec6_code/fraction.cpp
[lec6-clean-code]: fraction/lec6_code/clean-fraction.cpp

[eq]: fraction/three-file-layout

---

[Return to main PIC course website][main]

[main]: ..
