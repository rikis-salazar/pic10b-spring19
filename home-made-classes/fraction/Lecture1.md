% Lecture 1 (Summary)
% Ricardo Salazar
% January 8, 2017


# The theory 

## What we did

During lecture we:

 * discussed the class syllabus, and

 * started "coding" a `Fraction` class.


## What we discovered

Concepts we came across:

 i. differences between `class` and `struct`. 

 i. **Shadowing**:  
 
    > variables with similar names but different scopes, they can lead to
    > confusion and/or logic errors.

 i. **Overloading**:  
 
    > different functions that share their name, but take a different number
    > and/or type of parameters.
    

## The goals

For now, we want to: 

 i. Implement a `Fraction` class that is "smart"; it should be able to handle
    the statements
    
    * `Fraction f;        // f = 0/1`

    * `Fraction g(7);     // g = 7/1`

    * `Fraction h(2,3);   // h = 2/3`

 i. Identify and eliminate common programming errors and/or bad practices:
    
    *  _shadowing_,

    * _copy/paste_ sections of code,

    * unnecessary [re]assignation of values.
 

# The code

## The interface:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
class Fraction{
  // private:      // <-- Not needed. Why?
    int numer;
    int denom;

  public:
    Fraction();
    Fraction( int );
    Fraction( int, int );
    ...           // <-- We'll figure this out later.
};
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


## The member function definitions:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
Fraction::Fraction(){
    int numer = 0;    // <-- Shadowing!
    int denom = 1;
}

Fraction::Fraction( int n ){
    number = n;       // <-- Oops...
    denom = 1;
}

Fraction::Fraction( int n, int d ){
    number = n;      // <-- ... I did it again!!!
    denom = d;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Other notes

## The issues

 *  The constructor that takes no parameters constructs a _garbage_ fraction.

 *  Copy/paste of code leads to copy/paste of bugs (`number` _vs_ `numer`).

 *  Construction of objects is **inefficient**.  

    > All constructors reserve memory for `numer` and `denom`, but their
    > "correct" values are not stored until later. This is not a big issue for
    > primitive types (_e.g._ `int` or `double`), but it is for "big" objects.

## Wishlist

 *  Member functions that "display" (print) the `Fraction` object.

 *  A set of functions that act like the _square brackets_ operator for
    `std::vector<SomeType>`.  
    
    Assuming `v` is vector of integers, `v[0]` can act
    
    - as a _getter_:
    
    > ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
    > cout << "1st value: " << v[0];
    > ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    - or as a _setter_: 
    
    > ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
    > v[0] = 2018;
    > ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Wishlist (cont)

We'll use `Fraction::numerator()` and `Fraction::denominator()`
to perform similar tasks.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
// Currently f constructs a _garbage_ fraction
Fraction f;

// As setters to update `f`
f.numerator() = 20;
f.denominator() = 18;
// [ f = 20/18 ]

// As getters to display `f`
std::cout << f.numerator() << "/" << f.denominator();
// Output: 20/18 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 


## Experiments

Just like function parameters can be _sent to_ a function by either **value** 
or by **reference**, functions can also return by value or by reference.

We want to experiment with this concept. In the process, we'll see how we can 
overload a familiar operator, namely `operator<<`, so that we too can _send_
our `Fraction` objects to the console, and/or to a file.


## `struct` _vs_ `class`

In `C++` the only difference is the default privacy for members.
  
 *  Here `field1` and `fun1()` are **public** member fields.

    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
    struct Thing {
        int field1;
        void fun1();

      private:
        double field2;

      public:
        void fun2();
    };
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## `struct` _vs_ `class` (cont)

 *  Here `field1` and `fun1()` are **private** member fields.

    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
    class Cosa {
        int field1;
        void fun1();

      private:
        double field2;

      public:
        void fun2();
    };
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Other resources

## Code from lecture

|**File**|**Description**|**URL**|
|:---------------:|:------------------------------------------|:---------------|
| [`fraction.cpp`][00-example] | Constructors for `Fraction` class. | [`cpp.sh/8rgkq`][00-url] |

[00-example]: lec1_code/fraction.cpp
[00-url]: http://cpp.sh/8rgkq
