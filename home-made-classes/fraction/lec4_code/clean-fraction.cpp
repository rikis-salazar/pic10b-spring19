#include <iostream>          // std::cout, std::ostream
#include <fstream>           // std::ofstream

// using statements
using std::cout;
using std::ostream;
using std::ofstream;

class Fraction{
  private:
    int numer;
    int denom;

  public:
    // constructors (with default arguments)
    Fraction( int /* n */ = 0, int /* d */ = 1 );
    // Note(s):
    //  *  The place to set the default aguments is the function declaration
    //  *  The name of the parameters are not needed; only the types and values


    // Access to private fields
    int& numerator();
    int& denominator();
    int numerator() const;
    int denominator() const;

    // Operators can also be **MEMBERS**
    Fraction operator-();         // <----- Unary "minus"
    //                ^^ <----------------- Why no parameter(s)?
    Fraction operator*( const Fraction& );
    Fraction operator/( const Fraction& );
    //                  ^^^^^^^^^^^^^^^ <-- Why only one parameter?


    // Friend **NON-MEMBER** functions and operators
    friend ostream& operator<<( ostream&, const Fraction& );
    friend Fraction operator+( const Fraction&, const Fraction& );
};


// Operators can also be non-member, non-friend.
// 
//
// Binary "minus"
Fraction operator-( const Fraction&, const Fraction& );


/* ********************** end of declarations ******************** */


// One constructor to rule them all...
Fraction::Fraction( int n, int d ){
    numer = n;
    denom = d;
}


// the other getter/setter functions
int& Fraction::numerator() {
    return numer;
} 
int& Fraction::denominator() {
    return denom;
} 
int Fraction::numerator() const {
    return numer;
} 
int Fraction::denominator() const {
    return denom;
} 



// Non-member friend operators
ostream& operator<<( ostream& out, const Fraction& f ){
    out << f.numer << '/' << f.denom;   // <-- Requires friendship
    return out;
}

Fraction operator+( const Fraction& a, const Fraction& b ){
    // Recall:    p/q + r/s = ( p*s + q*r ) / q*s;

    int p = a.numerator();
    int q = a.denominator();
    int r = b.numerator();
    int s = b.denominator();

    int top = p*s + q*r;
    int bottom = q*s;

    return Fraction(top,bottom);
}

/* ********************** end of definitions ******************** */

// Member unary minus
Fraction Fraction::operator-(){
    return Fraction();
}
// Member multiplication operator
Fraction Fraction::operator*( const Fraction& rhs ){
    return Fraction();
}
// Member division operator
Fraction Fraction::operator/( const Fraction& rhs ){
    return Fraction();
}
// Non-member, non-friend, binary minus
Fraction operator-( const Fraction& lhs, const Fraction& rhs ){
    return Fraction();
}
/* ************************* end of stubs *********************** */


// The so-called DRIVER (or tester)
int main(){

    Fraction f;
    Fraction g(7);
    Fraction h(2,3);


    // `f` to file stream
    std::ofstream fout;
    fout.open("output.txt");
    fout << "f = " << f << '\n';
    fout.close();


    // `g` to console 
    cout << "g = " << g << '\n';

    h.numerator() = 2018;
    // `h` to console
    cout << "h = " << h << "\n\n";


    Fraction half(1,2);
    Fraction third(1,3);
    Fraction almost_one(1023,1024);

    cout << "One half: " << half << '\n';
    cout << "One third: " << third << '\n';
    cout << "Almost one: " << almost_one << "\n\n";

    cout << "1/2 + 1/3 = " << half + third << '\n';
    cout << almost_one + Fraction(1,1024) << " = 1\t(O.o)\n\n";

    // Unary minus
    cout << "-(1/2) = " << -half << '\n';
    cout << "-(1/2) = " << half.operator-() << '\n';

    // Multiplication
    cout << "1/2 * 1/3 = " << half * third << '\n';
    cout << "1/2 * 1/3 = " << half.operator*(third) << '\n';

    // Division (see multiplication above)

    // Binary minus
    cout << "1/2 - 1/3 = " << half - third << '\n';
    cout << "1/2 - 1/3 = " << operator-(half, third) << '\n';

    return 0;
}
