#include <iostream>          // std::cout, std::ostream
#include <fstream>           // std::ofstream
#include <string>            // std::string

// using statements
using std::cout;
using std::ostream;
using std::ofstream;
using std::string;

class Fraction{
  private:
    int numer;
    int denom;
    string name;

  public:
    // constructor(s)
    Fraction( int = 0, int = 1 );

    // Access to private fields
    int& numerator();
    int& denominator();
    int numerator() const;
    int denominator() const;

    // Operators can also be **MEMBERS**
    Fraction operator-();         // <----- Unary "minus"
    //                ^^ <----------------- Why no parameter(s)?
    // These 2 should be non-members
    Fraction operator*( const Fraction& );
    Fraction operator/( const Fraction& );
    //                  ^^^^^^^^^^^^^^^ <-- Why only one parameter?


    // Friend **NON-MEMBER** functions and operators
    friend ostream& operator<<( ostream&, const Fraction& );
    friend Fraction operator+( const Fraction&, const Fraction& );
};

// Alternat...
Fraction operator*( int n , Fraction f ){
    // reuse
    // return f * n;
    return f.operator*(n);
}

// Operators can also be non-member, non-friend.
// 
//
// Binary "minus"
Fraction operator-( const Fraction&, const Fraction& );


/* ********************** end of declarations ******************** */


// One constructor to rule them all...
Fraction::Fraction( int n, int d ) : numer(n), denom(d), name("Rikis") {
//     numer = n;
//     denom = d;
    //normalize();
    // name = "Rikis";
}


// the other getter/setter functions
int& Fraction::numerator() {
    return numer;
} 
int& Fraction::denominator() {
    return denom;
} 
int Fraction::numerator() const {
    return numer;
} 
int Fraction::denominator() const {
    return denom;
} 



// Non-member friend operators
ostream& operator<<( ostream& out, const Fraction& f ){
    out << f.numer << '/' << f.denom;   // <-- Requires friendship
    return out;
}

Fraction operator+( const Fraction& a, const Fraction& b ){
    // Recall:    p/q + r/s = ( p*s + q*r ) / q*s;

    int p = a.numerator();
    int q = a.denominator();
    int r = b.numerator();
    int s = b.denominator();

    int top = p*s + q*r;
    int bottom = q*s;

    return Fraction(top,bottom);
}


// Member multiplication operator
Fraction Fraction::operator*( const Fraction& rhs ){
    // Recall:    p/q * r/s = p*r / q*s;

    // Note: (*this) is the implicit parameter (i.e., lhs);
    int p = (*this).numerator();
    int q = this->denominator();

    // This is equivalent to
    // int p = numerator();
    // int q = denominator();

    int r = rhs.numerator();
    int s = rhs.denominator();

    int top = p*r;
    int bottom = q*s;

    return Fraction(top,bottom);
}

/* ********************** end of definitions ******************** */
// Member unary minus
Fraction Fraction::operator-(){
    return Fraction();
}

// Member division operator
Fraction Fraction::operator/( const Fraction& rhs ){
    return Fraction();
}
// Non-member, non-friend, binary minus
Fraction operator-( const Fraction& lhs, const Fraction& rhs ){
    return Fraction();
}

/* ************************* end of stubs *********************** */

bool operator<( const Fraction& f , const Fraction& g ){
    return 1 ;    // Fun fact: int values are interpreted as 
}                 //           0 = false, anything else = true.
bool operator>( const Fraction& f , const Fraction& g ){
    return g < f;
}


// The so-called DRIVER (or tester)
int main(){

    Fraction f;
    Fraction g(7);
    Fraction h(2,3);


    // `f` to file stream
    std::ofstream fout;
    fout.open("output.txt");
    fout << "f = " << f << '\n';
    fout.close();


    // `g` to console 
    cout << "g = " << g << '\n';

    h.numerator() = 2018;
    // `h` to console
    cout << "h = " << h << "\n\n";


    Fraction half(1,2);
    Fraction third(1,3);
    Fraction almost_one(1023,1024);

    cout << "One half: " << half << '\n';
    cout << "One third: " << third << '\n';
    cout << "Almost one: " << almost_one << "\n\n";

    // cout << "1/2 + 1/3 = " << half + third << '\n';
    cout << "1/2 + 1/3 = " << operator+(half, third) << '\n';
    cout << almost_one + Fraction(1,1024) << " = 1\t(O.o)\n\n";

    // Unary minus
    cout << "-(1/2) = " << -half << '\n';
    cout << "-(1/2) = " << half.operator-() << '\n';

    // Multiplication
    cout << "1/2 * 1/3 = " << half * third << '\n';
    cout << "1/2 * 1/3 = " << half.operator*(third) << '\n';

    // Division (see multiplication above)

    // Binary minus
    cout << "1/2 - 1/3 = " << half - third << '\n';
    cout << "1/2 - 1/3 = " << operator-(half, third) << '\n';


    // Member oper
    cout << "Half times 2 = " << half * 2 << '\n';
    cout << "Half times 2 = " << 2 * half << '\n';

    // Non-Member oper
    cout << "One plus half = " << 1 + half << '\n';
    cout << "One plus half = " << half + 1 << '\n';

    return 0;
}
