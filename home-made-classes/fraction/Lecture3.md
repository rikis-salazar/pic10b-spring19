% Lecture 3 (Summary)
% Ricardo Salazar
% January 12, 2017


# The theory 

## What we did

During lecture we

 *  learned equivalent _funtion-like_ ways to _operate_ on variables/objects.
    E.g.:

    - `my_string = "hi";` _vs_ `my_string.operator=("hi");`
    - `cout << a;` _vs_ `operator<<( cout, a );`

 *  attempted to _copy_ `cout`... and failed misserably.

 *  implemented _printer_ functions `print()` and `print_to()` sticking to the
    principle of **single point of maintenance**;

 *  learned about **default arguments**. 
 
    - `f.print_to();` displays `f` to the console; whereas
    - `f.print_to( fout );` displays `f` to a file. 

## Concepts we came across:

 i. **default argument** values:

    > if a function expects a parameter, and none is provided, a default value
    > is assumed.

 i. output stream objects (_e.g.,_ `cout`) cannot be copied:
 
    > their _copy constructor[^one]_ has been 'deleted'.   

 i. **inheritance**:  
 
    > a technique of _code reuse[^two]_ that allows us to use a general type
    > [`std::ostream`] to refer to `cout`, as well as output file streams.
    
[^one]: A member of the so-called _Big 3_.
[^two]: Unfortunately we will not cover it in this course.

## The goals

We want to: 

 i. Make our constructor(s) more efficient via **initialization lists**.

 i. Determine the correct signature of `operator<<` so that expressions like 
    `cout << Fraction(20,18);` "work as expected".

    This is what we have so far:

    ~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
    TYPE_OF_COUT operator<<( TYPE_OF_COUT, const Fraction& );
    ~~~~~~~~~~~~~~~~~~~~~~~

 i. Write mathematical operators that are readable and easy to maintain.

# The code

## The interface

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
class Fraction{
  ...

  public:
    // No longer needed
    //     Fraction();
    //     Fraction( int );
    Fraction( int /* n */ = 0, int /* d */ = 1 );
    ...                

    void print() const;
    void print_to( ostream& /* out */ = cout );
    ...
};
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


## Function definitions

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
Fraction::Fraction( int n, int d ) {
    numer = n;
    denom = d;
}

void Fraction::print() const {
    print_to( cout );        // code reuse
    return;
}

void Fraction::print_to( ostream& out ) const {
    out << numer < '/' < denom;
    return;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


# Other notes

## Fun facts & issues

Fun facts:

 *  `cout` **is not** a function; it is a global object. Its type is
    `std::ostream` and it is declared as 

    ~~~~~~~~~~~~~~~~~~~~ {.cpp}
    extern ostream cout;     // <-- Found in <iostream>
    ~~~~~~~~~~~~~~~~~~~~

Issues:

 *  Code is still ~~very repetitive and~~ inefficient.

 *  `std::ostream` objects **cannot** be copied. If they are to be parameters,
    they **must** be passed _by reference_.

 *  `std::ostream` objects **cannot** be copied. If they are to be return
    values...

## Wishlist

 *  A way to convert `Fraction` objects to `double` values.

    > ~~~~~~~~~~~~~~~~~~~~ {.cpp}
    > Fraction half(1,2);
    > cout << "1/2 = " << static_cast<double>(half);
    > // Output: "1/2 = 0.5"
    > ~~~~~~~~~~~~~~~~~~~~

 *  A working set of mathematical operations that can handle "mixed" numeric
    types. For example, we should be able to

    - add an `int` and a `Fraction`.

        ~~~~~~~~~~~~~~~~~~~~ {.cpp}
        cout << "Three halves = " << half + 1; 
        ~~~~~~~~~~~~~~~~~~~~

    - compare a `Fraction` and a `double`.

        ~~~~~~~~~~~~~~~~~~~~ {.cpp}
        if ( half < 3.141592 )
            cout << "Test failed\n"; 
        ~~~~~~~~~~~~~~~~~~~~


## Chaining of `cout` statements

The line of code 

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
cout << b << a;
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

is read from left to right. Moreover, if we partially _see it_ the way the
compiler does,

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
operator<<( cout, b ) << a;
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

it should be clear that the function `operator<<` needs to return its first
parameter, namely `cout`.  Thus, after `b` is "sent to `cout`", the
remaining statement is

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
cout << a;
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


## Challenge

Since the type of `cout` is known, we might be tempted to copy/paste the
body of `print_to( ostream& )` and code this

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
ostream operator<<( ostream out, const Fraction& f ){
    // Update the `ostream` object...
    out << numer << '/' << denom;

    // ... then return it to allow for chaining.
    // E.g.: cout << b << a ;
    return out;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

However, it fails miserably! Can you fix it?


# Resources

## Code from lecture

|**File**|**Description**|**URL**|
|:---------------:|:------------------------------------------|:---------------|
| [`fraction.cpp`][00-example] | Code from lecture. | [`cpp.sh/8joox`][00-url] |
| [`cle...on.cpp`][01-example] | Code from lecture (clean version). | [`cpp.sh/3dswi`][01-url] |
| [`output.txt`][02-example] | Output from `cle...on.cpp`. | [`ix.io/mXH`][02-url] |

[00-example]: lec3_code/fraction.cpp
[01-example]: lec3_code/clean-fraction.cpp
[02-example]: lec3_code/output.txt
[00-url]: http://cpp.sh/8joox
[01-url]: http://cpp.sh/3dswi
[02-url]: http://ix.io/mXH
