#include <iostream>

using std::cout;

class Fraction{
  private:
    int numer;
    int denom;

  public:
    Fraction();
    Fraction( int );
    Fraction( int, int );


    void print() const ;
    void print_to( /* some object*/ );

    int get_numerator() const ;
    int get_denominator() const ;

    void set_numerator( int );
    void set_denominator( int );

    int& numerator(){
        return numer;
    } 

    int& denominator(){
        return denom;
    } 
    int denominator() const {
        return denom;
    } 
    /*
        // RETURN_TYPE  numerator();
        // RETURN_TYPE  denominator();
    */
};

Fraction::Fraction(){
    // int numer = 0;
    // int denom = 1;
    numer = 0;
    denom = 1;
}

Fraction::Fraction( int n ){
    // number = n;    // <-- Oops ...
    numer = n;
    denom = 1;
}

Fraction::Fraction( int n, int d ){
    // number = n;    // <-- ... I did it again!!!
    numer = n;
    denom = d;
}

int Fraction::get_numerator() const {
    return numer;
}

int Fraction::get_denominator() const {
    return denom;
}

void Fraction::print() const {
    // cout << numer << "/" << get_denominator();
    cout << numer << "/" << denominator();
    return;
}


int main(){

    Fraction f;       // <-- Holds garbage values
    Fraction g(7);    // [ g = 7/1 ]
    Fraction h(2,3);  // [ h = 2/3 ]

    f.print();
    cout << '\n';


    h.numerator() = 2018;
    h.print();
    cout << '\n';


    return 0;
}
