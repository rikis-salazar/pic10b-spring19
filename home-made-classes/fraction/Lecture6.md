% Lecture 6 (Summary)
% Ricardo Salazar
% January 22, 2017


# The theory 

## During lecture we

 *  [almost] finished our `Fraction` class. 

 *  coded member _shortcut-like_ operators that return by reference.

 *  coded _prefix_ increment/decrement operators that also return by reference.

 *  coded _postfix_ increment/decrement operators that return by value.

 *  coded boolean operators via a helper `compare(...)` function.
    
 *  inadvertently came across the so-called "Big 3" member functions.

     - The compiler was kind enough to provide the code for these functions.

## Concepts we came across:

 i. **prefix** _vs_ **postfix** increment/decrement:

    > the latter returns a copy of the old value of the fraction.

 i. **assignment operator**:
 
    > did you notice? In `operator+=`, the line 
    >
    > ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
    > *this = *this + rhs;
    > ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 i. **copy constructor**:
 
    > did you notice? In `operator++( int )`, the line 
    >
    > ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
    > Fraction clone(*this);
    > ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
 i. **destructor**:
 
    > did you notice? Probably not. It is _invisible_.

# The code

## The interface

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
class Fraction{
    ...

    Fraction& operator+=( const Fraction& );
    Fraction& operator*=( const Fraction& );
    ...

    Fraction& operator++();       // prefix version
    Fraction operator++( int );   // postfix version
    ...

    operator double() const;      // No return type???
    ...
};
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


## Function definitions

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
Fraction& Fraction::operator+=( const Fraction& rhs ){
    *this = *this + rhs;   // This line packs a big punch!!!
    // this->operator=( operator+( *this, rhs) );
    // ^^^^^^ Not needed. I use it to [over] emphasize the
    //        fact that `operator=` is a member function.
    return *this;
}
Fraction& Fraction::operator*=( const Fraction& rhs ){
    // We can avoid using `operator=`. The "trick" is to use
    // the `int` version of `operator*=`.
    numer *= rhs.numer;
    denom *= rhs.denom;
    return *this;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


## Function definitions (cont)

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
// Prefix version. It has the net effect of calling `+= 1`.
Fraction& Fraction::operator++(){
    return *this += 1;    // Packs an even bigger punch!!!
    // return ( this->operator+=( Fraction(1) ) );
}

// Postfix version. It increase the fraction by 1, but
// returns a copy of the old value.
Fraction Fraction::operator++( int unused ){
    Fraction clone(*this); // Calls the copy constructor.
    ++(*this);
    return clone;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


## Function definitions (cont)

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
// Prefix decrement
Fraction& Fraction::operator--(){
    return *this -= 1;
}

// Postfix decrement
Fraction Fraction::operator--( int unused ){
    // Can avoid using the copy constructor. The trick is to
    // use the two-parameter constructor instead.
    Fraction clone( numer, denom );
    // Also, no need for the parenthesis.
    --*this;
    return clone;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


## Function definitions (cont)

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
// Conversion operator. The return type is _inferred_ by the
// name of the operator. `double`, in this case.
Fraction::operator double() const {  
    return static_cast<double>( numer ) / denom;
    // The following might not give the desired results
    //     return numer / denom ;
    // Why???
}
// Conversion to `bool`.
//     Fraction::operator bool() const {  
//         return numer;     // false if zero, true otherwise.
//     }
// This conversion causes more headaches than it resolves b/c 
// `bool` is an `int` type. It leads to conversion ambiguity.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


# Other notes

## Loose ends

 *  The `normalize()` helper function.  

    Ensures our fractions are reduced[^footnote], and that the sign, if any, is
    kept _in the numerator_.
    
 *  _Counting_ the number of `Fraction` objects created by a driver.  

    Hint: We will not remain `static`, we'll do something about it.

 *  The "Big 3".
 
     -  What are they?
     
     -  What do they do?
     
     -  Are they always there?
     
     -  What are their signatures?

     -  Why is the _destructor_ invisible, and what does it destroy?

[^footnote]: No common prime factors shared by both, the numerator, and the
denominator.


# Resources

## Code from lecture

|**File**|**Description**|**URL**|
|:---------------:|:------------------------------------------|:---------------|
| [`fraction.cpp`][00-example] | Code from lecture. | [`cpp.sh/4ftsh`][00-url] |
| [`cle...on.cpp`][01-example] | Code from lecture (clean version). | [`cpp.sh/7zoa`][01-url] |

[00-example]: lec5_code/fraction.cpp
[01-example]: lec5_code/clean-fraction.cpp
[00-url]: http://cpp.sh/4ftsh
[01-url]: http://cpp.sh/7zoa
